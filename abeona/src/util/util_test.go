package util

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type (
	ContainsStringFib struct {
		StringSlice    []string
		StringValue    string
		ExpectedResult bool
	}

	IntersectStringFib struct {
		StringSlice1         []string
		StringSlice2         []string
		ExpectedIntersection []string
	}
)

func TestContainsString(t *testing.T) {
	testFibs := map[string]ContainsStringFib{
		"Success1": ContainsStringFib{
			StringSlice:    []string{"asd", "qwe", "zxc"},
			StringValue:    "asd",
			ExpectedResult: true,
		},
		"Success2": ContainsStringFib{
			StringSlice:    []string{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m"},
			StringValue:    "q",
			ExpectedResult: true,
		},
		"Success3": ContainsStringFib{
			StringSlice:    []string{"123"},
			StringValue:    "123",
			ExpectedResult: true,
		},
		"Fail1": ContainsStringFib{
			StringSlice:    []string{"asd", "qwe", "zxc"},
			StringValue:    "as",
			ExpectedResult: false,
		},
		"Fail2": ContainsStringFib{
			StringSlice:    []string{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p", "a", "s", "d", "f", "g", "h", "j", "k", "l", "z", "x", "c", "v", "b", "n", "m"},
			StringValue:    "1",
			ExpectedResult: false,
		},
		"Fail3": ContainsStringFib{
			StringSlice:    []string{},
			StringValue:    "a",
			ExpectedResult: false,
		},
	}

	for tc, fib := range testFibs {
		ContainsStringTestHelp(t, tc, fib)
	}
}

func ContainsStringTestHelp(t *testing.T, tc string, fib ContainsStringFib) {
	check := ContainsString(fib.StringSlice, fib.StringValue)
	assert.Equal(t, fib.ExpectedResult, check)
}

func TestIntersectString(t *testing.T) {
	testFibs := map[string]IntersectStringFib{
		"Success1": IntersectStringFib{
			StringSlice1:         []string{"q", "w", "e", "r", "t", "y"},
			StringSlice2:         []string{"q", "w", "e", "r", "t", "y"},
			ExpectedIntersection: []string{"q", "w", "e", "r", "t", "y"},
		},
		"Success2": IntersectStringFib{
			StringSlice1:         []string{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"},
			StringSlice2:         []string{"q", "w", "e", "r", "t", "y"},
			ExpectedIntersection: []string{"q", "w", "e", "r", "t", "y"},
		},
		"Success3": IntersectStringFib{
			StringSlice1:         []string{"q", "w", "e", "r", "t", "y"},
			StringSlice2:         []string{"q", "w", "e", "r", "t", "y", "u", "i", "o", "p"},
			ExpectedIntersection: []string{"q", "w", "e", "r", "t", "y"},
		},
		"Success4": IntersectStringFib{
			StringSlice1:         []string{"q", "w", "e", "r", "t", "y"},
			StringSlice2:         []string{"t", "y", "u", "i", "o", "p"},
			ExpectedIntersection: []string{"t", "y"},
		},
		"Success5": IntersectStringFib{
			StringSlice1:         []string{"q", "w", "e", "r", "t", "y"},
			StringSlice2:         []string{},
			ExpectedIntersection: []string{},
		},
		"Success6": IntersectStringFib{
			StringSlice1:         []string{},
			StringSlice2:         []string{"q", "w", "e", "r", "t", "y"},
			ExpectedIntersection: []string{},
		},
		"Success7": IntersectStringFib{
			StringSlice1:         []string{},
			StringSlice2:         []string{},
			ExpectedIntersection: []string{},
		},
	}

	for tc, fib := range testFibs {
		IntersectStringTestHelp(t, tc, fib)
	}
}

func IntersectStringTestHelp(t *testing.T, tc string, fib IntersectStringFib) {
	t.Log("TESTCASE", tc)
	intersect := IntersectString(fib.StringSlice1, fib.StringSlice2)
	assert.Equal(t, fib.ExpectedIntersection, intersect)
}
