package util

// ContainsString checks if a string is contained in a string slice
func ContainsString(slice []string, val string) bool {
	for _, elm := range slice {
		if elm == val {
			return true
		}
	}

	return false
}

// IntersectString finds intersection between 2 slices
func IntersectString(slice1, slice2 []string) []string {
	found := make(map[string]bool)
	for _, elm := range slice1 {
		found[elm] = true
	}

	dup := make(map[string]bool)
	intersect := make([]string, 0)
	for _, elm := range slice2 {
		if found[elm] && !dup[elm] {
			intersect = append(intersect, elm)
			dup[elm] = true
		}
	}

	return intersect
}
