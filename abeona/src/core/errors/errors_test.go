package errors

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSampleErr(t *testing.T) {
	err := SampleErr()
	assert.Equal(t, "0001 : Failed to do something", err.Error())
}
