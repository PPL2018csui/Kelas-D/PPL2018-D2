package errors

import (
	"fmt"
)

// SampleErr creates sample error using error codes and strings
func SampleErr() error {
	return fmt.Errorf("%04d : %s", ErrSampleCode, ErrSampleStr)
}
