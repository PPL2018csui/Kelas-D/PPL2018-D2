package errors

// codes and strings for all top level errors in abeona web service
const (
	ErrSampleCode = 1
	ErrSampleStr  = "Failed to do something"
)
