package arango

import (
	"log"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
)

// GetDatabase gets a database from
func GetDatabase(dbName string) (driver.Database, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println(err)
		return nil, err
	}

	conn, err := http.NewConnection(http.ConnectionConfig{
		Endpoints: []string{env.Arango.Host},
	})
	if err != nil {
		log.Println(err)
		return nil, err
	}

	var arangoAuth driver.Authentication
	if env.Arango.NoAuth == 0 {
		arangoAuth = driver.BasicAuthentication(env.Arango.Username, env.Arango.Password)
	}
	client, err := driver.NewClient(driver.ClientConfig{
		Connection:     conn,
		Authentication: arangoAuth,
	})

	if err != nil {
		log.Println(err)
		return nil, err
	}

	database, err := client.Database(nil, dbName)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return database, nil
}
