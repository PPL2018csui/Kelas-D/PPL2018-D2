package arango

import (
	"fmt"
	"strings"

	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/util"
)

type (
	SortQueryStruct struct {
		CollAbbr   string
		AttrNames  []string
		Descending bool
	}

	FilterPredicateStruct struct {
		CollAbbr  string
		AttrName  string
		Reference string
		Type      string
	}

	FilterQueryStruct struct {
		Predicate FilterPredicateStruct
		Queries   []FilterQueryStruct
		Relation  string
	}

	MatchesQueryStruct struct {
		CollAbbr   string
		MatchJSONs []string
		Relation   string
	}
)

var (
	InsertQueryTemplate string = `
LET docs = %[2]s

FOR d IN docs
	INSERT d INTO %[1]s
	RETURN d`

	ReturnQueryTemplate string = `
FOR %[1]s IN %[2]s
	FILTER MATCHES(%[1]s, %[3]s)
	LIMIT %[4]d
	RETURN %[1]s`

	ReturnQueryMatchPaginateTemplate string = `
FOR %[1]s IN %[2]s
	FILTER MATCHES(%[1]s, %[3]s)
	SORT %[4]s
	LIMIT %[5]d, %[6]d
	RETURN %[1]s`

	ReturnQueryFilterPaginateTemplate string = `
FOR %[1]s IN %[2]s
	FILTER %[3]s
	SORT %[4]s
	LIMIT %[5]d, %[6]d
	RETURN %[1]s`

	UpdateQueryTemplate string = `
FOR %[1]s IN %[2]s
	FILTER MATCHES(%[1]s, %[3]s)
	UPDATE %[1]s WITH %[4]s IN %[2]s
	RETURN {
		"old" : OLD,
		"new" : %[1]s
	}`

	UpdateQueryFilterTemplate string = `
FOR %[1]s IN %[2]s
	FILTER %[3]s
	UPDATE %[1]s WITH %[4]s IN %[2]s
	RETURN {
		"old" : OLD,
		"new" : %[1]s
	}`

	RemoveQueryTemplate string = `
FOR %[1]s IN %[2]s
	FILTER MATCHES(%[1]s, %[3]s)
	REMOVE %[1]s IN %[2]s
	RETURN OLD`

	PredicateChoices = []string{"==", "!=", "<", "<=", ">", ">=", "LIKE", "IN", "NOT IN", "REGEX"}
	RelationChoices  = []string{"&&", "||"}
)

func GenerateSortQuery(data SortQueryStruct) string {
	attrStrings := make([]string, 0)
	for _, attr := range data.AttrNames {
		attrStrings = append(attrStrings, fmt.Sprintf("%s.%s", data.CollAbbr, attr))
	}

	var order string
	if data.Descending {
		order = " DESC"
	}

	return strings.Join(attrStrings, ", ") + order
}

func GenerateFilterPredicate(pred FilterPredicateStruct) string {
	if util.ContainsString(PredicateChoices, pred.Type) {
		return fmt.Sprintf("%s.%s %s %s", pred.CollAbbr, pred.AttrName, pred.Type, pred.Reference)
	}

	return "true == false"
}

func GenerateFilterQuery(data FilterQueryStruct) string {
	var filterQuery string
	if util.ContainsString(RelationChoices, data.Relation) {
		queryStrings := make([]string, 0)
		for _, query := range data.Queries {
			queryStrings = append(queryStrings, GenerateFilterQuery(query))
		}

		filterQuery = fmt.Sprintf("( %s )", strings.Join(queryStrings, fmt.Sprintf(" %s ", data.Relation)))
	} else if data.Relation == "" {
		filterQuery = GenerateFilterPredicate(data.Predicate)
	} else {
		filterQuery = "true == false"
	}

	return filterQuery
}

func GenerateMatchesQuery(data MatchesQueryStruct) string {
	var matchesQuery string
	if util.ContainsString(RelationChoices, data.Relation) {
		matches := make([]string, 0)
		for _, match := range data.MatchJSONs {
			matches = append(matches, fmt.Sprintf("MATCHES(%s, %s)", data.CollAbbr, match))
		}

		matchesQuery = strings.Join(matches, fmt.Sprintf(" %s ", data.Relation))
	} else {
		matchesQuery = "true == false"
	}

	return matchesQuery
}
