package arango

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type (
	InsertQueryTemplateFib struct {
		CollectionName string
		InsertData     string
		ExpectedQuery  string
	}

	ReturnQueryTemplateFib struct {
		CollectionAbbr string
		CollectionName string
		FilterData     string
		Limit          int
		ExpectedQuery  string
	}

	UpdateQueryTemplateFib struct {
		CollectionAbbr string
		CollectionName string
		FilterData     string
		NewData        string
		ExpectedQuery  string
	}

	RemoveQueryTemplateFib struct {
		CollectionAbbr string
		CollectionName string
		FilterData     string
		ExpectedQuery  string
	}

	GenerateSortQueryFib struct {
		QueryStruct   SortQueryStruct
		ExpectedQuery string
	}

	GenerateFilterPredicateFib struct {
		QueryStruct   FilterPredicateStruct
		ExpectedQuery string
	}

	GenerateFilterQueryFib struct {
		QueryStruct   FilterQueryStruct
		ExpectedQuery string
	}

	GenerateMatchesQueryFib struct {
		QueryStruct   MatchesQueryStruct
		ExpectedQuery string
	}
)

func TestInsertQueryTemplate(t *testing.T) {
	testFibs := map[string]InsertQueryTemplateFib{
		"tc1": InsertQueryTemplateFib{
			CollectionName: "Data",
			InsertData:     `[{"foo":"bar"}]`,
			ExpectedQuery: `
LET docs = [{"foo":"bar"}]

FOR d IN docs
	INSERT d INTO Data
	RETURN d`,
		},
		"tc2": InsertQueryTemplateFib{
			CollectionName: "Sample",
			InsertData:     `[{"foo":"bar","num":1}]`,
			ExpectedQuery: `
LET docs = [{"foo":"bar","num":1}]

FOR d IN docs
	INSERT d INTO Sample
	RETURN d`,
		},
		"tc3": InsertQueryTemplateFib{
			CollectionName: "Product",
			InsertData:     `[{"foo":"bar"},{"user":"user"},{"gallery":["img1","img2"]}]`,
			ExpectedQuery: `
LET docs = [{"foo":"bar"},{"user":"user"},{"gallery":["img1","img2"]}]

FOR d IN docs
	INSERT d INTO Product
	RETURN d`,
		},
	}

	for tc, fib := range testFibs {
		InsertQueryTemplateTestHelp(t, tc, fib)
	}
}

func InsertQueryTemplateTestHelp(t *testing.T, tc string, fib InsertQueryTemplateFib) {
	query := fmt.Sprintf(InsertQueryTemplate, fib.CollectionName, fib.InsertData)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestReturnQueryTemplate(t *testing.T) {
	testFibs := map[string]ReturnQueryTemplateFib{
		"tc1": ReturnQueryTemplateFib{
			CollectionAbbr: "d",
			CollectionName: "Data",
			FilterData:     `{"foo":"bar"}`,
			Limit:          1,
			ExpectedQuery: `
FOR d IN Data
	FILTER MATCHES(d, {"foo":"bar"})
	LIMIT 1
	RETURN d`,
		},
		"tc2": ReturnQueryTemplateFib{
			CollectionAbbr: "s",
			CollectionName: "Sample",
			FilterData:     `{"foo":"bar","test",123}`,
			Limit:          5,
			ExpectedQuery: `
FOR s IN Sample
	FILTER MATCHES(s, {"foo":"bar","test",123})
	LIMIT 5
	RETURN s`,
		},
		"tc3": ReturnQueryTemplateFib{
			CollectionAbbr: "p",
			CollectionName: "Product",
			FilterData:     `{"foo":"bar","temp":123}`,
			Limit:          10,
			ExpectedQuery: `
FOR p IN Product
	FILTER MATCHES(p, {"foo":"bar","temp":123})
	LIMIT 10
	RETURN p`,
		},
	}

	for tc, fib := range testFibs {
		ReturnQueryTemplateTestHelp(t, tc, fib)
	}
}

func ReturnQueryTemplateTestHelp(t *testing.T, tc string, fib ReturnQueryTemplateFib) {
	query := fmt.Sprintf(ReturnQueryTemplate, fib.CollectionAbbr, fib.CollectionName, fib.FilterData, fib.Limit)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestUpdateQueryTemplate(t *testing.T) {
	testFibs := map[string]UpdateQueryTemplateFib{
		"tc1": UpdateQueryTemplateFib{
			CollectionAbbr: "d",
			CollectionName: "Data",
			FilterData:     `{"foo":"bar"}`,
			NewData:        `{"data": "new"}`,
			ExpectedQuery: `
FOR d IN Data
	FILTER MATCHES(d, {"foo":"bar"})
	UPDATE d WITH {"data": "new"} IN Data
	RETURN {
		"old" : OLD,
		"new" : d
	}`,
		},
		"tc2": UpdateQueryTemplateFib{
			CollectionAbbr: "s",
			CollectionName: "Sample",
			FilterData:     `{"foo":"bar","test":123}`,
			NewData:        `{"data":"new","hello":"world"}`,
			ExpectedQuery: `
FOR s IN Sample
	FILTER MATCHES(s, {"foo":"bar","test":123})
	UPDATE s WITH {"data":"new","hello":"world"} IN Sample
	RETURN {
		"old" : OLD,
		"new" : s
	}`,
		},
		"tc3": UpdateQueryTemplateFib{
			CollectionAbbr: "p",
			CollectionName: "Product",
			FilterData:     `{"foo":"bar","temp":123}`,
			NewData:        `{"fake":"news","foo":["not", bar]}`,
			ExpectedQuery: `
FOR p IN Product
	FILTER MATCHES(p, {"foo":"bar","temp":123})
	UPDATE p WITH {"fake":"news","foo":["not", bar]} IN Product
	RETURN {
		"old" : OLD,
		"new" : p
	}`,
		},
	}

	for tc, fib := range testFibs {
		UpdateQueryTemplateTestHelp(t, tc, fib)
	}
}

func UpdateQueryTemplateTestHelp(t *testing.T, tc string, fib UpdateQueryTemplateFib) {
	query := fmt.Sprintf(UpdateQueryTemplate, fib.CollectionAbbr, fib.CollectionName, fib.FilterData, fib.NewData)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestRemoveQueryTemplate(t *testing.T) {
	testFibs := map[string]RemoveQueryTemplateFib{
		"tc1": RemoveQueryTemplateFib{
			CollectionAbbr: "d",
			CollectionName: "Data",
			FilterData:     `{"foo","bar"}`,
			ExpectedQuery: `
FOR d IN Data
	FILTER MATCHES(d, {"foo","bar"})
	REMOVE d IN Data
	RETURN OLD`,
		},
		"tc2": RemoveQueryTemplateFib{
			CollectionAbbr: "s",
			CollectionName: "Sample",
			FilterData:     `{"foo":"bar","test":123}`,
			ExpectedQuery: `
FOR s IN Sample
	FILTER MATCHES(s, {"foo":"bar","test":123})
	REMOVE s IN Sample
	RETURN OLD`,
		},
		"tc3": RemoveQueryTemplateFib{
			CollectionAbbr: "p",
			CollectionName: "Product",
			FilterData:     `{"foo":"bar","temp":123}`,
			ExpectedQuery: `
FOR p IN Product
	FILTER MATCHES(p, {"foo":"bar","temp":123})
	REMOVE p IN Product
	RETURN OLD`,
		},
	}

	for tc, fib := range testFibs {
		RemoveQueryTemplateTestHelp(t, tc, fib)
	}
}

func RemoveQueryTemplateTestHelp(t *testing.T, tc string, fib RemoveQueryTemplateFib) {
	query := fmt.Sprintf(RemoveQueryTemplate, fib.CollectionAbbr, fib.CollectionName, fib.FilterData)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestGenerateSortQuery(t *testing.T) {
	testFibs := map[string]GenerateSortQueryFib{
		"tc1": GenerateSortQueryFib{
			QueryStruct: SortQueryStruct{
				CollAbbr:  "foo",
				AttrNames: []string{"bar", "asd", "qwe", "hwe"},
			},
			ExpectedQuery: "foo.bar, foo.asd, foo.qwe, foo.hwe",
		},
		"tc2": GenerateSortQueryFib{
			QueryStruct: SortQueryStruct{
				CollAbbr:  "t",
				AttrNames: []string{"json", "string", "int64", "float64"},
			},
			ExpectedQuery: "t.json, t.string, t.int64, t.float64",
		},
		"tc3": GenerateSortQueryFib{
			QueryStruct: SortQueryStruct{
				CollAbbr:   "p",
				AttrNames:  []string{"is_available", "name", "price"},
				Descending: true,
			},
			ExpectedQuery: "p.is_available, p.name, p.price DESC",
		},
	}

	for tc, fib := range testFibs {
		GenerateSortQueryTestHelp(t, tc, fib)
	}
}

func GenerateSortQueryTestHelp(t *testing.T, tc string, fib GenerateSortQueryFib) {
	query := GenerateSortQuery(fib.QueryStruct)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestGenerateFilterPredicate(t *testing.T) {
	testFibs := map[string]GenerateFilterPredicateFib{
		"tc1": GenerateFilterPredicateFib{
			QueryStruct: FilterPredicateStruct{
				CollAbbr:  "p",
				AttrName:  "rating",
				Reference: "10",
				Type:      "==",
			},
			ExpectedQuery: "p.rating == 10",
		},
		"tc2": GenerateFilterPredicateFib{
			QueryStruct: FilterPredicateStruct{
				CollAbbr:  "p",
				AttrName:  "name",
				Reference: `"%haha%"`,
				Type:      "LIKE",
			},
			ExpectedQuery: `p.name LIKE "%haha%"`,
		},
		"tc3": GenerateFilterPredicateFib{
			QueryStruct: FilterPredicateStruct{
				CollAbbr:  "foo",
				AttrName:  "bar",
				Reference: `[ "asd", "qwe", "zxc", "other" ]`,
				Type:      "NOT IN",
			},
			ExpectedQuery: `foo.bar NOT IN [ "asd", "qwe", "zxc", "other" ]`,
		},
		"fail1": GenerateFilterPredicateFib{
			QueryStruct: FilterPredicateStruct{
				CollAbbr:  "p",
				AttrName:  "name",
				Reference: `"qwe"`,
				Type:      "ha",
			},
			ExpectedQuery: "true == false",
		},
		"fail2": GenerateFilterPredicateFib{
			QueryStruct: FilterPredicateStruct{
				CollAbbr:  "s",
				AttrName:  "data",
				Reference: "123",
				Type:      "no",
			},
			ExpectedQuery: "true == false",
		},
	}

	for tc, fib := range testFibs {
		GenerateFilterPredicateTestHelp(t, tc, fib)
	}
}

func GenerateFilterPredicateTestHelp(t *testing.T, tc string, fib GenerateFilterPredicateFib) {
	query := GenerateFilterPredicate(fib.QueryStruct)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestGenerateFilterQuery(t *testing.T) {
	testFibs := map[string]GenerateFilterQueryFib{
		"tc1": GenerateFilterQueryFib{
			QueryStruct: FilterQueryStruct{
				Predicate: FilterPredicateStruct{
					CollAbbr:  "p",
					AttrName:  "rating",
					Reference: "10",
					Type:      "==",
				},
			},
			ExpectedQuery: "p.rating == 10",
		},
		"tc2": GenerateFilterQueryFib{
			QueryStruct: FilterQueryStruct{
				Queries: []FilterQueryStruct{
					FilterQueryStruct{
						Predicate: FilterPredicateStruct{
							CollAbbr:  "p",
							AttrName:  "rating",
							Reference: "10",
							Type:      "==",
						},
					},
					FilterQueryStruct{
						Predicate: FilterPredicateStruct{
							CollAbbr:  "p",
							AttrName:  "name",
							Reference: `"%haha%"`,
							Type:      "LIKE",
						},
					},
					FilterQueryStruct{
						Predicate: FilterPredicateStruct{
							CollAbbr:  "foo",
							AttrName:  "bar",
							Reference: `[ "asd", "qwe", "zxc", "other" ]`,
							Type:      "NOT IN",
						},
					},
				},
				Relation: "&&",
			},
			ExpectedQuery: `( p.rating == 10 && p.name LIKE "%haha%" && foo.bar NOT IN [ "asd", "qwe", "zxc", "other" ] )`,
		},
		"tc3": GenerateFilterQueryFib{
			QueryStruct: FilterQueryStruct{
				Queries: []FilterQueryStruct{
					FilterQueryStruct{
						Queries: []FilterQueryStruct{
							FilterQueryStruct{
								Predicate: FilterPredicateStruct{
									CollAbbr:  "p",
									AttrName:  "rating",
									Reference: "10",
									Type:      "==",
								},
							},
							FilterQueryStruct{
								Predicate: FilterPredicateStruct{
									CollAbbr:  "foo",
									AttrName:  "bar",
									Reference: `[ "asd", "qwe", "zxc", "other" ]`,
									Type:      "NOT IN",
								},
							},
						},
						Relation: "||",
					},
					FilterQueryStruct{
						Predicate: FilterPredicateStruct{
							CollAbbr:  "p",
							AttrName:  "name",
							Reference: `"%haha%"`,
							Type:      "LIKE",
						},
					},
				},
				Relation: "&&",
			},
			ExpectedQuery: `( ( p.rating == 10 || foo.bar NOT IN [ "asd", "qwe", "zxc", "other" ] ) && p.name LIKE "%haha%" )`,
		},
		"fail1": GenerateFilterQueryFib{
			QueryStruct: FilterQueryStruct{
				Queries:  []FilterQueryStruct{},
				Relation: "hey",
			},
			ExpectedQuery: "true == false",
		},
		"fail2": GenerateFilterQueryFib{
			QueryStruct: FilterQueryStruct{
				Queries: []FilterQueryStruct{
					FilterQueryStruct{
						Predicate: FilterPredicateStruct{
							CollAbbr:  "p",
							AttrName:  "rating",
							Reference: "10",
							Type:      "!=",
						},
					},
					FilterQueryStruct{
						Predicate: FilterPredicateStruct{
							CollAbbr:  "s",
							AttrName:  "data",
							Reference: "123",
							Type:      "no",
						},
					},
				},
				Relation: "||",
			},
			ExpectedQuery: "( p.rating != 10 || true == false )",
		},
	}

	for tc, fib := range testFibs {
		GenerateFilterQueryTestHelp(t, tc, fib)
	}
}

func GenerateFilterQueryTestHelp(t *testing.T, tc string, fib GenerateFilterQueryFib) {
	query := GenerateFilterQuery(fib.QueryStruct)
	assert.Equal(t, fib.ExpectedQuery, query)
}

func TestGenerateMatchesQuery(t *testing.T) {
	testFibs := map[string]GenerateMatchesQueryFib{
		"tc1": GenerateMatchesQueryFib{
			QueryStruct: MatchesQueryStruct{
				CollAbbr: "foo",
				MatchJSONs: []string{
					`{"_key":"123"}`,
				},
				Relation: "||",
			},
			ExpectedQuery: `MATCHES(foo, {"_key":"123"})`,
		},
		"tc2": GenerateMatchesQueryFib{
			QueryStruct: MatchesQueryStruct{
				CollAbbr: "p",
				MatchJSONs: []string{
					`{"_key":"234","is_available":true}`,
					`{"_key":"345","is_available":false}`,
				},
				Relation: "&&",
			},
			ExpectedQuery: `MATCHES(p, {"_key":"234","is_available":true}) && MATCHES(p, {"_key":"345","is_available":false})`,
		},
		"tc3": GenerateMatchesQueryFib{
			QueryStruct: MatchesQueryStruct{
				CollAbbr: "s",
				MatchJSONs: []string{
					`{"_key":"456","name":"Wibi"}`,
					`{"_key":"567","name":"Sandi"}`,
					`{"_key":"678","test":"success","help":"fail"}`,
				},
				Relation: "||",
			},
			ExpectedQuery: `MATCHES(s, {"_key":"456","name":"Wibi"}) || MATCHES(s, {"_key":"567","name":"Sandi"}) || MATCHES(s, {"_key":"678","test":"success","help":"fail"})`,
		},
		"fail1": GenerateMatchesQueryFib{
			QueryStruct: MatchesQueryStruct{
				CollAbbr: "s",
				MatchJSONs: []string{
					`{"_key":"456"}`,
				},
				Relation: "ha",
			},
			ExpectedQuery: "true == false",
		},
		"fail2": GenerateMatchesQueryFib{
			QueryStruct: MatchesQueryStruct{
				CollAbbr: "s",
				MatchJSONs: []string{
					`{"_key":"456","name":"Wibi"}`,
					`{"_key":"567","name":"Sandi"}`,
					`{"_key":"678","test":"success","help":"fail"}`,
				},
				Relation: "&|",
			},
			ExpectedQuery: "true == false",
		},
	}

	for tc, fib := range testFibs {
		GenerateMatchesQueryTestHelp(t, tc, fib)
	}
}

func GenerateMatchesQueryTestHelp(t *testing.T, tc string, fib GenerateMatchesQueryFib) {
	query := GenerateMatchesQuery(fib.QueryStruct)
	assert.Equal(t, fib.ExpectedQuery, query)
}
