package arango

import (
	"errors"
	"testing"

	driver "github.com/arangodb/go-driver"
	"github.com/arangodb/go-driver/http"
	"github.com/bouk/monkey"
	"github.com/stretchr/testify/assert"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
)

func mockHTTPNewConnectionFail(config http.ConnectionConfig) (driver.Connection, error) {
	return nil, driver.ArangoError(driver.ArangoError{HasError: true, Code: 404, ErrorNum: -1, ErrorMessage: "this is a mock failure"})
}

func mockDriverNewClientFail(config driver.ClientConfig) (driver.Client, error) {
	return nil, driver.ArangoError(driver.ArangoError{HasError: true, Code: 404, ErrorNum: -1, ErrorMessage: "this is a mock failure"})
}

func mockCoreGetEnvFail() (*core.Config, error) {
	return nil, errors.New("this is a mock error")
}

type (
	GetDatabaseFib struct {
		PatchHTTPNewConnection func(config http.ConnectionConfig) (driver.Connection, error)
		PatchDriverNewClient   func(config driver.ClientConfig) (driver.Client, error)
		PatchCoreGetEnv        func() (*core.Config, error)
		DatabaseName           string
		CollectionName         string
		ExpectedDBError        error
		ExpectedCollError      error
	}
)

func TestGetDatabase(t *testing.T) {
	testFibs := map[string]GetDatabaseFib{
		"abeona-User": GetDatabaseFib{
			DatabaseName:   "abeona",
			CollectionName: "User",
		},
		"abeona-Admin": GetDatabaseFib{
			DatabaseName:   "abeona",
			CollectionName: "Admin",
		},
		"something1": GetDatabaseFib{
			DatabaseName:    "something1",
			CollectionName:  "asd",
			ExpectedDBError: driver.ArangoError(driver.ArangoError{HasError: true, Code: 404, ErrorNum: 1228, ErrorMessage: "database not found"}),
		},
		"something2": GetDatabaseFib{
			DatabaseName:    "something2",
			CollectionName:  "qwe",
			ExpectedDBError: driver.ArangoError(driver.ArangoError{HasError: true, Code: 404, ErrorNum: 1228, ErrorMessage: "database not found"}),
		},
		"Bad Connection": GetDatabaseFib{
			PatchHTTPNewConnection: mockHTTPNewConnectionFail,
			DatabaseName:           "abeona",
			CollectionName:         "User",
			ExpectedDBError:        driver.ArangoError(driver.ArangoError{HasError: true, Code: 404, ErrorNum: -1, ErrorMessage: "this is a mock failure"}),
		},
		"Bad Client": GetDatabaseFib{
			PatchDriverNewClient: mockDriverNewClientFail,
			DatabaseName:         "abeona",
			CollectionName:       "User",
			ExpectedDBError:      driver.ArangoError(driver.ArangoError{HasError: true, Code: 404, ErrorNum: -1, ErrorMessage: "this is a mock failure"}),
		},
		"Env Error": GetDatabaseFib{
			PatchCoreGetEnv: mockCoreGetEnvFail,
			DatabaseName:    "abeona",
			CollectionName:  "User",
			ExpectedDBError: errors.New("this is a mock error"),
		},
	}

	for tc, fib := range testFibs {
		GetDatabaseTestHelp(t, tc, fib)
	}
}

func GetDatabaseTestHelp(t *testing.T, tc string, fib GetDatabaseFib) {
	// monkeypatch
	if fib.PatchHTTPNewConnection != nil {
		monkey.Patch(http.NewConnection, fib.PatchHTTPNewConnection)
	}
	if fib.PatchDriverNewClient != nil {
		monkey.Patch(driver.NewClient, fib.PatchDriverNewClient)
	}
	if fib.PatchCoreGetEnv != nil {
		monkey.Patch(core.GetEnv, fib.PatchCoreGetEnv)
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	t.Log("TESTCASE", tc)
	db, err := GetDatabase(fib.DatabaseName)
	assert.Equal(t, fib.ExpectedDBError, err)
	if err == nil {
		assert.NotNil(t, db)
	}

	t.Log("PASS")
}
