package core

import (
	"log"

	"github.com/joeshaw/envdecode"
)

// Config stores configuration data from env
// has hierarchical structure to organize variables
type Config struct {
	Test struct {
		String string  `env:"TEST_STRING"`
		Int    int     `env:"TEST_INT"`
		Float  float64 `env:"TEST_FLOAT"`
		Bool   bool    `env:"TEST_BOOL"`
	}

	Arango struct {
		Host     string `env:"ARANGO_HOST,default=http://127.0.0.1:8529"`
		NoAuth   int    `env:"ARANGO_NO_AUTH,default=0"`
		Username string `env:"ARANGO_USERNAME,default=admin"`
		Password string `env:"ARANGO_PASSWORD,default=admin"`
		Database string `env:"ARANGO_DATABASE,default=database"`
	}

	Server struct {
		Port string `env:"PORT,default=14712"`
	}
}

// GetEnv reads env variables and unpacks them to Config structure
func GetEnv() (*Config, error) {
	var cfg Config
	err := envdecode.Decode(&cfg)
	if err != nil {
		log.Println(err)
		return nil, err
	}

	return &cfg, nil
}
