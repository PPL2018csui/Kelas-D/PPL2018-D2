package product

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/util"
)

type (
	// Document represents product document in arangodb
	Document struct {
		Key         string    `json:"_key,omitempty" db:"_key"`
		Name        *string   `json:"name,omitempty" db:"name"`
		IsAvailable *bool     `json:"is_available,omitempty" db:"is_available"`
		Price       *float64  `json:"price,omitempty" db:"price"`
		Category    *string   `json:"category,omitempty" db:"category"`
		Tags        *[]string `json:"tags,omitempty" db:"tags"`
		Description *string   `json:"description,omitempty" db:"description"`
		Image       *string   `json:"image,omitempty" db:"image"`
		Gallery     *[]string `json:"gallery,omitempty" db:"gallery"`
		Rating      *float64  `json:"rating,omitempty" db:"rating"`

		Other string `json:"other,omitempty" db:"other"`
	}

	// ModificationResponse contains old and new product documents to compare
	ModificationResponse struct {
		Old *Document `json:"old"`
		New *Document `json:"new"`
	}

	CreateDocumentParams struct {
		Documents []*Document
	}

	GetDocumentParams struct {
		Filter   *Document
		Sorting  []string
		Page     int
		Quantity int
	}

	UpdateDocumentsParams struct {
		Filters []*Document
		NewData *Document
	}

	DeleteDocumentParams struct {
		Filters []*Document
	}

	GetProductByTagParams struct {
		Tags     []string
		Quantity int
	}
)

var (
	ProductAbbreviation string = "p"
	ProductCollection   string = "Product"

	SortableProductAttr = []string{"_key", "name", "is_available", "price", "rating"}

	DefaultIsAvailable = true
	DefaultPrice       = 0.0
	DefaultDescription = "No Description."
	DefaultRating      = 0.0

	DefaultDocument Document = Document{
		IsAvailable: &DefaultIsAvailable,
		Price:       &DefaultPrice,
		Description: &DefaultDescription,
		Rating:      &DefaultRating,
	}
)

// ValidateNewProduct validates important fields in the new Product Document and fills default fields if needed
func ValidateNewProduct(product *Document) error {
	if product.Name == nil || product.Category == nil {
		return errors.New("Invalid new product, fields (name, category) must be filled")
	}

	if product.IsAvailable == nil {
		product.IsAvailable = DefaultDocument.IsAvailable
	}

	if product.Price == nil {
		product.Price = DefaultDocument.Price
	}

	if product.Description == nil {
		product.Description = DefaultDocument.Description
	}

	if product.Rating == nil {
		product.Rating = DefaultDocument.Rating
	}

	return nil
}

func ValidateModifyFilter(filters []*Document) error {
	if len(filters) == 0 {
		return errors.New("Invalid modify filter, there must be at least 1 filter")
	}

	for _, product := range filters {
		if product.Key == "" {
			return errors.New("Invalid modify filter, field (_key) must be explicitly defined")
		}
	}

	return nil
}

func ValidateUpdateData(product *Document) error {
	if product.Key != "" || product.Rating != nil {
		return errors.New("Invalid update data, fields (_key, rating) may not be updated manually")
	}

	return nil
}

// CreateProduct creates new documents in arangodb
// receives list of products to be created
// generates documents in arangodb
// returns list of successfully created products
func CreateProduct(ctx context.Context, params *CreateDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse docs section for the final query
	docsJson, err := json.Marshal(params.Documents)
	if err != nil {
		log.Println("Marshal Documents", err)
		return nil, nil
	}

	// Generate the final query
	insertQuery := fmt.Sprintf(arango.InsertQueryTemplate, ProductCollection, docsJson)

	err = db.ValidateQuery(nil, insertQuery)
	if err != nil {
		log.Println("Validate Query", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, insertQuery, nil)
	if err != nil {
		log.Println("Execute Query", err)
		return nil, nil
	}
	defer cursor.Close()

	insertedProducts := make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Document", err)
				continue
			}
		}

		doc.Key = meta.Key
		insertedProducts = append(insertedProducts, &doc)
	}

	return insertedProducts, nil
}

// GetProduct returns documents in arangodb
// receives filter for products
// fetches documents from arangodb
// returns list of productts matching filter
func GetProduct(ctx context.Context, params *GetDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse FILTER section for the final query
	filterJson, err := json.Marshal(params.Filter)
	if err != nil {
		log.Println("Marshal Filter", err)
		return nil, nil
	}

	page := params.Page
	if page < 1 {
		page = 1
	}

	quantity := params.Quantity
	if quantity < 1 {
		quantity = 5
	}

	offset := (page - 1) * quantity

	// Generate the final query
	var returnQuery string
	if quantity < 0 {
		returnQuery = fmt.Sprintf(arango.ReturnQueryTemplate, ProductAbbreviation, ProductCollection, filterJson, offset, quantity)
	} else {
		// Calculate sorting and offset
		sorting := util.IntersectString(params.Sorting, SortableProductAttr)
		if len(sorting) == 0 {
			sorting = append(sorting, "_key")
		}
		sortQuery := arango.GenerateSortQuery(arango.SortQueryStruct{
			CollAbbr:  ProductAbbreviation,
			AttrNames: sorting,
		})

		returnQuery = fmt.Sprintf(arango.ReturnQueryMatchPaginateTemplate, ProductAbbreviation, ProductCollection, filterJson, sortQuery, offset, quantity)
	}

	err = db.ValidateQuery(nil, returnQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, returnQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, nil
	}

	returnedProducts := make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		returnedProducts = append(returnedProducts, &doc)
	}

	return returnedProducts, nil
}

// UpdateProduct updates documents in arangodb
// receives filter for affected products and fields to be replaces along with the data
// updates documents in arangodb
// returns a modification comparison
func UpdateProduct(ctx context.Context, params *UpdateDocumentsParams) ([]*ModificationResponse, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse FILTER section for the final query
	filterStrings := make([]string, 0)
	for _, filter := range params.Filters {
		filterJson, err := json.Marshal(filter)
		if err != nil {
			log.Println("Marshal Filter", err)
			return nil, nil
		}
		filterStrings = append(filterStrings, string(filterJson))
	}

	filterMatches := arango.GenerateMatchesQuery(arango.MatchesQueryStruct{
		CollAbbr:   ProductAbbreviation,
		MatchJSONs: filterStrings,
		Relation:   "||",
	})

	// Parse WITH section for the final query
	withJson, err := json.Marshal(params.NewData)
	if err != nil {
		log.Println("Marshal Data", err)
		return nil, nil
	}

	// Generate the final query
	updateQuery := fmt.Sprintf(arango.UpdateQueryFilterTemplate, ProductAbbreviation, ProductCollection, filterMatches, withJson)

	err = db.ValidateQuery(nil, updateQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, updateQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, nil
	}

	updatedProducts := make([]*ModificationResponse, 0)
	for {
		var doc ModificationResponse
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Old.Key = meta.Key
		doc.New.Key = meta.Key
		updatedProducts = append(updatedProducts, &doc)
	}

	return updatedProducts, nil
}

// DeleteProduct deletes documents in arangodb
// receives filter for products to be deleted
// deletes documents from arangodb collection
// returns a list of deleted products
func DeleteProduct(ctx context.Context, params *DeleteDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse FILTER section for the final query
	filterStrings := make([]string, 0)
	for _, filter := range params.Filters {
		filterJson, err := json.Marshal(filter)
		if err != nil {
			log.Println("Marshal Filter", err)
			return nil, nil
		}
		filterStrings = append(filterStrings, string(filterJson))
	}

	filterMatches := arango.GenerateMatchesQuery(arango.MatchesQueryStruct{
		CollAbbr:   ProductAbbreviation,
		MatchJSONs: filterStrings,
		Relation:   "||",
	})

	// Generate the final query
	removeQuery := fmt.Sprintf(arango.RemoveQueryTemplate, ProductAbbreviation, ProductCollection, filterMatches)

	err = db.ValidateQuery(nil, removeQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, removeQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, nil
	}

	removedProducts := make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		removedProducts = append(removedProducts, &doc)
	}

	return removedProducts, nil
}

func GetProductByTag(ctx context.Context, params *GetProductByTagParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env:", err)
		return nil, err
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database:", err)
		return nil, err
	}

	tags, err := json.Marshal(params.Tags)
	if err != nil {
		log.Println("Marshal Tags:", err)
		return nil, err
	}

	quantity := params.Quantity
	if quantity < 1 {
		quantity = 5
	}

	// Generate the final query
	var returnQuery string
	if len(tags) > 0 {
		returnQuery = fmt.Sprintf(`
FOR t in Tag
	FILTER t.name IN %s
	FOR v IN 1..1 INBOUND t HasTag
		LIMIT %d
		RETURN v`, string(tags), quantity)
	} else {
		returnQuery = fmt.Sprintf(`
FOR p in Product
	LIMIT %d
	RETURN p`, quantity)
	}

	err = db.ValidateQuery(nil, returnQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, err
	}

	// Run the query
	cursor, err := db.Query(nil, returnQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, err
	}

	returnedProducts := make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		returnedProducts = append(returnedProducts, &doc)
	}

	return returnedProducts, nil
}
