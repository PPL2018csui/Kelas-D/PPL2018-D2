package product

import (
	"encoding/json"
	"errors"
	"log"
	"testing"

	driver "github.com/arangodb/go-driver"
	"github.com/bouk/monkey"
	"github.com/stretchr/testify/assert"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
)

func mockGetEnv() (*core.Config, error) {
	return nil, errors.New("this is a mock error")
}

func mockGetDatabaseFail(dbName string) (driver.Database, error) {
	return nil, errors.New("this is a mock error")
}

func mockJSONMarshalFail(v interface{}) ([]byte, error) {
	return nil, errors.New("this is a mock error")
}

type (
	ValidateNewProductFib struct {
		Product          *Document
		ExpectedDocument Document
		ExpectedError    error
	}

	ValidateModifyFilterFib struct {
		Filters       []*Document
		ExpectedError error
	}

	ValidateUpdateDataFib struct {
		Product       *Document
		ExpectedError error
	}

	CreateProductFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		NewProducts      []*Document
		ExpectedError    error
		ExpectedProducts []*Document
	}

	GetProductFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		ProductFilter    *Document
		Sorting          []string
		Page             int
		Quantity         int
		ExpectedError    error
		ExpectedProducts []*Document
	}

	UpdateProductFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		ProductFilters   []*Document
		NewData          *Document
		ExpectedError    error
		ExpectedProducts []*Document
	}

	DeleteProductFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		ProductFilters   []*Document
		ExpectedError    error
		ExpectedProducts []*Document
	}
)

func TestValidateNewProduct(t *testing.T) {
	name1 := "carash"
	name2 := "krivis"
	isAvailable1 := true
	isAvailable2 := false
	price1 := float64(10000.0)
	price2 := float64(55000.0)
	category1 := "food"
	category2 := "drink"
	tags1 := []string{"vintage"}
	description1 := "Something you must try."
	description2 := "You won't regret it."
	image1 := "http://imageurl.com"
	image2 := "https://not.imgur.lol"
	gallery1 := []string{"http://wrong.web"}
	gallery2 := []string{"http://nothing.to.se", "https://definitely.not.com"}
	rating1 := float64(4.4)
	rating2 := float64(3.9)

	err := errors.New("Invalid new product, fields (name, category) must be filled")

	testFibs := map[string]ValidateNewProductFib{
		"tc1": ValidateNewProductFib{
			Product: &Document{
				Name:     &name1,
				Category: &category1,
			},
			ExpectedDocument: Document{
				Name:        &name1,
				Category:    &category1,
				IsAvailable: &DefaultIsAvailable,
				Price:       &DefaultPrice,
				Description: &DefaultDescription,
				Rating:      &DefaultRating,
			},
		},
		"tc2": ValidateNewProductFib{
			Product: &Document{
				Name:        &name2,
				Category:    &category2,
				IsAvailable: &isAvailable2,
				Price:       &price2,
			},
			ExpectedDocument: Document{
				Name:        &name2,
				Category:    &category2,
				IsAvailable: &isAvailable2,
				Price:       &price2,
				Description: &DefaultDescription,
				Rating:      &DefaultRating,
			},
		},
		"tc3": ValidateNewProductFib{
			Product: &Document{
				Name:        &name2,
				Category:    &category1,
				IsAvailable: &isAvailable2,
				Price:       &price1,
				Description: &description2,
				Rating:      &rating1,
			},
			ExpectedDocument: Document{
				Name:        &name2,
				Category:    &category1,
				IsAvailable: &isAvailable2,
				Price:       &price1,
				Description: &description2,
				Rating:      &rating1,
			},
		},
		"tc4": ValidateNewProductFib{
			Product: &Document{
				Name:        &name1,
				Category:    &category2,
				Description: &description1,
				Image:       &image2,
				Gallery:     &gallery1,
				Rating:      &rating2,
			},
			ExpectedDocument: Document{
				Name:        &name1,
				Category:    &category2,
				IsAvailable: &DefaultIsAvailable,
				Price:       &DefaultPrice,
				Description: &description1,
				Image:       &image2,
				Gallery:     &gallery1,
				Rating:      &rating2,
			},
		},
		"tc5": ValidateNewProductFib{
			Product: &Document{
				Name:        &name1,
				IsAvailable: &isAvailable1,
				Price:       &price1,
				Category:    &category2,
				Tags:        &tags1,
				Description: &description2,
				Image:       &image1,
				Gallery:     &gallery2,
				Rating:      &rating1,
			},
			ExpectedDocument: Document{
				Name:        &name1,
				IsAvailable: &isAvailable1,
				Price:       &price1,
				Category:    &category2,
				Tags:        &tags1,
				Description: &description2,
				Image:       &image1,
				Gallery:     &gallery2,
				Rating:      &rating1,
			},
		},
		"fail1": ValidateNewProductFib{
			Product:          &Document{},
			ExpectedDocument: Document{},
			ExpectedError:    err,
		},
		"fail2": ValidateNewProductFib{
			Product: &Document{
				Name: &name1,
			},
			ExpectedDocument: Document{
				Name: &name1,
			},
			ExpectedError: err,
		},
		"fail3": ValidateNewProductFib{
			Product: &Document{
				Category: &category2,
			},
			ExpectedDocument: Document{
				Category: &category2,
			},
			ExpectedError: err,
		},
	}

	for tc, fib := range testFibs {
		ValidateNewProductTestHelp(t, tc, fib)
	}
}

func ValidateNewProductTestHelp(t *testing.T, tc string, fib ValidateNewProductFib) {
	err := ValidateNewProduct(fib.Product)
	assert.Equal(t, fib.ExpectedDocument, *fib.Product)
	assert.Equal(t, fib.ExpectedError, err)
}

func TestValidateModifyFilter(t *testing.T) {
	name := "carash"
	price := 10000.0

	errCount := errors.New("Invalid modify filter, there must be at least 1 filter")
	errField := errors.New("Invalid modify filter, field (_key) must be explicitly defined")

	testFibs := map[string]ValidateModifyFilterFib{
		"tc1": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"tc2": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key: "1234",
				},
				&Document{
					Key: "2345",
				},
				&Document{
					Key: "3456",
				},
			},
		},
		"tc3": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key:  "4567",
					Name: &name,
				},
				&Document{
					Key:   "5678",
					Name:  &name,
					Price: &price,
				},
				&Document{
					Key:  "6789",
					Name: &name,
				},
				&Document{
					Key: "7890",
				},
			},
		},
		"fail1": ValidateModifyFilterFib{
			Filters:       []*Document{},
			ExpectedError: errCount,
		},
		"fail2": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{},
			},
			ExpectedError: errField,
		},
		"fail3": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key:  "1234",
					Name: &name,
				},
				&Document{},
			},
			ExpectedError: errField,
		},
		"fail4": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key:  "2345",
					Name: &name,
				},
				&Document{
					Name:  &name,
					Price: &price,
				},
				&Document{
					Key:   "4567",
					Name:  &name,
					Price: &price,
				},
				&Document{
					Key:   "5678",
					Price: &price,
				},
			},
			ExpectedError: errField,
		},
	}

	for tc, fib := range testFibs {
		ValidateModifyFilterTestHelp(t, tc, fib)
	}
}

func ValidateModifyFilterTestHelp(t *testing.T, tc string, fib ValidateModifyFilterFib) {
	err := ValidateModifyFilter(fib.Filters)
	assert.Equal(t, fib.ExpectedError, err)
}

func TestValidateUpdateData(t *testing.T) {
	name := "carash"
	price := float64(10000.0)
	rating := float64(5.0)

	err := errors.New("Invalid update data, fields (_key, rating) may not be updated manually")

	testFibs := map[string]ValidateUpdateDataFib{
		"tc1": ValidateUpdateDataFib{
			Product: &Document{},
		},
		"tc2": ValidateUpdateDataFib{
			Product: &Document{
				Name: &name,
			},
		},
		"tc3": ValidateUpdateDataFib{
			Product: &Document{
				Price: &price,
			},
		},
		"tc4": ValidateUpdateDataFib{
			Product: &Document{
				Name:  &name,
				Price: &price,
			},
		},
		"fail1": ValidateUpdateDataFib{
			Product: &Document{
				Key:    "1234",
				Rating: &rating,
			},
			ExpectedError: err,
		},
		"fail2": ValidateUpdateDataFib{
			Product: &Document{
				Key:    "2345",
				Name:   &name,
				Rating: &rating,
			},
			ExpectedError: err,
		},
		"fail3": ValidateUpdateDataFib{
			Product: &Document{
				Rating: &rating,
			},
			ExpectedError: err,
		},
		"fail4": ValidateUpdateDataFib{
			Product: &Document{
				Key: "3456",
			},
			ExpectedError: err,
		},
	}

	for tc, fib := range testFibs {
		ValidateUpdateDataTestHelp(t, tc, fib)
	}
}

func ValidateUpdateDataTestHelp(t *testing.T, tc string, fib ValidateUpdateDataFib) {
	err := ValidateUpdateData(fib.Product)
	assert.Equal(t, fib.ExpectedError, err)
}

func TestCreateProduct(t *testing.T) {
	name1 := "carash"
	isAvailable1 := true
	price1 := float64(10000.0)
	category1 := "food"
	tags1 := []string{"vintage"}
	description1 := "Something you must try."
	image1 := "http://imageurl.com"
	gallery1 := []string{"http://wrong.web"}
	rating1 := float64(4.4)

	testFibs := map[string]CreateProductFib{
		"Success1": CreateProductFib{
			NewProducts: make([]*Document, 0),
		},
		"Success2": CreateProductFib{
			NewProducts: []*Document{
				&Document{
					Name:        &name1,
					IsAvailable: &isAvailable1,
					Price:       &price1,
					Category:    &category1,
					Tags:        &tags1,
					Description: &description1,
					Image:       &image1,
					Gallery:     &gallery1,
					Rating:      &rating1,
				},
			},
		},
		"Bad core.GetEnv": CreateProductFib{
			PatchGetEnv: mockGetEnv,
			NewProducts: make([]*Document, 0),
		},
		"Bad arango.GetDatabase": CreateProductFib{
			PatchGetDatabase: mockGetDatabaseFail,
			NewProducts:      make([]*Document, 0),
		},
		"Bad json.Marshal": CreateProductFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			NewProducts:      make([]*Document, 0),
		},
	}

	for tc, fib := range testFibs {
		CreateProductTestHelp(t, tc, fib)
	}
}

func CreateProductTestHelp(t *testing.T, tc string, fib CreateProductFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data (test)
	docs, err := CreateProduct(nil, &CreateDocumentParams{
		Documents: fib.NewProducts,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docs)
		assert.NotNil(t, err)
		return
	} else {
		assert.Nil(t, err)
	}

	// assert return value
	docs = docs
	// TODO assert values in docs

	// assert database values
	// TODO direct RETURN query to database

	// cleanup the data
	CleanUpDatabase(t)
}

func TestGetProduct(t *testing.T) {
	testFibs := map[string]GetProductFib{
		"Success": GetProductFib{
			ProductFilter: &Document{},
		},
		"Bad core.getEnv": GetProductFib{
			PatchGetEnv:   mockGetEnv,
			ProductFilter: &Document{},
		},
		"Bad arango.getDatabase": GetProductFib{
			PatchGetDatabase: mockGetDatabaseFail,
			ProductFilter:    &Document{},
		},
		"Bad json.Marshal": GetProductFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ProductFilter:    &Document{},
		},
	}

	for tc, fib := range testFibs {
		GetProductTestHelp(t, tc, fib)
	}
}

func GetProductTestHelp(t *testing.T, tc string, fib GetProductFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data
	// TODO direct INSERT query to database

	// get database values (test)
	docs, err := GetProduct(nil, &GetDocumentParams{
		Filter:   fib.ProductFilter,
		Sorting:  fib.Sorting,
		Page:     fib.Page,
		Quantity: fib.Quantity,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docs)
		assert.NotNil(t, err)
		return
	} else {
		assert.Nil(t, err)
	}

	// assert return values
	// TODO assert values in docs

	// cleanup the data
	CleanUpDatabase(t)
}

func TestUpdateProduct(t *testing.T) {
	price := float64(10000)

	testFibs := map[string]UpdateProductFib{
		"Success": UpdateProductFib{
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{
				Price: &price,
			},
			ExpectedProducts: []*Document{
				&Document{
					Key:   "1234",
					Price: &price,
				},
			},
		},
		"Bad core.getEnv": UpdateProductFib{
			PatchGetEnv: mockGetEnv,
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
		"Bad arango.getDatabase": UpdateProductFib{
			PatchGetDatabase: mockGetDatabaseFail,
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
		"Bad json.Marshal": UpdateProductFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
	}

	for tc, fib := range testFibs {
		UpdateProductTestHelp(t, tc, fib)
	}
}

func UpdateProductTestHelp(t *testing.T, tc string, fib UpdateProductFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data
	// TODO direct INSERT query to database

	// update database values (test)
	docModResps, err := UpdateProduct(nil, &UpdateDocumentsParams{
		Filters: fib.ProductFilters,
		NewData: fib.NewData,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docModResps)
		assert.NotNil(t, err)
		return
	} else {
		assert.Nil(t, err)
	}

	// assert return values
	// TODO assert values in docModResps

	// assert database values
	// TODO direct RETURN query to database

	// cleanup the data
	CleanUpDatabase(t)
}

func TestDeleteProduct(t *testing.T) {
	testFibs := map[string]DeleteProductFib{
		"temp": DeleteProductFib{
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"Bad core.getEnv": DeleteProductFib{
			PatchGetEnv: mockGetEnv,
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"Bad arango.getDatabase": DeleteProductFib{
			PatchGetDatabase: mockGetDatabaseFail,
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"Bad json.Marshal": DeleteProductFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ProductFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
	}

	for tc, fib := range testFibs {
		DeleteProductTestHelp(t, tc, fib)
	}
}

func DeleteProductTestHelp(t *testing.T, tc string, fib DeleteProductFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data
	// TODO direct INSERT query to database

	// delete database values (test)
	docs, err := DeleteProduct(nil, &DeleteDocumentParams{
		Filters: fib.ProductFilters,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docs)
		assert.NotNil(t, err)
		return
	} else {
		assert.Nil(t, err)
	}

	// assert return values
	// TODO assert values in docs

	// assert database values
	// TODO direct RETURN query to database (check if the data still exists)

	// cleanup the data
	CleanUpDatabase(t)
}

func CleanUpDatabase(t *testing.T) {
	db, err := arango.GetDatabase("abeona")
	if err != nil {
		log.Println("Get Database", err)
		return
	}

	cleanUpQuery := `
FOR p IN Product
	FILTER p.other == "asd"
	REMOVE p IN Product
	RETURN p`

	cursor, err := db.Query(nil, cleanUpQuery, nil)
	t.Log(cursor.Count())
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		t.Log(doc)
	}
}
