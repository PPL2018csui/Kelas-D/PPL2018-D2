package tag

import (
	"context"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
)

type (
	Connection struct {
		arango driver.Database
	}

	Document struct {
		Key  string `json:"key"`
		Name string `json:"name"`
	}

	AllDocumentsParams struct {
		Page     int
		Quantity int
	}
)

func Init() (*Connection, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, err
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, err
	}

	conn := &Connection{
		arango: db,
	}

	return conn, nil
}

func AllDocuments(ctx context.Context, params *AllDocumentsParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env:", err)
		return nil, err
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database:", err)
		return nil, err
	}

	page := params.Page
	if page < 1 {
		page = 1
	}

	quantity := params.Quantity
	if quantity < 1 {
		quantity = 40
	}

	offset := (page - 1) * quantity

	allQueryTemplate := `
FOR t in Tag
    LIMIT %[1]d, %[2]d
    RETURN t`

	allQuery := fmt.Sprintf(allQueryTemplate, offset, quantity)

	err = db.ValidateQuery(ctx, allQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, err
	}

	cursor, err := db.Query(ctx, allQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, err
	}

	allTags := make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		allTags = append(allTags, &doc)
	}

	return allTags, nil
}
