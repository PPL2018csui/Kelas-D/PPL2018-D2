package tag

import (
	"errors"
	"testing"

	driver "github.com/arangodb/go-driver"
	"github.com/bouk/monkey"
	"github.com/stretchr/testify/assert"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
)

func mockCoreGetEnvFail() (*core.Config, error) {
	return nil, errors.New("this is a mock error")
}

func mockArangoGetDatabaseFail(dbName string) (driver.Database, error) {
	return nil, errors.New("this is a mock error")
}

type (
	InitFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		ExpectedError    error
	}

	AllDocumentsFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		ExpectedError    error
	}
)

func TestInit(t *testing.T) {
	testFibs := map[string]InitFib{
		"Success": InitFib{},
		"Bad core.GetEnv": InitFib{
			PatchGetEnv:   mockCoreGetEnvFail,
			ExpectedError: errors.New("this is a mock error"),
		},
		"Bad arango.GetDatabase": InitFib{
			PatchGetDatabase: mockArangoGetDatabaseFail,
			ExpectedError:    errors.New("this is a mock error"),
		},
	}

	for tc, fib := range testFibs {
		InitTestHelp(t, tc, fib)
	}
}

func InitTestHelp(t *testing.T, tc string, fib InitFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		monkey.Patch(core.GetEnv, fib.PatchGetEnv)
	}

	if fib.PatchGetDatabase != nil {
		monkey.Patch(arango.GetDatabase, fib.PatchGetDatabase)
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	check, err := Init()
	if fib.ExpectedError == nil {
		assert.NotNil(t, check)
	} else {
		assert.Nil(t, check)
	}
	assert.Equal(t, err, fib.ExpectedError)
}

func TestAll(t *testing.T) {
	testFibs := map[string]AllDocumentsFib{
		"Success": AllDocumentsFib{},
		"Bad core.GetEnv": AllDocumentsFib{
			PatchGetEnv:   mockCoreGetEnvFail,
			ExpectedError: errors.New("this is a mock error"),
		},
		"Bad arango.GetDatabase": AllDocumentsFib{
			PatchGetDatabase: mockArangoGetDatabaseFail,
			ExpectedError:    errors.New("this is a mock error"),
		},
	}

	for tc, fib := range testFibs {
		AllDocumentsTestHelp(t, tc, fib)
	}
}

func AllDocumentsTestHelp(t *testing.T, tc string, fib AllDocumentsFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		monkey.Patch(core.GetEnv, fib.PatchGetEnv)
	}

	if fib.PatchGetDatabase != nil {
		monkey.Patch(arango.GetDatabase, fib.PatchGetDatabase)
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	tags, err := AllDocuments(nil, &AllDocumentsParams{})
	if fib.ExpectedError == nil {
		assert.NotNil(t, tags)
	} else {
		assert.Nil(t, tags)
	}
	assert.Equal(t, err, fib.ExpectedError)
}
