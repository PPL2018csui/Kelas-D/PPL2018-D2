package merchant

import (
	"encoding/json"
	"errors"
	"log"
	"testing"

	driver "github.com/arangodb/go-driver"
	"github.com/bouk/monkey"
	"github.com/stretchr/testify/assert"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
)

func mockGetEnv() (*core.Config, error) {
	return nil, errors.New("this is a mock error")
}

func mockGetDatabaseFail(dbName string) (driver.Database, error) {
	return nil, errors.New("this is a mock error")
}

func mockJSONMarshalFail(v interface{}) ([]byte, error) {
	return nil, errors.New("this is a mock error")
}

type (
	ValidateNewMerchantFib struct {
		Merchant         *Document
		ExpectedDocument Document
		ExpectedError    error
	}

	ValidateModifyFilterFib struct {
		Filters       []*Document
		ExpectedError error
	}

	ValidateUpdateDataFib struct {
		Merchant      *Document
		ExpectedError error
	}

	CreateMerchantFib struct {
		PatchGetEnv       func() (*core.Config, error)
		PatchGetDatabase  func(dbName string) (driver.Database, error)
		PatchJSONMarshal  func(v interface{}) ([]byte, error)
		NewMerchants      []*Document
		ExpectedError     error
		ExpectedMerchants []*Document
	}

	GetMerchantFib struct {
		PatchGetEnv       func() (*core.Config, error)
		PatchGetDatabase  func(dbName string) (driver.Database, error)
		PatchJSONMarshal  func(v interface{}) ([]byte, error)
		MerchantFilter    *Document
		Sorting           []string
		Page              int
		Quantity          int
		ExpectedError     error
		ExpectedMerchants []*Document
	}

	UpdateMerchantFib struct {
		PatchGetEnv       func() (*core.Config, error)
		PatchGetDatabase  func(dbName string) (driver.Database, error)
		PatchJSONMarshal  func(v interface{}) ([]byte, error)
		MerchantFilters   []*Document
		NewData           *Document
		ExpectedError     error
		ExpectedMerchants []*Document
	}

	DeleteMerchantFib struct {
		PatchGetEnv       func() (*core.Config, error)
		PatchGetDatabase  func(dbName string) (driver.Database, error)
		PatchJSONMarshal  func(v interface{}) ([]byte, error)
		MerchantFilters   []*Document
		ExpectedError     error
		ExpectedMerchants []*Document
	}
)

func TestValidateNewMerchant(t *testing.T) {
	name := "carash"
	location := [2]float64{123.45, 541.23}
	rating := 5.0

	err := errors.New("Invalid new merchant, fields (name, location) must be filled")

	testFibs := map[string]ValidateNewMerchantFib{
		"tc1": ValidateNewMerchantFib{
			Merchant: &Document{
				Name:     &name,
				Location: &location,
			},
			ExpectedDocument: Document{
				Name:     &name,
				Location: &location,
				Rating:   &DefaultRating,
			},
		},
		"tc2": ValidateNewMerchantFib{
			Merchant: &Document{
				Name:     &name,
				Location: &location,
				Rating:   &rating,
			},
			ExpectedDocument: Document{
				Name:     &name,
				Location: &location,
				Rating:   &rating,
			},
		},
		"fail1": ValidateNewMerchantFib{
			Merchant:         &Document{},
			ExpectedDocument: Document{},
			ExpectedError:    err,
		},
		"fail2": ValidateNewMerchantFib{
			Merchant: &Document{
				Name: &name,
			},
			ExpectedDocument: Document{
				Name: &name,
			},
			ExpectedError: err,
		},
		"fail3": ValidateNewMerchantFib{
			Merchant: &Document{
				Location: &location,
			},
			ExpectedDocument: Document{
				Location: &location,
			},
			ExpectedError: err,
		},
	}

	for tc, fib := range testFibs {
		ValidateNewMerchantTestHelp(t, tc, fib)
	}
}

func ValidateNewMerchantTestHelp(t *testing.T, tc string, fib ValidateNewMerchantFib) {
	err := ValidateNewMerchant(fib.Merchant)
	assert.Equal(t, fib.ExpectedDocument, *fib.Merchant)
	assert.Equal(t, fib.ExpectedError, err)
}

func TestValidateModifyFilter(t *testing.T) {
	name := "carash"

	errCount := errors.New("Invalid modify filter, there must be at least 1 filter")
	errField := errors.New("Invalid modify filter, field (_key) must be explicitly defined")

	testFibs := map[string]ValidateModifyFilterFib{
		"tc1": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"tc2": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key: "1234",
				},
				&Document{
					Key: "2345",
				},
				&Document{
					Key: "3456",
				},
			},
		},
		"tc3": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key:  "4567",
					Name: &name,
				},
				&Document{
					Key:  "5678",
					Name: &name,
				},
				&Document{
					Key:  "6789",
					Name: &name,
				},
				&Document{
					Key: "7890",
				},
			},
		},
		"fail1": ValidateModifyFilterFib{
			Filters:       []*Document{},
			ExpectedError: errCount,
		},
		"fail2": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{},
			},
			ExpectedError: errField,
		},
		"fail3": ValidateModifyFilterFib{
			Filters: []*Document{
				&Document{
					Key:  "1234",
					Name: &name,
				},
				&Document{},
			},
			ExpectedError: errField,
		},
	}

	for tc, fib := range testFibs {
		ValidateModifyFilterTestHelp(t, tc, fib)
	}
}

func ValidateModifyFilterTestHelp(t *testing.T, tc string, fib ValidateModifyFilterFib) {
	err := ValidateModifyFilter(fib.Filters)
	assert.Equal(t, fib.ExpectedError, err)
}

func TestValidateUpdateData(t *testing.T) {
	name := "carash"
	rating := float64(5.0)

	err := errors.New("Invalid update data, fields (_key, rating) may not be updated manually")

	testFibs := map[string]ValidateUpdateDataFib{
		"tc1": ValidateUpdateDataFib{
			Merchant: &Document{},
		},
		"tc2": ValidateUpdateDataFib{
			Merchant: &Document{
				Name: &name,
			},
		},
		"fail1": ValidateUpdateDataFib{
			Merchant: &Document{
				Key:    "1234",
				Rating: &rating,
			},
			ExpectedError: err,
		},
		"fail2": ValidateUpdateDataFib{
			Merchant: &Document{
				Key:    "2345",
				Name:   &name,
				Rating: &rating,
			},
			ExpectedError: err,
		},
		"fail3": ValidateUpdateDataFib{
			Merchant: &Document{
				Rating: &rating,
			},
			ExpectedError: err,
		},
	}

	for tc, fib := range testFibs {
		ValidateUpdateDataTestHelp(t, tc, fib)
	}
}

func ValidateUpdateDataTestHelp(t *testing.T, tc string, fib ValidateUpdateDataFib) {
	err := ValidateUpdateData(fib.Merchant)
	assert.Equal(t, fib.ExpectedError, err)
}

func TestCreateMerchant(t *testing.T) {
	name := "carash"
	location := [2]float64{123.45, 543.21}
	rating := float64(5)

	testFibs := map[string]CreateMerchantFib{
		"Success": CreateMerchantFib{
			NewMerchants: []*Document{
				&Document{
					Name:     &name,
					Location: &location,
					Rating:   &rating,
				},
			},
		},
		"Bad core.GetEnv": CreateMerchantFib{
			PatchGetEnv:  mockGetEnv,
			NewMerchants: make([]*Document, 0),
		},
		"Bad arango.GetDatabase": CreateMerchantFib{
			PatchGetDatabase: mockGetDatabaseFail,
			NewMerchants:     make([]*Document, 0),
		},
		"Bad json.Marshal": CreateMerchantFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			NewMerchants:     make([]*Document, 0),
		},
	}

	for tc, fib := range testFibs {
		CreateMerchantTestHelp(t, tc, fib)
	}
}

func CreateMerchantTestHelp(t *testing.T, tc string, fib CreateMerchantFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data (test)
	docs, err := CreateMerchant(nil, &CreateDocumentParams{
		Documents: fib.NewMerchants,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docs)
		assert.NotNil(t, err)
		return
	}
	assert.Nil(t, err)

	// assert return value
	docs = docs
	// TODO assert values in docs

	// assert database values
	// TODO direct RETURN query to database

	// cleanup the data
	CleanUpDatabase(t)
}

func TestGetMerchant(t *testing.T) {
	testFibs := map[string]GetMerchantFib{
		"temp": GetMerchantFib{
			MerchantFilter: &Document{},
		},
		"Bad core.GetEnv": GetMerchantFib{
			PatchGetEnv:    mockGetEnv,
			MerchantFilter: &Document{},
		},
		"Bad arango.GetDatabase": GetMerchantFib{
			PatchGetDatabase: mockGetDatabaseFail,
			MerchantFilter:   &Document{},
		},
		"Bad json.Marshal": GetMerchantFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			MerchantFilter:   &Document{},
		},
	}

	for tc, fib := range testFibs {
		GetMerchantTestHelp(t, tc, fib)
	}
}

func GetMerchantTestHelp(t *testing.T, tc string, fib GetMerchantFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data
	// TODO direct INSERT query to database

	// get database values (test)
	docs, err := GetMerchant(nil, &GetDocumentParams{
		Filter:   fib.MerchantFilter,
		Sorting:  fib.Sorting,
		Page:     fib.Page,
		Quantity: fib.Quantity,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docs)
		assert.NotNil(t, err)
		return
	}
	assert.Nil(t, err)

	// assert return values
	// TODO assert values in docs

	// cleanup the data
	CleanUpDatabase(t)
}

func TestUpdateMerchant(t *testing.T) {
	testFibs := map[string]UpdateMerchantFib{
		"temp": UpdateMerchantFib{
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
		"Bad core.GetEnv": UpdateMerchantFib{
			PatchGetEnv: mockGetEnv,
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
		"Bad arango.GetDatabase": UpdateMerchantFib{
			PatchGetDatabase: mockGetDatabaseFail,
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
		"Bad json.Marshal": UpdateMerchantFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
			NewData: &Document{},
		},
	}

	for tc, fib := range testFibs {
		UpdateMerchantTestHelp(t, tc, fib)
	}
}

func UpdateMerchantTestHelp(t *testing.T, tc string, fib UpdateMerchantFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data
	// TODO direct INSERT query to database

	// update database values (test)
	docModResps, err := UpdateMerchant(nil, &UpdateDocumentsParams{
		Filters: fib.MerchantFilters,
		NewData: fib.NewData,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docModResps)
		assert.NotNil(t, err)
		return
	}
	assert.Nil(t, err)

	// assert return values
	// TODO assert values in docModResps

	// assert database values
	// TODO direct RETURN query to database

	// cleanup the data
	CleanUpDatabase(t)
}

func TestDeleteMerchant(t *testing.T) {
	testFibs := map[string]DeleteMerchantFib{
		"temp": DeleteMerchantFib{
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"Bad core.GetEnv": DeleteMerchantFib{
			PatchGetEnv: mockGetEnv,
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"Bad arango.GetDatabase": DeleteMerchantFib{
			PatchGetDatabase: mockGetDatabaseFail,
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
		"Bad json.Marshal": DeleteMerchantFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			MerchantFilters: []*Document{
				&Document{
					Key: "1234",
				},
			},
		},
	}

	for tc, fib := range testFibs {
		DeleteMerchantTestHelp(t, tc, fib)
	}
}

func DeleteMerchantTestHelp(t *testing.T, tc string, fib DeleteMerchantFib) {
	// monkeypatch
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer func() {

	}()

	// insert new data
	// TODO direct INSERT query to database

	// delete database values (test)
	docs, err := DeleteMerchant(nil, &DeleteDocumentParams{
		Filters: fib.MerchantFilters,
	})
	if fib.ExpectedError != nil {
		assert.Nil(t, docs)
		assert.NotNil(t, err)
		return
	}
	assert.Nil(t, err)

	// assert return values
	// TODO assert values in docs

	// assert database values
	// TODO direct RETURN query to database (check if the data still exists)

	// cleanup the data
	CleanUpDatabase(t)
}

func CleanUpDatabase(t *testing.T) {
	db, err := arango.GetDatabase("abeona")
	if err != nil {
		log.Println("Get Database", err)
		return
	}

	cleanUpQuery := `
FOR p IN Merchant
	FILTER p.other == "asd"
	REMOVE p IN Merchant
	RETURN p`

	cursor, err := db.Query(nil, cleanUpQuery, nil)
	t.Log(cursor.Count())
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		t.Log(doc)
	}
}

// func TestCreateMerchant(t *testing.T) {
// 	testFibs := map[string]CreateMerchantFib{}
//
// 	CreateMerchant(nil)
// 	for tc, fib := range testFibs {
// 		CreateMerchantTestHelp(t, tc, fib)
// 	}
// }
//
// func CreateMerchantTestHelp(t *testing.T, tc string, fib CreateMerchantFib) {
// 	// TODO implement
// }
//
// func TestGetMerchant(t *testing.T) {
// 	testFibs := map[string]GetMerchantFib{}
//
// 	GetMerchant(nil)
// 	for tc, fib := range testFibs {
// 		GetMerchantTestHelp(t, tc, fib)
// 	}
// }
//
// func GetMerchantTestHelp(t *testing.T, tc string, fib GetMerchantFib) {
// 	// TODO implement
// }
//
// func TestUpdateMerchant(t *testing.T) {
// 	testFibs := map[string]UpdateMerchantFib{}
//
// 	UpdateMerchant(nil, nil)
// 	for tc, fib := range testFibs {
// 		UpdateMerchantTestHelp(t, tc, fib)
// 	}
// }
//
// func UpdateMerchantTestHelp(t *testing.T, tc string, fib UpdateMerchantFib) {
// 	// TODO implement
// }
//
// func TestDeleteMerchant(t *testing.T) {
// 	testFibs := map[string]DeleteMerchantFib{}
//
// 	DeleteMerchant(nil)
// 	for tc, fib := range testFibs {
// 		DeleteMerchantTestHelp(t, tc, fib)
// 	}
// }
//
// func DeleteMerchantTestHelp(t *testing.T, tc string, fib DeleteMerchantFib) {
// 	// TODO implement
// }
