package merchant

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/util"
)

type (
	// Document represents mechant document in arangodb
	Document struct {
		Key      string      `json:"_key,omitempty" db:"_key"`
		Name     *string     `json:"name,omitempty" db:"name"`
		Location *[2]float64 `json:"location,omitempty" db:"location"`
		Rating   *float64    `json:"rating,omitempty" db:"rating"`

		Other string `json:"other,omitempty" db:"other"`
	}

	// ModificationResponse contains old and new merchant documents to compare
	ModificationResponse struct {
		Old *Document `json:"old"`
		New *Document `json:"new"`
	}

	// CreateDocumentParams contains new merchant documents
	CreateDocumentParams struct {
		Documents []*Document
	}

	// GetDocumentParams contains filter, sorting, page and quantity
	GetDocumentParams struct {
		Filter   *Document
		Sorting  []string
		Page     int
		Quantity int
	}

	// UpdateDocumentsParams contains filter and new merchant document
	UpdateDocumentsParams struct {
		Filters []*Document
		NewData *Document
	}

	// DeleteDocumentParams contains filter
	DeleteDocumentParams struct {
		Filters []*Document
	}
)

var (
	// MerchantAbbreviation is string with value "m"
	MerchantAbbreviation = "m"
	// MerchantCollection is string with value "Merchant"
	MerchantCollection = "Merchant"
	// SortableMerchantAttr is list of field that can be use for sorting
	SortableMerchantAttr = []string{"_key", "name", "rating"}
	// DefaultRating is default value for merchant's rating
	DefaultRating = 0.0
)

// ValidateNewMerchant validates important fields in the new Merchant Document
func ValidateNewMerchant(merchant *Document) error {
	if merchant.Name == nil || merchant.Location == nil {
		return errors.New("Invalid new merchant, fields (name, location) must be filled")
	}

	if merchant.Rating == nil {
		merchant.Rating = &DefaultRating
	}

	return nil
}

// ValidateModifyFilter validates modify filter
func ValidateModifyFilter(filters []*Document) error {
	if len(filters) == 0 {
		return errors.New("Invalid modify filter, there must be at least 1 filter")
	}

	for _, merchant := range filters {
		if merchant.Key == "" {
			return errors.New("Invalid modify filter, field (_key) must be explicitly defined")
		}
	}

	return nil
}

// ValidateUpdateData validates update data
func ValidateUpdateData(merchant *Document) error {
	if merchant.Key != "" || merchant.Rating != nil {
		return errors.New("Invalid update data, fields (_key, rating) may not be updated manually")
	}

	return nil
}

// CreateMerchant creates new documents in arangodb
// receives list of merchants to be created
// generates documents in arangodb
// returns list of successfully created merchants
func CreateMerchant(ctx context.Context, params *CreateDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	docsJSON, err := json.Marshal(params.Documents)
	if err != nil {
		log.Println("Marshal Documents", err)
		return nil, nil
	}

	// Generate the final query
	insertQuery := fmt.Sprintf(arango.InsertQueryTemplate, MerchantCollection, docsJSON)

	err = db.ValidateQuery(nil, insertQuery)
	if err != nil {
		log.Println("Validate Query", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, insertQuery, nil)
	if err != nil {
		log.Println("Execute Query", err)
		return nil, nil
	}
	defer cursor.Close()

	var insertedMerchants = make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Document", err)
				continue
			}
		}

		doc.Key = meta.Key
		insertedMerchants = append(insertedMerchants, &doc)
	}

	return insertedMerchants, nil
}

// GetMerchant returns documents in arangodb
// receives filter for merchants
// fetches documents from arangodb
// returns list of merchants matching filter
func GetMerchant(ctx context.Context, params *GetDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse FILTER section for the final query
	filterJSON, err := json.Marshal(params.Filter)
	if err != nil {
		log.Println("Marshal Filter", err)
		return nil, nil
	}

	page := params.Page
	if page < 1 {
		page = 1
	}

	quantity := params.Quantity
	if quantity < 1 {
		quantity = 5
	}

	offset := (page - 1) * quantity

	// Generate the final query
	var returnQuery string
	if quantity < 0 {
		returnQuery = fmt.Sprintf(arango.ReturnQueryTemplate, MerchantAbbreviation, MerchantCollection, filterJSON, offset, quantity)
	} else {
		// Calculate sorting and offset
		sorting := util.IntersectString(params.Sorting, SortableMerchantAttr)
		if len(sorting) == 0 {
			sorting = append(sorting, "_key")
		}
		sortQuery := arango.GenerateSortQuery(arango.SortQueryStruct{
			CollAbbr:  MerchantAbbreviation,
			AttrNames: sorting,
		})

		returnQuery = fmt.Sprintf(arango.ReturnQueryMatchPaginateTemplate, MerchantAbbreviation, MerchantCollection, filterJSON, sortQuery, offset, quantity)
	}

	err = db.ValidateQuery(nil, returnQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, returnQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, nil
	}

	var returnedMerchants = make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		returnedMerchants = append(returnedMerchants, &doc)
	}

	return returnedMerchants, nil
}

// UpdateMerchant updates documents in arangodb
// receives filter for affected merchants and fields to be replaces along with the data
// updates documents in arangodb
// returns a modification comparison
func UpdateMerchant(ctx context.Context, params *UpdateDocumentsParams) ([]*ModificationResponse, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse FILTER section for the final query
	var filterStrings = make([]string, 0)
	for _, filter := range params.Filters {
		filterJSON, err := json.Marshal(filter)
		if err != nil {
			log.Println("Marshal Filter", err)
			return nil, nil
		}
		filterStrings = append(filterStrings, string(filterJSON))
	}

	filterMatches := arango.GenerateMatchesQuery(arango.MatchesQueryStruct{
		CollAbbr:   MerchantAbbreviation,
		MatchJSONs: filterStrings,
		Relation:   "||",
	})

	// Parse WITH section for the final query
	withJSON, err := json.Marshal(params.NewData)
	if err != nil {
		log.Println("Marshal Data", err)
		return nil, nil
	}

	// Generate the final query
	updateQuery := fmt.Sprintf(arango.UpdateQueryFilterTemplate, MerchantAbbreviation, MerchantCollection, filterMatches, withJSON)

	err = db.ValidateQuery(nil, updateQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, updateQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, nil
	}

	var updatedMerchants = make([]*ModificationResponse, 0)
	for {
		var doc ModificationResponse
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Old.Key = meta.Key
		doc.New.Key = meta.Key
		updatedMerchants = append(updatedMerchants, &doc)
	}

	return updatedMerchants, nil
}

// DeleteMerchant deletes documents in arangodb
// receives filter for merchants to be deleted
// deletes documents from arangodb collection
// returns a list of deleted merchants
func DeleteMerchant(ctx context.Context, params *DeleteDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	// Parse FILTER section for the final query
	var filterStrings = make([]string, 0)
	for _, filter := range params.Filters {
		filterJSON, err := json.Marshal(filter)
		if err != nil {
			log.Println("Marshal Filter", err)
			return nil, nil
		}
		filterStrings = append(filterStrings, string(filterJSON))
	}

	filterMatches := arango.GenerateMatchesQuery(arango.MatchesQueryStruct{
		CollAbbr:   MerchantAbbreviation,
		MatchJSONs: filterStrings,
		Relation:   "||",
	})

	// Generate the final query
	removeQuery := fmt.Sprintf(arango.RemoveQueryTemplate, MerchantAbbreviation, MerchantCollection, filterMatches)

	err = db.ValidateQuery(nil, removeQuery)
	if err != nil {
		log.Println("Validate Query:", err)
		return nil, nil
	}

	// Run the query
	cursor, err := db.Query(nil, removeQuery, nil)
	if err != nil {
		log.Println("Execute Query:", err)
		return nil, nil
	}

	var removedMerchants = make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Cursor:", err)
				continue
			}
		}

		doc.Key = meta.Key
		removedMerchants = append(removedMerchants, &doc)
	}

	return removedMerchants, nil
}
