package user

import (
	"encoding/json"
	"errors"
	"testing"

	driver "github.com/arangodb/go-driver"
	"github.com/bouk/monkey"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
)

func mockGetEnv() (*core.Config, error) {
	return nil, errors.New("this is a mock error")
}

func mockGetDatabaseFail(dbName string) (driver.Database, error) {
	return nil, errors.New("this is a mock error")
}

func mockJSONMarshalFail(v interface{}) ([]byte, error) {
	return nil, errors.New("this is a mock error")
}

type (
	CreateUserFib struct {
		PatchGetEnv      func() (*core.Config, error)
		PatchGetDatabase func(dbName string) (driver.Database, error)
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		NewUsers         []*Document
		ExpectedError    error
		ExpectedUsers    []*Document
	}

	OnboardUserFib struct {
	}
)

func TestCreateUser(t *testing.T) {
	firebaseID := "spectrum"
	dName := "disorder"
	pRange := Float64Range{
		Min: 0.0,
		Max: 0.0,
	}
	pTags := make(map[string]float64)

	testFibs := map[string]CreateUserFib{
		"Success1": CreateUserFib{
			NewUsers: make([]*Document, 0),
		},
		"Success2": CreateUserFib{
			NewUsers: []*Document{
				&Document{
					FirebaseID:    &firebaseID,
					DisplayName:   &dName,
					PriceRange:    &pRange,
					PreferredTags: &pTags,
				},
			},
		},
		"Bad core.GetEnv": CreateUserFib{
			PatchGetEnv: mockGetEnv,
			NewUsers:    make([]*Document, 0),
		},
		"Bad arango.GetDatabase": CreateUserFib{
			PatchGetDatabase: mockGetDatabaseFail,
			NewUsers:         make([]*Document, 0),
		},
		"Bad json.Marshal": CreateUserFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			NewUsers:         make([]*Document, 0),
		},
	}

	for tc, fib := range testFibs {
		CreateUserTestHelp(t, tc, fib)
	}
}

func CreateUserTestHelp(t *testing.T, tc string, fib CreateUserFib) {
	if fib.PatchGetEnv != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(core.GetEnv, func() (*core.Config, error) {
			defer guard.Unpatch()
			return fib.PatchGetEnv()
		})
	}

	if fib.PatchGetDatabase != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(arango.GetDatabase, func(dbName string) (driver.Database, error) {
			defer guard.Unpatch()
			return fib.PatchGetDatabase(dbName)
		})
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}
}
