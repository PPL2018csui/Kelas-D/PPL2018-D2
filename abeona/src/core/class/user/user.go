package user

import (
	"context"
	"encoding/json"
	"fmt"
	"log"

	driver "github.com/arangodb/go-driver"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
	product_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/product"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/database/arango"
)

type (
	Float64Range struct {
		Min float64 `json:"min"`
		Max float64 `json:"max"`
	}

	Document struct {
		Key           string              `json:"_key,omitempty"`
		FirebaseID    *string             `json:"firebase_id,omitempty"`
		DisplayName   *string             `json:"display_name,omitempty"`
		PriceRange    *Float64Range       `json:"price_range,omitempty"`
		PreferredTags *map[string]float64 `json:"preferred_tags,omitempty"`
	}

	ProductSelectionPair struct {
		Product   *product_obj.Document `json:"product"`
		Preferred bool                  `json:"preferred"`
	}

	CheckParams struct {
		FirebaseId string
	}

	OnboardParams struct {
		FirebaseID     string                  `json:"firebase_id"`
		PreferenceList []*ProductSelectionPair `json:"preference"`
	}

	CreateDocumentParams struct {
		Documents []*Document
	}
)

var (
	UserAbbreviation string = "u"
	UserCollection   string = "User"

	DefaultPriceRange = Float64Range{
		Min: 0.0,
		Max: 0.0,
	}

	DefaultDocument Document = Document{
		PriceRange: &DefaultPriceRange,
	}
)

func CheckUser(ctx context.Context, params *CheckParams) (check bool, err error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return check, err
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return check, err
	}

	checkQuery := `
FOR u IN User
	FILTER u.firebase_id == @firebaseId
	RETURN u`

	err = db.ValidateQuery(nil, checkQuery)
	if err != nil {
		log.Println("Validate Query", err)
		return check, err
	}

	cursor, err := db.Query(nil, checkQuery, map[string]interface{}{"firebaseId": params.FirebaseId})
	if err != nil {
		log.Println("Execute Query", err)
		return check, err
	}
	defer cursor.Close()

	if cursor.HasMore() {
		check = true
	}

	return check, nil
}

func CreateUser(ctx context.Context, params *CreateDocumentParams) ([]*Document, error) {
	env, err := core.GetEnv()
	if err != nil {
		log.Println("Get Env", err)
		return nil, nil
	}

	db, err := arango.GetDatabase(env.Arango.Database)
	if err != nil {
		log.Println("Get Database", err)
		return nil, nil
	}

	docsJson, err := json.Marshal(params.Documents)
	if err != nil {
		log.Println("Marshal Documents", err)
		return nil, nil
	}

	insertQuery := fmt.Sprintf(arango.InsertQueryTemplate, UserCollection, docsJson)

	err = db.ValidateQuery(nil, insertQuery)
	if err != nil {
		log.Println("Validate Query", err)
		return nil, nil
	}

	cursor, err := db.Query(nil, insertQuery, nil)
	if err != nil {
		log.Println("Execute Query", err)
		return nil, nil
	}
	defer cursor.Close()

	insertedUsers := make([]*Document, 0)
	for {
		var doc Document
		meta, err := cursor.ReadDocument(nil, &doc)
		if err != nil {
			if driver.IsNoMoreDocuments(err) {
				break
			} else {
				log.Println("Read Document", err)
				continue
			}
		}

		doc.Key = meta.Key
		insertedUsers = append(insertedUsers, &doc)
	}

	return insertedUsers, nil
}

func OnboardUser(ctx context.Context, params *OnboardParams) (*Document, error) {
	tagPref := make(map[string]float64)

	// TODO: Formula

	newUser := &Document{
		FirebaseID:  &params.FirebaseID,
		DisplayName: &params.FirebaseID,
		PriceRange: &Float64Range{
			Min: 0.0,
			Max: 0.0,
		},
		PreferredTags: &tagPref,
	}

	docSlice, err := CreateUser(ctx, &CreateDocumentParams{Documents: []*Document{newUser}})
	if err != nil {
		// handle
		return nil, nil
	}

	if len(docSlice) != 1 {
		// handle
		log.Println("Can only create 1 user")
		return nil, nil
	}

	return docSlice[0], nil
}
