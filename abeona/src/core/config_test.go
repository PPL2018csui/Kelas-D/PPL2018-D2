package core

import (
	"errors"
	"os"
	"strconv"
	"testing"

	"github.com/bouk/monkey"
	"github.com/joeshaw/envdecode"
	"github.com/stretchr/testify/assert"
)

func mockEnvdecodeDecodeFail(target interface{}) error {
	return errors.New("this is a mock error")
}

type (
	GetEnvFib struct {
		PatchEnvdecodeDecode func(target interface{}) error
		EnvKey               string
		EnvVal               interface{}
	}
)

func TestGetEnv(t *testing.T) {
	testFibs := map[string]GetEnvFib{
		"String1": GetEnvFib{
			EnvKey: "TEST_STRING",
			EnvVal: "asd",
		},
		"String2": GetEnvFib{
			EnvKey: "TEST_STRING",
			EnvVal: "test",
		},
		"Int1": GetEnvFib{
			EnvKey: "TEST_INT",
			EnvVal: 1,
		},
		"Int2": GetEnvFib{
			EnvKey: "TEST_INT",
			EnvVal: 100,
		},
		"Float1": GetEnvFib{
			EnvKey: "TEST_FLOAT",
			EnvVal: float64(1.0),
		},
		"Float2": GetEnvFib{
			EnvKey: "TEST_FLOAT",
			EnvVal: float64(1.65),
		},
		"Bool1": GetEnvFib{
			EnvKey: "TEST_BOOL",
			EnvVal: true,
		},
		"Bool2": GetEnvFib{
			EnvKey: "TEST_BOOL",
			EnvVal: false,
		},
		"Fail": GetEnvFib{
			PatchEnvdecodeDecode: mockEnvdecodeDecodeFail,
			EnvKey:               "FAIL",
			EnvVal:               nil,
		},
	}

	for tc, fib := range testFibs {
		GetEnvTestHelp(t, tc, fib)
	}
}

func GetEnvTestHelp(t *testing.T, tc string, fib GetEnvFib) {
	if fib.PatchEnvdecodeDecode != nil {
		monkey.Patch(envdecode.Decode, fib.PatchEnvdecodeDecode)
	}

	defer monkey.UnpatchAll()

	var check interface{}
	switch fib.EnvKey {
	case "TEST_STRING":
		os.Setenv(fib.EnvKey, fib.EnvVal.(string))
		env, _ := GetEnv()
		check = env.Test.String
	case "TEST_INT":
		os.Setenv(fib.EnvKey, strconv.Itoa(fib.EnvVal.(int)))
		env, _ := GetEnv()
		check = env.Test.Int
	case "TEST_FLOAT":
		os.Setenv(fib.EnvKey, strconv.FormatFloat(fib.EnvVal.(float64), 'f', 6, 64))
		env, _ := GetEnv()
		check = env.Test.Float
	case "TEST_BOOL":
		os.Setenv(fib.EnvKey, strconv.FormatBool(fib.EnvVal.(bool)))
		env, _ := GetEnv()
		check = env.Test.Bool
	case "FAIL":
		_, err := GetEnv()
		assert.Equal(t, errors.New("this is a mock error"), err)
	}

	assert.Equal(t, fib.EnvVal, check)
}
