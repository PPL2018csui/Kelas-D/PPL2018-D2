package admin

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	"github.com/appleboy/gofight"
	"github.com/bouk/monkey"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	merchant_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/merchant"
	product_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/product"
)

func GinEngine() *gin.Engine {
	gin.SetMode(gin.TestMode)
	r := gin.New()

	r.POST("/product/crud", HandleProductCreate)
	r.GET("/product/crud", HandleProductRead)
	r.GET("/product/crud/:_key", HandleProductDetail)
	r.PUT("/product/crud", HandleProductUpdate)
	r.DELETE("/product/crud", HandleProductDelete)

	r.POST("/merchant/crud", HandleMerchantCreate)
	r.GET("/merchant/crud", HandleMerchantRead)
	r.PUT("/merchant/crud", HandleMerchantUpdate)
	r.DELETE("/merchant/crud", HandleMerchantDelete)

	return r
}

func mockIoutilReadallFail(r io.Reader) ([]byte, error) {
	return nil, errors.New("this is a mock error")
}

func mockJSONMarshalFail(v interface{}) ([]byte, error) {
	return nil, errors.New("this is a mock error")
}

func mockJSONUnmarshalFail(data []byte, v interface{}) error {
	return errors.New("this is a mock error")
}

func mockGinRequestParseFormFail(r *http.Request) error {
	return errors.New("this is a mock error")
}

func mockProductValidateNewProductFail(product *product_obj.Document) error {
	return errors.New("this is a mock error")
}

func mockProductValidateModifyFilterFail(filters []*product_obj.Document) error {
	return errors.New("this is a mock error")
}

func mockProductValidateUpdateDataFail(product *product_obj.Document) error {
	return errors.New("this is a mock error")
}

func mockProductCreateProductSucc(ctx context.Context, params *product_obj.CreateDocumentParams) ([]*product_obj.Document, error) {
	return params.Documents, nil
}

func mockProductCreateProductFail(ctx context.Context, params *product_obj.CreateDocumentParams) ([]*product_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockProductGetProductSucc(ctx context.Context, params *product_obj.GetDocumentParams) ([]*product_obj.Document, error) {
	name := "foobar"
	category := "test"
	isAvailable := true
	price := float64(10000)
	description := "lorem ipsum"
	rating := float64(5)

	var ret []*product_obj.Document
	if params.Filter.Category != nil {
		ret = []*product_obj.Document{
			&product_obj.Document{
				Key:         "1234",
				Name:        &name,
				IsAvailable: &isAvailable,
				Price:       &price,
				Category:    &category,
				Description: &description,
				Rating:      &rating,
			},
		}
	} else {
		ret = []*product_obj.Document{}
	}

	return ret, nil
}

func mockProductGetProductFail(ctx context.Context, params *product_obj.GetDocumentParams) ([]*product_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockProductUpdateProductSucc(ctx context.Context, params *product_obj.UpdateDocumentsParams) ([]*product_obj.ModificationResponse, error) {
	name := "foobar"
	category := "test"
	isAvailable := true
	price := float64(10000)
	description := "lorem ipsum"
	rating := float64(5)

	var ret []*product_obj.ModificationResponse
	if len(params.Filters) == 1 {
		ret = []*product_obj.ModificationResponse{
			&product_obj.ModificationResponse{
				Old: &product_obj.Document{
					Key:         "1234",
					Name:        &name,
					IsAvailable: &isAvailable,
					Price:       &price,
					Category:    &category,
					Description: &description,
					Rating:      &rating,
				},
				New: &product_obj.Document{
					Key:         "1234",
					Name:        &name,
					IsAvailable: &isAvailable,
					Price:       params.NewData.Price,
					Category:    &category,
					Description: &description,
					Rating:      &rating,
				},
			},
		}
	} else if len(params.Filters) == 2 {
		ret = []*product_obj.ModificationResponse{
			&product_obj.ModificationResponse{
				Old: &product_obj.Document{
					Key:         "1234",
					Name:        &name,
					IsAvailable: &isAvailable,
					Price:       &price,
					Category:    &category,
					Description: &description,
					Rating:      &rating,
				},
				New: &product_obj.Document{
					Key:         "1234",
					Name:        &name,
					IsAvailable: &isAvailable,
					Price:       params.NewData.Price,
					Category:    &category,
					Description: &description,
					Rating:      &rating,
				},
			},
			&product_obj.ModificationResponse{
				Old: &product_obj.Document{
					Key:         "2345",
					Name:        &name,
					IsAvailable: &isAvailable,
					Price:       &price,
					Category:    &category,
					Description: &description,
					Rating:      &rating,
				},
				New: &product_obj.Document{
					Key:         "2345",
					Name:        &name,
					IsAvailable: &isAvailable,
					Price:       params.NewData.Price,
					Category:    &category,
					Description: &description,
					Rating:      &rating,
				},
			},
		}
	} else {
		ret = []*product_obj.ModificationResponse{}
	}

	return ret, nil
}

func mockProductUpdateProductFail(ctx context.Context, params *product_obj.UpdateDocumentsParams) ([]*product_obj.ModificationResponse, error) {
	return nil, errors.New("this is a mock error")
}

func mockProductDeleteProductSucc(ctx context.Context, params *product_obj.DeleteDocumentParams) ([]*product_obj.Document, error) {
	name := "foobar"
	category := "test"
	isAvailable := true
	price := float64(10000)
	description := "lorem ipsum"
	rating := float64(5)

	var ret []*product_obj.Document
	if len(params.Filters) == 1 {
		ret = []*product_obj.Document{
			&product_obj.Document{
				Key:         "1234",
				Name:        &name,
				IsAvailable: &isAvailable,
				Price:       &price,
				Category:    &category,
				Description: &description,
				Rating:      &rating,
			},
		}
	} else if len(params.Filters) == 2 {
		ret = []*product_obj.Document{
			&product_obj.Document{
				Key:         "1234",
				Name:        &name,
				IsAvailable: &isAvailable,
				Price:       &price,
				Category:    &category,
				Description: &description,
				Rating:      &rating,
			},
			&product_obj.Document{
				Key:         "2345",
				Name:        &name,
				IsAvailable: &isAvailable,
				Price:       &price,
				Category:    &category,
				Description: &description,
				Rating:      &rating,
			},
		}
	} else {
		ret = []*product_obj.Document{}
	}

	return ret, nil
}

func mockProductDeleteProductFail(ctx context.Context, params *product_obj.DeleteDocumentParams) ([]*product_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockMerchantValidateNewMerchantFail(merchant *merchant_obj.Document) error {
	return errors.New("this is a mock error")
}

func mockMerchantValidateModifyFilterFail(filters []*merchant_obj.Document) error {
	return errors.New("this is a mock error")
}

func mockMerchantValidateUpdateDataFail(pmerchant *merchant_obj.Document) error {
	return errors.New("this is a mock error")
}

func mockMerchantCreateMerchantSucc(ctx context.Context, params *merchant_obj.CreateDocumentParams) ([]*merchant_obj.Document, error) {
	return params.Documents, nil
}

func mockMerchantCreateMerchantFail(ctx context.Context, params *merchant_obj.CreateDocumentParams) ([]*merchant_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockMerchantGetMerchantSucc(ctx context.Context, params *merchant_obj.GetDocumentParams) ([]*merchant_obj.Document, error) {
	name := "foobar"
	location := [2]float64{123.45, 451.23}
	rating := float64(5)

	var ret []*merchant_obj.Document
	ret = []*merchant_obj.Document{
		&merchant_obj.Document{
			Key:      "1234",
			Name:     &name,
			Location: &location,
			Rating:   &rating,
		},
	}

	return ret, nil
}

func mockMerchantGetMerchantFail(ctx context.Context, params *merchant_obj.GetDocumentParams) ([]*merchant_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockMerchantUpdateMerchantSucc(ctx context.Context, params *merchant_obj.UpdateDocumentsParams) ([]*merchant_obj.ModificationResponse, error) {
	name := "foobar"
	location := [2]float64{123.45, 451.23}
	rating := float64(5)

	var ret []*merchant_obj.ModificationResponse
	if len(params.Filters) == 1 {
		ret = []*merchant_obj.ModificationResponse{
			&merchant_obj.ModificationResponse{
				Old: &merchant_obj.Document{
					Key:      "1234",
					Name:     &name,
					Location: &location,
					Rating:   &rating,
				},
				New: &merchant_obj.Document{
					Key:      "1234",
					Name:     &name,
					Location: params.NewData.Location,
					Rating:   &rating,
				},
			},
		}
	} else if len(params.Filters) == 2 {
		ret = []*merchant_obj.ModificationResponse{
			&merchant_obj.ModificationResponse{
				Old: &merchant_obj.Document{
					Key:      "1234",
					Name:     &name,
					Location: &location,
					Rating:   &rating,
				},
				New: &merchant_obj.Document{
					Key:      "1234",
					Name:     &name,
					Location: params.NewData.Location,
					Rating:   &rating,
				},
			},
			&merchant_obj.ModificationResponse{
				Old: &merchant_obj.Document{
					Key:      "2345",
					Name:     &name,
					Location: &location,
					Rating:   &rating,
				},
				New: &merchant_obj.Document{
					Key:      "2345",
					Name:     &name,
					Location: params.NewData.Location,
					Rating:   &rating,
				},
			},
		}
	} else {
		ret = []*merchant_obj.ModificationResponse{}
	}

	return ret, nil
}

func mockMerchantUpdateMerchantFail(ctx context.Context, params *merchant_obj.UpdateDocumentsParams) ([]*merchant_obj.ModificationResponse, error) {
	return nil, errors.New("this is a mock error")
}

func mockMerchantDeleteMerchantSucc(ctx context.Context, params *merchant_obj.DeleteDocumentParams) ([]*merchant_obj.Document, error) {
	name := "foobar"
	location := [2]float64{123.45, 451.23}
	rating := float64(5)

	var ret []*merchant_obj.Document
	if len(params.Filters) == 1 {
		ret = []*merchant_obj.Document{
			&merchant_obj.Document{
				Key:      "1234",
				Name:     &name,
				Location: &location,
				Rating:   &rating,
			},
		}
	} else if len(params.Filters) == 2 {
		ret = []*merchant_obj.Document{
			&merchant_obj.Document{
				Key:      "1234",
				Name:     &name,
				Location: &location,
				Rating:   &rating,
			},
			&merchant_obj.Document{
				Key:      "2345",
				Name:     &name,
				Location: &location,
				Rating:   &rating,
			},
		}
	} else {
		ret = []*merchant_obj.Document{}
	}

	return ret, nil
}

func mockMerchantDeleteMerchantFail(ctx context.Context, params *merchant_obj.DeleteDocumentParams) ([]*merchant_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

type (
	ProductCreateFib struct {
		PatchIoutilReadall      func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal      func(data []byte, v interface{}) error
		PatchValidateNewProduct func(product *product_obj.Document) error
		PatchCreateProduct      func(ctx context.Context, params *product_obj.CreateDocumentParams) ([]*product_obj.Document, error)
		PatchJSONMarshal        func(v interface{}) ([]byte, error)
		ReqURI                  string
		ReqBody                 string
		ResCode                 int
		ResBody                 string
	}

	ProductReadFib struct {
		PatchRequestParseForm func(r *http.Request) error
		PatchGetProduct       func(ctx context.Context, params *product_obj.GetDocumentParams) ([]*product_obj.Document, error)
		PatchJSONMarshal      func(v interface{}) ([]byte, error)
		ReqURI                string
		ResCode               int
		ResBody               string
	}

	ProductUpdateFib struct {
		PatchIoutilReadall        func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal        func(data []byte, v interface{}) error
		PatchValidateModifyFilter func(filters []*product_obj.Document) error
		PatchValidateUpdateData   func(product *product_obj.Document) error
		PatchUpdateProduct        func(ctx context.Context, params *product_obj.UpdateDocumentsParams) ([]*product_obj.ModificationResponse, error)
		PatchJSONMarshal          func(v interface{}) ([]byte, error)
		ReqURI                    string
		ReqBody                   string
		ResCode                   int
		ResBody                   string
	}

	ProductDeleteFib struct {
		PatchIoutilReadall        func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal        func(data []byte, v interface{}) error
		PatchValidateModifyFilter func(filters []*product_obj.Document) error
		PatchDeleteProduct        func(ctx context.Context, params *product_obj.DeleteDocumentParams) ([]*product_obj.Document, error)
		PatchJSONMarshal          func(v interface{}) ([]byte, error)
		ReqURI                    string
		ReqBody                   string
		ResCode                   int
		ResBody                   string
	}

	MerchantCreateFib struct {
		PatchIoutilReadall       func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal       func(data []byte, v interface{}) error
		PatchValidateNewMerchant func(merchant *merchant_obj.Document) error
		PatchCreateMerchant      func(ctx context.Context, params *merchant_obj.CreateDocumentParams) ([]*merchant_obj.Document, error)
		PatchJSONMarshal         func(v interface{}) ([]byte, error)
		ReqURI                   string
		ReqBody                  string
		ResCode                  int
		ResBody                  string
	}

	MerchantReadFib struct {
		PatchRequestParseForm func(r *http.Request) error
		PatchGetMerchant      func(ctx context.Context, params *merchant_obj.GetDocumentParams) ([]*merchant_obj.Document, error)
		PatchJSONMarshal      func(v interface{}) ([]byte, error)
		ReqURI                string
		ResCode               int
		ResBody               string
	}

	MerchantUpdateFib struct {
		PatchIoutilReadall        func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal        func(data []byte, v interface{}) error
		PatchValidateModifyFilter func(filters []*merchant_obj.Document) error
		PatchValidateUpdateData   func(merchant *merchant_obj.Document) error
		PatchUpdateMerchant       func(ctx context.Context, params *merchant_obj.UpdateDocumentsParams) ([]*merchant_obj.ModificationResponse, error)
		PatchJSONMarshal          func(v interface{}) ([]byte, error)
		ReqURI                    string
		ReqBody                   string
		ResCode                   int
		ResBody                   string
	}

	MerchantDeleteFib struct {
		PatchIoutilReadall        func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal        func(data []byte, v interface{}) error
		PatchValidateModifyFilter func(filters []*merchant_obj.Document) error
		PatchDeleteMerchant       func(ctx context.Context, params *merchant_obj.DeleteDocumentParams) ([]*merchant_obj.Document, error)
		PatchJSONMarshal          func(v interface{}) ([]byte, error)
		ReqURI                    string
		ReqBody                   string
		ResCode                   int
		ResBody                   string
	}
)

func TestHandleProductCreate(t *testing.T) {
	testFibs := map[string]ProductCreateFib{
		"Success": ProductCreateFib{
			ReqURI:  "/product/crud",
			ReqBody: `[{"name":"sample_name","is_available":true,"category":"sample_r18","rating":0.0,"other":"asd"},{"name":"sample_namae","is_available":true,"category":"sample_r18","rating":10.0,"other":"asd"},{"name":"sample_nama","is_available":true,"category":"sample_tupee","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"category":"sample_fuck","rating":0.0,"other":"asd"}]`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":4},"data":[{"name":"sample_name","is_available":true,"price":0,"category":"sample_r18","description":"No Description.","rating":0,"other":"asd"},{"name":"sample_namae","is_available":true,"price":0,"category":"sample_r18","description":"No Description.","rating":10,"other":"asd"},{"name":"sample_nama","is_available":true,"price":0,"category":"sample_tupee","description":"No Description.","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"price":0,"category":"sample_fuck","description":"No Description.","rating":0,"other":"asd"}]}`,
		},
		"Bad ioutil.Readall": ProductCreateFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/product/crud",
			ReqBody:            `[{"name":"sample_name","is_available":true,"category":"sample_r18","rating":0.0,"other":"asd"},{"name":"sample_namae","is_available":true,"category":"sample_r18","rating":10.0,"other":"asd"},{"name":"sample_nama","is_available":true,"category":"sample_tupee","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"category":"sample_fuck","rating":0.0,"other":"asd"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": ProductCreateFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/product/crud",
			ReqBody:            `[{"name":"sample_name","is_available":true,"category":"sample_r18","rating":0.0,"other":"asd"},{"name":"sample_namae","is_available":true,"category":"sample_r18","rating":10.0,"other":"asd"},{"name":"sample_nama","is_available":true,"category":"sample_tupee","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"category":"sample_fuck","rating":0.0,"other":"asd"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.ValidateNewProduct": ProductCreateFib{
			PatchValidateNewProduct: mockProductValidateNewProductFail,
			ReqURI:                  "/product/crud",
			ReqBody:                 `[{"name":"sample_name","is_available":true,"category":"sample_r18","rating":0.0,"other":"asd"},{"name":"sample_namae","is_available":true,"category":"sample_r18","rating":10.0,"other":"asd"},{"name":"sample_nama","is_available":true,"category":"sample_tupee","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"category":"sample_fuck","rating":0.0,"other":"asd"}]`,
			ResCode:                 http.StatusBadRequest,
			ResBody:                 `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.CreateProduct": ProductCreateFib{
			PatchCreateProduct: mockProductCreateProductFail,
			ReqURI:             "/product/crud",
			ReqBody:            `[{"name":"sample_name","is_available":true,"category":"sample_r18","rating":0.0,"other":"asd"},{"name":"sample_namae","is_available":true,"category":"sample_r18","rating":10.0,"other":"asd"},{"name":"sample_nama","is_available":true,"category":"sample_tupee","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"category":"sample_fuck","rating":0.0,"other":"asd"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":3,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": ProductCreateFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/product/crud",
			ReqBody:          `[{"name":"sample_name","is_available":true,"category":"sample_r18","rating":0.0,"other":"asd"},{"name":"sample_namae","is_available":true,"category":"sample_r18","rating":10.0,"other":"asd"},{"name":"sample_nama","is_available":true,"category":"sample_tupee","rating":0.1,"other":"asd"},{"name":"sample_adolf","is_available":false,"category":"sample_fuck","rating":0.0,"other":"asd"}]`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":4,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleProductCreateTestHelp(t, tc, fib)
	}
}

func HandleProductCreateTestHelp(t *testing.T, tc string, fib ProductCreateFib) {
	// monkeypatch
	monkey.Patch(product_obj.CreateProduct, mockProductCreateProductSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchValidateNewProduct != nil {
		monkey.Patch(product_obj.ValidateNewProduct, fib.PatchValidateNewProduct)
	}

	if fib.PatchCreateProduct != nil {
		monkey.Patch(product_obj.CreateProduct, fib.PatchCreateProduct)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.POST(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleProductRead(t *testing.T) {
	testFibs := map[string]ProductReadFib{
		"Success1": ProductReadFib{
			ReqURI:  "/product/crud?_key=1234&sort=rating,name&is_available=true&page=1&qty=5",
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":0},"data":[]}`,
		},
		"Success2": ProductReadFib{
			ReqURI:  "/product/crud?cat=test",
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5}]}`,
		},
		"Bad http.Request.ParseForm": ProductReadFib{
			PatchRequestParseForm: mockGinRequestParseFormFail,
			ReqURI:                "/product/crud?key=1234",
			ResCode:               http.StatusBadRequest,
			ResBody:               `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.GetProduct": ProductReadFib{
			PatchGetProduct: mockProductGetProductFail,
			ReqURI:          "/product/crud?key=1234",
			ResCode:         http.StatusBadRequest,
			ResBody:         `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": ProductReadFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/product/crud?key=1234",
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleProductReadTestHelp(t, tc, fib)
	}
}

func HandleProductReadTestHelp(t *testing.T, tc string, fib ProductReadFib) {
	// monkeypatch
	monkey.Patch(product_obj.GetProduct, mockProductGetProductSucc)

	if fib.PatchRequestParseForm != nil {
		var req *http.Request
		monkey.PatchInstanceMethod(reflect.TypeOf(req), "ParseForm", fib.PatchRequestParseForm)
	}

	if fib.PatchGetProduct != nil {
		monkey.Patch(product_obj.GetProduct, fib.PatchGetProduct)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.GET(fib.ReqURI).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleProductUpdate(t *testing.T) {
	testFibs := map[string]ProductUpdateFib{
		"Success1": ProductUpdateFib{
			ReqURI:  "/product/crud",
			ReqBody: `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"old":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5},"new":{"_key":"1234","name":"foobar","is_available":true,"price":15000,"category":"test","description":"lorem ipsum","rating":5}}]}`,
		},
		"Success2": ProductUpdateFib{
			ReqURI:  "/product/crud",
			ReqBody: `{"filter":[{"_key":"1234"},{"_key":"2345"}],"new_data":{"price":50000}}`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":2},"data":[{"old":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5},"new":{"_key":"1234","name":"foobar","is_available":true,"price":50000,"category":"test","description":"lorem ipsum","rating":5}},{"old":{"_key":"2345","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5},"new":{"_key":"2345","name":"foobar","is_available":true,"price":50000,"category":"test","description":"lorem ipsum","rating":5}}]}`,
		},
		"No Filters": ProductUpdateFib{
			ReqURI:  "/product/crud",
			ReqBody: `{"filter":[],"new_data":{}}`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"Invalid modify filter, there must be at least 1 filter"},"status_code":400}`,
		},
		"Bad ioutil.Readall": ProductUpdateFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/product/crud",
			ReqBody:            `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": ProductUpdateFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/product/crud",
			ReqBody:            `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.ValidateModifyFilter": ProductUpdateFib{
			PatchValidateModifyFilter: mockProductValidateModifyFilterFail,
			ReqURI:  "/product/crud",
			ReqBody: `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.ValidateUpdateData": ProductUpdateFib{
			PatchValidateUpdateData: mockProductValidateUpdateDataFail,
			ReqURI:                  "/product/crud",
			ReqBody:                 `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode:                 http.StatusBadRequest,
			ResBody:                 `{"meta":{"err_code":3,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.UpdateProduct": ProductUpdateFib{
			PatchUpdateProduct: mockProductUpdateProductFail,
			ReqURI:             "/product/crud",
			ReqBody:            `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":4,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": ProductUpdateFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/product/crud",
			ReqBody:          `{"filter":[{"_key":"1234"}],"new_data":{"price":15000}}`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":5,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleProductUpdateTestHelp(t, tc, fib)
	}
}

func HandleProductUpdateTestHelp(t *testing.T, tc string, fib ProductUpdateFib) {
	// monkeypatch
	monkey.Patch(product_obj.UpdateProduct, mockProductUpdateProductSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchValidateModifyFilter != nil {
		monkey.Patch(product_obj.ValidateModifyFilter, fib.PatchValidateModifyFilter)
	}

	if fib.PatchValidateUpdateData != nil {
		monkey.Patch(product_obj.ValidateUpdateData, fib.PatchValidateUpdateData)
	}

	if fib.PatchUpdateProduct != nil {
		monkey.Patch(product_obj.UpdateProduct, fib.PatchUpdateProduct)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.PUT(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleProductDelete(t *testing.T) {
	testFibs := map[string]ProductDeleteFib{
		"Success1": ProductDeleteFib{
			ReqURI:  "/product/crud",
			ReqBody: `[{"_key":"1234"}]`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5}]}`,
		},
		"Success2": ProductDeleteFib{
			ReqURI:  "/product/crud",
			ReqBody: `[{"_key":"1234"},{"_key":"2345"}]`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":2},"data":[{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5},{"_key":"2345","name":"foobar","is_available":true,"price":10000,"category":"test","description":"lorem ipsum","rating":5}]}`,
		},
		"No Filters": ProductDeleteFib{
			ReqURI:  "/product/crud",
			ReqBody: `[]`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"Invalid modify filter, there must be at least 1 filter"},"status_code":400}`,
		},
		"Bad ioutil.Readall": ProductDeleteFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/product/crud",
			ReqBody:            `[{"_key":"1234"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": ProductDeleteFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/product/crud",
			ReqBody:            `[{"_key":"1234"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.ValidateModifyFilter": ProductDeleteFib{
			PatchValidateModifyFilter: mockProductValidateModifyFilterFail,
			ReqURI:  "/product/crud",
			ReqBody: `[{"_key":"1234"}]`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.DeleteProduct": ProductDeleteFib{
			PatchDeleteProduct: mockProductDeleteProductFail,
			ReqURI:             "/product/crud",
			ReqBody:            `[{"_key":"1234"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":3,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": ProductDeleteFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/product/crud",
			ReqBody:          `[{"_key":"1234"}]`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":4,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleProductDeleteTestHelp(t, tc, fib)
	}
}

func HandleProductDeleteTestHelp(t *testing.T, tc string, fib ProductDeleteFib) {
	// monkeypatch
	monkey.Patch(product_obj.DeleteProduct, mockProductDeleteProductSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchValidateModifyFilter != nil {
		monkey.Patch(product_obj.ValidateModifyFilter, fib.PatchValidateModifyFilter)
	}

	if fib.PatchDeleteProduct != nil {
		monkey.Patch(product_obj.DeleteProduct, fib.PatchDeleteProduct)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.DELETE(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleMerchantCreate(t *testing.T) {
	testFibs := map[string]MerchantCreateFib{
		"Success": MerchantCreateFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `[{"name":"sample_name","location":[123.45,543.21],"rating":5.0,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"other":"asd"}]`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":2},"data":[{"name":"sample_name","location":[123.45,543.21],"rating":5,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"rating":0,"other":"asd"}]}`,
		},
		"Bad ioutil.Readall": MerchantCreateFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/merchant/crud",
			ReqBody:            `[{"name":"sample_name","location":[123.45,543.21],"rating":5.0,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"other":"asd"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": MerchantCreateFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/merchant/crud",
			ReqBody:            `[{"name":"sample_name","location":[123.45,543.21],"rating":5.0,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"other":"asd"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.ValidateNewMerchant": MerchantCreateFib{
			PatchValidateNewMerchant: mockMerchantValidateNewMerchantFail,
			ReqURI:  "/merchant/crud",
			ReqBody: `[{"name":"sample_name","location":[123.45,543.21],"rating":5.0,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"other":"asd"}]`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.CreateMerchant": MerchantCreateFib{
			PatchCreateMerchant: mockMerchantCreateMerchantFail,
			ReqURI:              "/merchant/crud",
			ReqBody:             `[{"name":"sample_name","location":[123.45,543.21],"rating":5.0,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"other":"asd"}]`,
			ResCode:             http.StatusBadRequest,
			ResBody:             `{"meta":{"err_code":3,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": MerchantCreateFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/merchant/crud",
			ReqBody:          `[{"name":"sample_name","location":[123.45,543.21],"rating":5.0,"other":"asd"},{"name":"sample_namae","location":[123.452,543.212],"other":"asd"}]`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":4,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleMerchantCreateTestHelp(t, tc, fib)
	}
}

func HandleMerchantCreateTestHelp(t *testing.T, tc string, fib MerchantCreateFib) {
	// monkeypatch
	monkey.Patch(merchant_obj.CreateMerchant, mockMerchantCreateMerchantSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchValidateNewMerchant != nil {
		monkey.Patch(merchant_obj.ValidateNewMerchant, fib.PatchValidateNewMerchant)
	}

	if fib.PatchCreateMerchant != nil {
		monkey.Patch(merchant_obj.CreateMerchant, fib.PatchCreateMerchant)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.POST(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleMerchantRead(t *testing.T) {
	testFibs := map[string]MerchantReadFib{
		"Success": MerchantReadFib{
			ReqURI:  "/merchant/crud?_key=1234&sort=rating,name&page=1&qty=5",
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"_key":"1234","name":"foobar","location":[123.45,451.23],"rating":5}]}`,
		},
		"Bad http.Request.ParseForm": MerchantReadFib{
			PatchRequestParseForm: mockGinRequestParseFormFail,
			ReqURI:                "/merchant/crud?key=1234",
			ResCode:               http.StatusBadRequest,
			ResBody:               `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.GetMerchant": MerchantReadFib{
			PatchGetMerchant: mockMerchantGetMerchantFail,
			ReqURI:           "/merchant/crud?key=1234",
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": MerchantReadFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/merchant/crud?key=1234",
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleMerchantReadTestHelp(t, tc, fib)
	}
}

func HandleMerchantReadTestHelp(t *testing.T, tc string, fib MerchantReadFib) {
	// monkeypatch
	monkey.Patch(merchant_obj.GetMerchant, mockMerchantGetMerchantSucc)

	if fib.PatchRequestParseForm != nil {
		var req *http.Request
		monkey.PatchInstanceMethod(reflect.TypeOf(req), "ParseForm", fib.PatchRequestParseForm)
	}

	if fib.PatchGetMerchant != nil {
		monkey.Patch(merchant_obj.GetMerchant, fib.PatchGetMerchant)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.GET(fib.ReqURI).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleMerchantUpdate(t *testing.T) {
	testFibs := map[string]MerchantUpdateFib{
		"Success1": MerchantUpdateFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45,543.21]}}`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"old":{"_key":"1234","name":"foobar","location":[123.45,451.23],"rating":5},"new":{"_key":"1234","name":"foobar","location":[123.45,543.21],"rating":5}}]}`,
		},
		"Success2": MerchantUpdateFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `{"filter":[{"_key":"1234"},{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":2},"data":[{"old":{"_key":"1234","name":"foobar","location":[123.45,451.23],"rating":5},"new":{"_key":"1234","name":"foobar","location":[123.45,543.21],"rating":5}},{"old":{"_key":"2345","name":"foobar","location":[123.45,451.23],"rating":5},"new":{"_key":"2345","name":"foobar","location":[123.45,543.21],"rating":5}}]}`,
		},
		"No Filters": MerchantUpdateFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `{"filter":[],"new_data":{}}`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"Invalid modify filter, there must be at least 1 filter"},"status_code":400}`,
		},
		"Bad ioutil.Readall": MerchantUpdateFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/merchant/crud",
			ReqBody:            `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": MerchantUpdateFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/merchant/crud",
			ReqBody:            `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.ValidateModifyFilter": MerchantUpdateFib{
			PatchValidateModifyFilter: mockMerchantValidateModifyFilterFail,
			ReqURI:  "/merchant/crud",
			ReqBody: `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.ValidateUpdateData": MerchantUpdateFib{
			PatchValidateUpdateData: mockMerchantValidateUpdateDataFail,
			ReqURI:                  "/merchant/crud",
			ReqBody:                 `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode:                 http.StatusBadRequest,
			ResBody:                 `{"meta":{"err_code":3,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.UpdateMerchant": MerchantUpdateFib{
			PatchUpdateMerchant: mockMerchantUpdateMerchantFail,
			ReqURI:              "/merchant/crud",
			ReqBody:             `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode:             http.StatusBadRequest,
			ResBody:             `{"meta":{"err_code":4,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": MerchantUpdateFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/merchant/crud",
			ReqBody:          `{"filter":[{"_key":"1234"}],"new_data":{"location":[123.45, 543.21]}}`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":5,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleMerchantUpdateTestHelp(t, tc, fib)
	}
}

func HandleMerchantUpdateTestHelp(t *testing.T, tc string, fib MerchantUpdateFib) {
	// monkeypatch
	monkey.Patch(merchant_obj.UpdateMerchant, mockMerchantUpdateMerchantSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchValidateModifyFilter != nil {
		monkey.Patch(merchant_obj.ValidateModifyFilter, fib.PatchValidateModifyFilter)
	}

	if fib.PatchValidateUpdateData != nil {
		monkey.Patch(merchant_obj.ValidateUpdateData, fib.PatchValidateUpdateData)
	}

	if fib.PatchUpdateMerchant != nil {
		monkey.Patch(merchant_obj.UpdateMerchant, fib.PatchUpdateMerchant)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.PUT(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleMerchantDelete(t *testing.T) {
	testFibs := map[string]MerchantDeleteFib{
		"Success1": MerchantDeleteFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `[{"_key":"1234"}]`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"_key":"1234","name":"foobar","location":[123.45,451.23],"rating":5}]}`,
		},
		"Success2": MerchantDeleteFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `[{"_key":"1234"},{"_key":"2345"}]`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":2},"data":[{"_key":"1234","name":"foobar","location":[123.45,451.23],"rating":5},{"_key":"2345","name":"foobar","location":[123.45,451.23],"rating":5}]}`,
		},
		"No Filters": MerchantDeleteFib{
			ReqURI:  "/merchant/crud",
			ReqBody: `[]`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"Invalid modify filter, there must be at least 1 filter"},"status_code":400}`,
		},
		"Bad ioutil.Readall": MerchantDeleteFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/merchant/crud",
			ReqBody:            `[{"_key":"1234"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": MerchantDeleteFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/merchant/crud",
			ReqBody:            `[{"_key":"1234"}]`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.ValidateModifyFilter": MerchantDeleteFib{
			PatchValidateModifyFilter: mockMerchantValidateModifyFilterFail,
			ReqURI:  "/merchant/crud",
			ReqBody: `[{"_key":"1234"}]`,
			ResCode: http.StatusBadRequest,
			ResBody: `{"meta":{"err_code":2,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad merchant.DeleteMerchant": MerchantDeleteFib{
			PatchDeleteMerchant: mockMerchantDeleteMerchantFail,
			ReqURI:              "/merchant/crud",
			ReqBody:             `[{"_key":"1234"}]`,
			ResCode:             http.StatusBadRequest,
			ResBody:             `{"meta":{"err_code":3,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": MerchantDeleteFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/merchant/crud",
			ReqBody:          `[{"_key":"1234"}]`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":4,"err_msg":"this is a mock error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleMerchantDeleteTestHelp(t, tc, fib)
	}
}

func HandleMerchantDeleteTestHelp(t *testing.T, tc string, fib MerchantDeleteFib) {
	// monkeypatch
	monkey.Patch(merchant_obj.DeleteMerchant, mockMerchantDeleteMerchantSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchValidateModifyFilter != nil {
		monkey.Patch(merchant_obj.ValidateModifyFilter, fib.PatchValidateModifyFilter)
	}

	if fib.PatchDeleteMerchant != nil {
		monkey.Patch(merchant_obj.DeleteMerchant, fib.PatchDeleteMerchant)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.DELETE(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}
