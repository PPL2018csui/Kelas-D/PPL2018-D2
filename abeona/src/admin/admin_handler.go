package admin

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/render"
	merchant_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/merchant"
	product_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/product"
)

type (
	// ProductUpdateRequest contains filters and new product document
	ProductUpdateRequest struct {
		Filters []*product_obj.Document `json:"filter"`
		NewData *product_obj.Document   `json:"new_data"`
	}

	// AdminMeta contains error code, error and count
	AdminMeta struct {
		ErrCode int     `json:"err_code"`
		Error   *string `json:"err_msg,omitempty"`
		Count   *int    `json:"count,omitempty"`
	}

	AdminHandlerProductResponse struct {
		StatusCode int                     `json:"status_code"`
		Meta       AdminMeta               `json:"meta"`
		Data       []*product_obj.Document `json:"data"`
	}

	AdminHandlerProductModifyResponse struct {
		StatusCode int                                 `json:"status_code"`
		Meta       AdminMeta                           `json:"meta"`
		Data       []*product_obj.ModificationResponse `json:"data"`
	}

	MerchantUpdateRequest struct {
		Filters []*merchant_obj.Document `json:"filter"`
		NewData *merchant_obj.Document   `json:"new_data"`
	}

	AdminHandlerMerchantResponse struct {
		StatusCode int                      `json:"status_code"`
		Meta       AdminMeta                `json:"meta"`
		Data       []*merchant_obj.Document `json:"data"`
	}

	AdminHandlerMerchantModifyResponse struct {
		StatusCode int                                  `json:"status_code"`
		Meta       AdminMeta                            `json:"meta"`
		Data       []*merchant_obj.ModificationResponse `json:"data"`
	}
)

// HandleProductCreate handles product POST requets
// creates new documents in arangodb, returns list of created documents
func HandleProductCreate(c *gin.Context) {

	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Product Document
	products := make([]*product_obj.Document, 0)
	err = json.Unmarshal(body, &products)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate data
	for _, prod := range products {
		// if any of the  data is not sufficient, don't forward data
		if err = product_obj.ValidateNewProduct(prod); err != nil {
			log.Println("Invalid Body:", err)
			c.JSON(http.StatusBadRequest, gin.H{
				"status_code": http.StatusBadRequest,
				"meta": gin.H{
					"err_code": 2,
					"err_msg":  err.Error(),
				},
			})
			return
		}
	}

	// Add documents to arangodb
	newProducts, err := product_obj.CreateProduct(nil, &product_obj.CreateDocumentParams{
		Documents: products,
	})
	if err != nil {
		log.Println("Create Products:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	productCount := len(newProducts)

	// Create response structure
	response := AdminHandlerProductResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &productCount,
		},
		Data: newProducts,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 4,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleProductRead handles product GET request
// returns list of products that matches filter
func HandleProductRead(c *gin.Context) {

	// Parse form to get values
	err := c.Request.ParseForm()
	if err != nil {
		log.Println("Parse Form:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Get values for filter
	sorting := c.Request.FormValue("sort")
	var sortFields []string
	if sorting != "" {
		sortFields = strings.Split(sorting, ",")
	}

	filter := &product_obj.Document{}

	key := c.Request.FormValue("_key")
	if key != "" {
		filter.Key = key
	}

	isAvailableStr := c.Request.FormValue("is_available")
	if isAvailableStr != "" {
		isAvailable, err := strconv.ParseBool(isAvailableStr)
		if err == nil {
			filter.IsAvailable = &isAvailable
		}
	}

	category := c.Request.FormValue("cat")
	if category != "" {
		filter.Category = &category
	}

	pageStr := c.Request.FormValue("page")
	var page int
	if pageStr != "" {
		// ignore unparseable, use default value
		page, _ = strconv.Atoi(pageStr)
	}

	quantityStr := c.Request.FormValue("qty")
	var quantity int
	if quantityStr != "" {
		// ignore unparseable, use default value
		quantity, _ = strconv.Atoi(quantityStr)
	}

	// Get documents from arangodb
	products, err := product_obj.GetProduct(nil, &product_obj.GetDocumentParams{
		Filter:   filter,
		Sorting:  sortFields,
		Page:     page,
		Quantity: quantity,
	})
	if err != nil {
		log.Println("Get Product:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	productCount := len(products)

	// Create response structure
	response := AdminHandlerProductResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &productCount,
		},
		Data: products,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleProductRead handles product GET request
// returns list of products that matches filter
func HandleProductDetail(c *gin.Context) {
	key := c.Param("_key")

	// TODO get data from DB

	product := product_obj.Document{
		Key:   key,
		Other: "foobar",
	}

	resp, err := json.Marshal(product)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"err": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, string(resp))
}

// HandleProductUpdate handles product PUT request
// updates documents in arangodb, returns old and new data comparison
func HandleProductUpdate(c *gin.Context) {

	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Product Document
	updateRequest := ProductUpdateRequest{}
	err = json.Unmarshal(body, &updateRequest)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate filters for update
	err = product_obj.ValidateModifyFilter(updateRequest.Filters)
	if err != nil {
		log.Println("Validate Filters:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate update data
	err = product_obj.ValidateUpdateData(updateRequest.NewData)
	if err != nil {
		log.Println("Validate New Data:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Update documents in arangodb
	updateResponse, err := product_obj.UpdateProduct(nil, &product_obj.UpdateDocumentsParams{
		Filters: updateRequest.Filters,
		NewData: updateRequest.NewData,
	})
	if err != nil {
		log.Println("Update Products:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 4,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	responseCount := len(updateResponse)

	// Create response structure
	response := AdminHandlerProductModifyResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &responseCount,
		},
		Data: updateResponse,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 5,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleProductDelete handles product DELETE requests
// deletes documents in arangodb, returns the deleted data
func HandleProductDelete(c *gin.Context) {

	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Product Document
	filters := make([]*product_obj.Document, 0)
	err = json.Unmarshal(body, &filters)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate filters for deletion
	err = product_obj.ValidateModifyFilter(filters)
	if err != nil {
		log.Println("Validate Filters:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Delete products from arangodb
	oldProducts, err := product_obj.DeleteProduct(nil, &product_obj.DeleteDocumentParams{
		Filters: filters,
	})
	if err != nil {
		log.Println("Delete Products:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	productCount := len(oldProducts)

	// Create response structure
	response := AdminHandlerProductResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &productCount,
		},
		Data: oldProducts,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 4,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleMerchantCreate handles merchant POST requets
// creates new documents in arangodb, returns list of created documents
func HandleMerchantCreate(c *gin.Context) {
	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Merchant Document
	merchants := make([]*merchant_obj.Document, 0)
	err = json.Unmarshal(body, &merchants)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate data
	for _, merc := range merchants {
		// if any of the  data is not sufficient, don't forward data
		if err = merchant_obj.ValidateNewMerchant(merc); err != nil {
			log.Println("Invalid Body:", err)
			c.JSON(http.StatusBadRequest, gin.H{
				"status_code": http.StatusBadRequest,
				"meta": gin.H{
					"err_code": 2,
					"err_msg":  err.Error(),
				},
			})
			return
		}
	}

	// Add documents to arangodb
	newMerchants, err := merchant_obj.CreateMerchant(nil, &merchant_obj.CreateDocumentParams{
		Documents: merchants,
	})
	if err != nil {
		log.Println("Create Merchants:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	merchantCount := len(newMerchants)

	// Create response structure
	response := AdminHandlerMerchantResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &merchantCount,
		},
		Data: newMerchants,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 4,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleMerchantRead handles merchant GET request
// returns list of products that matches filter
func HandleMerchantRead(c *gin.Context) {
	// Parse form to get values
	err := c.Request.ParseForm()
	if err != nil {
		log.Println("Parse Form:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Get values for filter
	sorting := c.Request.FormValue("sort")
	var sortFields []string
	if sorting != "" {
		sortFields = strings.Split(sorting, ",")
	}

	filter := &merchant_obj.Document{}

	key := c.Request.FormValue("_key")
	if key != "" {
		filter.Key = key
	}

	pageStr := c.Request.FormValue("page")
	var page int
	if pageStr != "" {
		// ignore unparseable, use default value
		page, _ = strconv.Atoi(pageStr)
	}

	quantityStr := c.Request.FormValue("qty")
	var quantity int
	if quantityStr != "" {
		// ignore unparseable, use default value
		quantity, _ = strconv.Atoi(quantityStr)
	}

	// Get documents from arangodb
	merchants, err := merchant_obj.GetMerchant(nil, &merchant_obj.GetDocumentParams{
		Filter:   filter,
		Sorting:  sortFields,
		Page:     page,
		Quantity: quantity,
	})
	if err != nil {
		log.Println("Get Product:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	merchantCount := len(merchants)

	// Create response structure
	response := AdminHandlerMerchantResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &merchantCount,
		},
		Data: merchants,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleMerchantUpdate handles merchant PUT request
// updates documents in arangodb, returns old and new data comparison
func HandleMerchantUpdate(c *gin.Context) {
	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Product Document
	updateRequest := MerchantUpdateRequest{}
	err = json.Unmarshal(body, &updateRequest)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate filters for update
	err = merchant_obj.ValidateModifyFilter(updateRequest.Filters)
	if err != nil {
		log.Println("Validate Filters:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate update data
	err = merchant_obj.ValidateUpdateData(updateRequest.NewData)
	if err != nil {
		log.Println("Validate New Data:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Update documents in arangodb
	updateResponse, err := merchant_obj.UpdateMerchant(nil, &merchant_obj.UpdateDocumentsParams{
		Filters: updateRequest.Filters,
		NewData: updateRequest.NewData,
	})
	if err != nil {
		log.Println("Update Products:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 4,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	responseCount := len(updateResponse)

	// Create response structure
	response := AdminHandlerMerchantModifyResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &responseCount,
		},
		Data: updateResponse,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 5,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleMerchantDelete handles merchant DELETE requests
// deletes documents in arangodb, returns the deleted data
func HandleMerchantDelete(c *gin.Context) {
	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Product Document
	filters := make([]*merchant_obj.Document, 0)
	err = json.Unmarshal(body, &filters)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Validate filters for deletion
	err = merchant_obj.ValidateModifyFilter(filters)
	if err != nil {
		log.Println("Validate Filters:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Delete merchants from arangodb
	oldMerchants, err := merchant_obj.DeleteMerchant(nil, &merchant_obj.DeleteDocumentParams{
		Filters: filters,
	})
	if err != nil {
		log.Println("Delete Merchants:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	merchantCount := len(oldMerchants)

	// Create response structure
	response := AdminHandlerMerchantResponse{
		StatusCode: http.StatusOK,
		Meta: AdminMeta{
			Count: &merchantCount,
		},
		Data: oldMerchants,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 4,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}
