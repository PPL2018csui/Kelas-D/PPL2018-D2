package abeona

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/appleboy/gofight"
	"github.com/bouk/monkey"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	product_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/product"
	tag_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/tag"
	user_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/user"
)

func mockTagAllDocumentsSucc(ctx context.Context, params *tag_obj.AllDocumentsParams) ([]*tag_obj.Document, error) {
	ret := []*tag_obj.Document{
		&tag_obj.Document{
			Name: "foobar",
		},
	}

	return ret, nil
}

func mockTagAllDocumentsFail(ctx context.Context, params *tag_obj.AllDocumentsParams) ([]*tag_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockCreateUserSucc(ctx context.Context, params *user_obj.OnboardParams) (*user_obj.Document, error) {
	fbid := "spectrum"
	dname := "disorder"

	ret := &user_obj.Document{
		Key:         "autism",
		FirebaseID:  &fbid,
		DisplayName: &dname,
		PriceRange: &user_obj.Float64Range{
			Min: 0.0,
			Max: 0.0,
		},
	}

	return ret, nil
}

func mockProductsByTagSucc(ctx context.Context, params *product_obj.GetDocumentParams) ([]*product_obj.Document, error) {
	name := "foobar"
	isAvailable := true
	tags := &[]string{"foobar"}
	price := float64(10000)
	category := "test"
	description := "lorem ipsum"
	rating := float64(5)

	ret := []*product_obj.Document{
		&product_obj.Document{
			Key:         "1234",
			Name:        &name,
			IsAvailable: &isAvailable,
			Tags:        tags,
			Price:       &price,
			Category:    &category,
			Description: &description,
			Rating:      &rating,
		}}

	return ret, nil
}

func mockCreateUserFail(ctx context.Context, params *user_obj.OnboardParams) ([]*user_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockProductsByTagFail(ctx context.Context, params *product_obj.GetDocumentParams) ([]*product_obj.Document, error) {
	return nil, errors.New("this is a mock error")
}

func mockIoutilReadallFail(r io.Reader) ([]byte, error) {
	return nil, errors.New("this is a mock error")
}
func mockJSONUnmarshalFail(data []byte, v interface{}) error {
	return errors.New("this is a mock error")
}

type (
	HandleOnboardingTagFib struct {
		PatchAllDocuments func(ctx context.Context, params *tag_obj.AllDocumentsParams) ([]*tag_obj.Document, error)
		PatchJSONMarshal  func(v interface{}) ([]byte, error)
		ReqURI            string
		ResCode           int
		ResBody           string
	}

	HandleOnboardingCreateUserFib struct {
		PatchIoutilReadall func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal func(data []byte, v interface{}) error
		//PatchCreateUser    func(ctx context.Context, params *user_obj.GetDocumentParams) ([]*user_obj.Document, error)
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		ReqURI           string
		ReqBody          string
		ResCode          int
		ResBody          string
	}

	HandleOnboardingProductFib struct {
		PatchIoutilReadall func(r io.Reader) ([]byte, error)
		PatchJSONUnmarshal func(data []byte, v interface{}) error
		PatchProductsByTag func(ctx context.Context, params *product_obj.GetDocumentParams) ([]*product_obj.Document, error)
		PatchJSONMarshal   func(v interface{}) ([]byte, error)
		ReqURI             string
		ReqBody            string
		ResCode            int
		ResBody            string
	}
)

func OnboardingGinEngine() *gin.Engine {
	gin.SetMode(gin.TestMode)
	r := gin.New()

	r.GET("/onboard/tag", HandleOnboardingTag)
	r.POST("/onboard/user", HandleOnboardingCreateUser)
	r.POST("/onboard/product", HandleOnboardingProduct)

	return r
}

func TestHandleOnboardingTag(t *testing.T) {
	testFibs := map[string]HandleOnboardingTagFib{
		"Success": HandleOnboardingTagFib{
			ReqURI:  "/onboard/tag",
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":1},"data":[{"key":"","name":"foobar"}]}`,
		},
		"Bad tag.AllDocuments": HandleOnboardingTagFib{
			PatchAllDocuments: mockTagAllDocumentsFail,
			ReqURI:            "/onboard/tag",
			ResCode:           http.StatusBadRequest,
			ResBody:           `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Marshal": HandleOnboardingTagFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/onboard/tag",
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":2,"err_msg":"mocker error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleOnboardingTagTestHelp(t, tc, fib)
	}
}

func HandleOnboardingTagTestHelp(t *testing.T, tc string, fib HandleOnboardingTagFib) {
	monkey.Patch(tag_obj.AllDocuments, mockTagAllDocumentsSucc)

	// monkeypatch
	if fib.PatchAllDocuments != nil {
		monkey.Patch(tag_obj.AllDocuments, fib.PatchAllDocuments)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.GET(fib.ReqURI).
		Run(OnboardingGinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleOnboardingCreateUser(t *testing.T) {
	testFibs := map[string]HandleOnboardingCreateUserFib{
		"Success": HandleOnboardingCreateUserFib{
			ReqURI:  "/onboard/user",
			ReqBody: `{"firebase_id":"","preference":[{"product":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","tags":["foobar"],"description":"lorem ipsum","rating":5},"preferred":true}]}`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0},"user":{"_key":"autism","firebase_id":"spectrum","display_name":"disorder","price_range":{"min":0,"max":0}}}`,
		},
		"Bad ioutil.Readall": HandleOnboardingCreateUserFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/onboard/user",
			ReqBody:            `{"firebase_id":"","preference":[{"product":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","tags":["foobar"],"description":"lorem ipsum","rating":5},"preferred":true}]}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": HandleOnboardingCreateUserFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/onboard/user",
			ReqBody:            `{"firebase_id":"","preference":[{"product":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","tags":["foobar"],"description":"lorem ipsum","rating":5},"preferred":true}]}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		/*	"Bad user.CreateUser": HandleOnboardingCreateUserFib{
			PatchCreateUser: mockProductsByTagFail,
			ReqURI:          "/onboard/user",
			ReqBody:         `{"firebase_id":"","preference":[{"product":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","tags":["foobar"],"description":"lorem ipsum","rating":5},"preferred":true}]}`,
			ResCode:         http.StatusOK,
			ResBody:         `{"status_code":200,"meta":{"err_code":2,"count":0},"data":[]}`,
		},*/
		"Bad json.Marshal": HandleOnboardingCreateUserFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/onboard/user",
			ReqBody:          `{"firebase_id":"","preference":[{"product":{"_key":"1234","name":"foobar","is_available":true,"price":10000,"category":"test","tags":["foobar"],"description":"lorem ipsum","rating":5},"preferred":true}]}`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":3,"err_msg":"mocker error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleOnboardingCreateUserTestHelp(t, tc, fib)
	}
}

func HandleOnboardingCreateUserTestHelp(t *testing.T, tc string, fib HandleOnboardingCreateUserFib) {
	monkey.Patch(user_obj.OnboardUser, mockCreateUserSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}
	/*
		if fib.PatchCreateUser != nil {
			monkey.Patch(user_obj.CreateUser, mockCreateUserFail)
		}
	*/
	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.POST(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(OnboardingGinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			t.Log(res.Code)
			t.Log(res.Body.String())
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}

func TestHandleOnboardingProduct(t *testing.T) {
	testFibs := map[string]HandleOnboardingProductFib{
		"Success": HandleOnboardingProductFib{
			ReqURI:  "/onboard/product",
			ReqBody: `{"data":[{"key":"","name":"foobar"}]}`,
			ResCode: http.StatusOK,
			ResBody: `{"status_code":200,"meta":{"err_code":0,"count":0},"data":[]}`,
		},
		"Bad ioutil.Readall": HandleOnboardingProductFib{
			PatchIoutilReadall: mockIoutilReadallFail,
			ReqURI:             "/onboard/product",
			ReqBody:            `{"data":[{"key":"","name":"foobar"}]}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":0,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad json.Unmarshal": HandleOnboardingProductFib{
			PatchJSONUnmarshal: mockJSONUnmarshalFail,
			ReqURI:             "/onboard/product",
			ReqBody:            `{"data":[{"key":"","name":"foobar"}]}`,
			ResCode:            http.StatusBadRequest,
			ResBody:            `{"meta":{"err_code":1,"err_msg":"this is a mock error"},"status_code":400}`,
		},
		"Bad product.ProductsByTag": HandleOnboardingProductFib{
			PatchProductsByTag: mockProductsByTagFail,
			ReqURI:             "/onboard/product",
			ReqBody:            `{"data":[{"key":"","name":"foobar"}]}`,
			ResCode:            http.StatusOK,
			ResBody:            `{"status_code":200,"meta":{"err_code":0,"count":0},"data":[]}`,
		},
		"Bad json.Marshal": HandleOnboardingProductFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/onboard/product",
			ReqBody:          `{"data":[{"key":"","name":"foobar"}]}`,
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"meta":{"err_code":2,"err_msg":"mocker error"},"status_code":400}`,
		},
	}

	for tc, fib := range testFibs {
		HandleOnboardingProductTestHelp(t, tc, fib)
	}
}

func HandleOnboardingProductTestHelp(t *testing.T, tc string, fib HandleOnboardingProductFib) {
	// monkeypatch
	monkey.Patch(product_obj.GetProduct, mockProductsByTagSucc)

	if fib.PatchIoutilReadall != nil {
		monkey.Patch(ioutil.ReadAll, fib.PatchIoutilReadall)
	}

	if fib.PatchJSONUnmarshal != nil {
		monkey.Patch(json.Unmarshal, fib.PatchJSONUnmarshal)
	}

	if fib.PatchProductsByTag != nil {
		monkey.Patch(product_obj.GetProduct, mockProductsByTagFail)
	}

	if fib.PatchJSONMarshal != nil {
		var guard *monkey.PatchGuard
		guard = monkey.Patch(json.Marshal, func(v interface{}) ([]byte, error) {
			defer guard.Unpatch()
			return fib.PatchJSONMarshal(v)
		})
	}

	// revert monkeypatch
	defer monkey.UnpatchAll()

	r := gofight.New()
	r.POST(fib.ReqURI).
		SetBody(fib.ReqBody).
		Run(OnboardingGinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, fib.ResCode, res.Code)
			assert.Equal(t, fib.ResBody, res.Body.String())
		})
}
