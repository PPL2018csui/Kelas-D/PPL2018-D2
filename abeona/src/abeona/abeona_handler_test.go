package abeona

import (
	"encoding/json"
	"errors"
	"net/http"
	"testing"

	"github.com/appleboy/gofight"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
)

type (
	ProductFib struct {
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		ReqURI           string
		ResCode          int
		ResBody          string
	}

	MerchantFib struct {
		PatchJSONMarshal func(v interface{}) ([]byte, error)
		ReqURI           string
		ResCode          int
		ResBody          string
	}
)

func mockJSONMarshalFail(v interface{}) ([]byte, error) {
	return nil, errors.New("mocker error")
}

func GinEngine() *gin.Engine {
	gin.SetMode(gin.TestMode)
	r := gin.New()

	r.GET("/", HandleIndex)
	r.GET("/product", HandleProductView)
	r.GET("/merchant", HandleMerchantView)

	return r
}

func TestHandleIndex(t *testing.T) {
	r := gofight.New()
	r.GET("/").
		Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
			assert.Equal(t, http.StatusOK, res.Code)
			assert.Equal(t, "{\"foo\":\"bar\"}", res.Body.String())
		})
}

func TestHandleProductView(t *testing.T) {
	testFibs := map[string]ProductFib{
		"Success": ProductFib{
			ReqURI:  "/product",
			ResCode: http.StatusOK,
			ResBody: `{"products":[{"_key":"1234","other":"foobar"},{"_key":"2345","other":"foobar"}]}`,
		},
		"Bad Json Marshal": ProductFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/product",
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"err":{}}`,
		},
	}

	for _, fib := range testFibs {
		// monkeypatch
		if fib.PatchJSONMarshal != nil {
			jsonMarshal = fib.PatchJSONMarshal
		}

		r := gofight.New()
		r.GET(fib.ReqURI).
			Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
				assert.Equal(t, fib.ResCode, res.Code)
				assert.Equal(t, fib.ResBody, res.Body.String())
			})

		// revert monkeypatch
		jsonMarshal = json.Marshal
	}
}

func TestHandleMerchantView(t *testing.T) {
	testFibs := map[string]MerchantFib{
		"Success": MerchantFib{
			ReqURI:  "/merchant",
			ResCode: http.StatusOK,
			ResBody: `{"merchants":[{"_key":"1234","other":"foobar"},{"_key":"2345","other":"foobar"}]}`,
		},
		"Bad Json Marshal": MerchantFib{
			PatchJSONMarshal: mockJSONMarshalFail,
			ReqURI:           "/merchant",
			ResCode:          http.StatusBadRequest,
			ResBody:          `{"err":{}}`,
		},
	}

	for _, fib := range testFibs {
		// monkeypatch
		if fib.PatchJSONMarshal != nil {
			jsonMarshal = fib.PatchJSONMarshal
		}

		r := gofight.New()
		r.GET(fib.ReqURI).
			Run(GinEngine(), func(res gofight.HTTPResponse, req gofight.HTTPRequest) {
				assert.Equal(t, fib.ResCode, res.Code)
				assert.Equal(t, fib.ResBody, res.Body.String())
			})

		// revert monkeypatch
		jsonMarshal = json.Marshal
	}
}
