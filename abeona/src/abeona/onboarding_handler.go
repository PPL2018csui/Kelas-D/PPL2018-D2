package abeona

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/render"
	product_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/product"
	tag_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/tag"
	user_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/user"
)

var (
	productAmountPerTag = 5
)

type (
	// OnboardingMeta contains error code, error message and count of data
	OnboardingMeta struct {
		ErrCode int     `json:"err_code"`
		Error   *string `json:"err_msg,omitempty"`
		Count   *int    `json:"count,omitempty"`
	}

	OnboardingMetaUser struct {
		ErrCode int     `json:"err_code"`
		Error   *string `json:"err_msg,omitempty"`
	}

	OnboardingHandlerTagResponse struct {
		StatusCode int                 `json:"status_code"`
		Meta       OnboardingMeta      `json:"meta"`
		Data       []*tag_obj.Document `json:"data"`
	}

	UserCheckData struct {
		Exists bool `json:"exists"`
	}

	OnboardingHandlerCheckUserResponse struct {
		StatusCode int                `json:"status_code"`
		Meta       OnboardingMetaUser `json:"meta"`
		Data       UserCheckData      `json:"data"`
	}

	ProductSelectionPair struct {
		Product   *product_obj.Document `json:"product"`
		Preferred bool                  `json:"preferred"`
	}

	OnboardingHandlerCreateUserRequest struct {
		FirebaseID     string                  `json:"firebase_id"`
		PreferenceList []*ProductSelectionPair `json:"preference"`
	}

	OnboardingHandlerCreateUserResponse struct {
		StatusCode int                `json:"status_code"`
		Meta       OnboardingMetaUser `json:"meta"`
		User       *user_obj.Document `json:"user"`
	}

	// OnboardingHandlerProductResponse contains statuscode, metadata and product document
	OnboardingHandlerProductRequest struct {
		Data []*tag_obj.Document `json:"data"`
	}

	// OnboardingHandlerProductResponse contains statuscode, metadata and product document
	OnboardingHandlerProductResponse struct {
		StatusCode int                     `json:"status_code"`
		Meta       OnboardingMeta          `json:"meta"`
		Data       []*product_obj.Document `json:"data"`
	}
)

func HandleOnboardingCheck(c *gin.Context) {

	// Parse form to get values
	err := c.Request.ParseForm()
	if err != nil {
		log.Println("Parse Form:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	firebaseId := c.Request.FormValue("firebase_id")

	var isCreated bool
	if firebaseId != "" {
		isCreated, err = user_obj.CheckUser(nil, &user_obj.CheckParams{FirebaseId: firebaseId})
		if err != nil {
			log.Println("Check User:", err)
			c.JSON(http.StatusBadRequest, gin.H{
				"status_code": http.StatusBadRequest,
				"meta": gin.H{
					"err_code": 2,
					"err_msg":  err.Error(),
				},
			})
			return
		}
	}

	// Create response structure
	response := OnboardingHandlerCheckUserResponse{
		StatusCode: http.StatusOK,
		Meta:       OnboardingMetaUser{},
		Data:       UserCheckData{Exists: isCreated},
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

func HandleOnboardingTag(c *gin.Context) {
	// Get documents from arangodb
	tags, err := tag_obj.AllDocuments(nil, &tag_obj.AllDocumentsParams{
		Page:     1,
		Quantity: 40,
	})
	if err != nil {
		log.Println("Get All Tags:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	tagCount := len(tags)

	// Create response structure
	response := OnboardingHandlerTagResponse{
		StatusCode: http.StatusOK,
		Meta: OnboardingMeta{
			Count: &tagCount,
		},
		Data: tags,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

func HandleOnboardingCreateUser(c *gin.Context) {
	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Product Document
	var userPreference OnboardingHandlerCreateUserRequest
	err = json.Unmarshal(body, &userPreference)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	prefList := make([]*user_obj.ProductSelectionPair, 0)

	for _, pref := range userPreference.PreferenceList {
		prefList = append(prefList, &user_obj.ProductSelectionPair{
			Product:   pref.Product,
			Preferred: pref.Preferred,
		})
	}

	newUser, err := user_obj.OnboardUser(nil, &user_obj.OnboardParams{
		FirebaseID:     userPreference.FirebaseID,
		PreferenceList: prefList,
	})
	if err != nil {
		log.Println("Onboard User:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Create response structure
	response := OnboardingHandlerCreateUserResponse{
		StatusCode: http.StatusOK,
		Meta: OnboardingMetaUser{
			ErrCode: 0,
		},
		User: newUser,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}

// HandleOnboardingProduct handles product by tag POST requets
// get all product by tag in arangodb, returns list of product documents
func HandleOnboardingProduct(c *gin.Context) {
	// Read request body
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Println("Read Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 0,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	// Parse JSON to Tag Document
	request := OnboardingHandlerProductRequest{}
	err = json.Unmarshal(body, &request)
	if err != nil {
		log.Println("Unmarshal Body:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 1,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	tags := make([]string, 0)
	for _, tag := range request.Data {
		tags = append(tags, tag.Name)
	}

	// Create empty list for append the product
	products, err := product_obj.GetProductByTag(nil, &product_obj.GetProductByTagParams{
		Tags:     tags,
		Quantity: 7,
	})
	if err != nil {
		log.Println("Get Product (By Tag):", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 2,
				"err_msg":  err.Error(),
			},
		})
		return
	}
	// var products = []*product_obj.Document{}
	// for _, tag := range request.Data {
	// 	filter := &product_obj.Document{
	// 		Tags: &[]string{tag.Name},
	// 	}
	//
	// 	productsByTag, _ := product_obj.GetProduct(nil, &product_obj.GetDocumentParams{
	// 		Filter:   filter,
	// 		Quantity: productAmountPerTag,
	// 	})
	// 	products = append(products, productsByTag...)
	// }

	productCount := len(products)

	// Create response structure
	response := OnboardingHandlerProductResponse{
		StatusCode: http.StatusOK,
		Meta: OnboardingMeta{
			Count: &productCount,
		},
		Data: products,
	}

	// Package JSON response data
	resp, err := json.Marshal(response)
	if err != nil {
		log.Println("Marshal Response:", err)
		c.JSON(http.StatusBadRequest, gin.H{
			"status_code": http.StatusBadRequest,
			"meta": gin.H{
				"err_code": 3,
				"err_msg":  err.Error(),
			},
		})
		return
	}

	c.Render(http.StatusOK, render.Data{
		ContentType: "application/json",
		Data:        resp,
	})
}
