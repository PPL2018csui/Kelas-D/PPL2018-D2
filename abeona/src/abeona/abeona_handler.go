package abeona

import (
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
	merchant_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/merchant"
	product_obj "gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core/class/product"
)

var (
	jsonMarshal = json.Marshal
)

//HandleIndex : Handle function for index page
func HandleIndex(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{
		"foo": "bar",
	})
}

//HandleProductView : Handle function for endpoint product
func HandleProductView(c *gin.Context) {
	// TODO get data from DB

	// Dummy data
	productList := map[string][]product_obj.Document{
		"products": []product_obj.Document{
			product_obj.Document{
				Key:   "1234",
				Other: "foobar",
			},
			product_obj.Document{
				Key:   "2345",
				Other: "foobar",
			},
		},
	}

	resp, err := jsonMarshal(productList)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"err": err,
		})
		return
	}

	c.String(http.StatusOK, string(resp))
}

//HandleMerchantView : Handle function for endpoint merchant
func HandleMerchantView(c *gin.Context) {
	// TODO get data from DB

	// Dummy data
	merchantList := map[string][]merchant_obj.Document{
		"merchants": []merchant_obj.Document{
			merchant_obj.Document{
				Key:   "1234",
				Other: "foobar",
			},
			merchant_obj.Document{
				Key:   "2345",
				Other: "foobar",
			},
		},
	}

	resp, err := jsonMarshal(merchantList)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"err": err,
		})
		return
	}

	c.String(http.StatusOK, string(resp))
}
