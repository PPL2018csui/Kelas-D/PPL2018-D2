package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/form"
	_ "github.com/heroku/x/hmetrics/onload"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/abeona"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/admin"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
)

func index(c *gin.Context) {
	c.String(http.StatusOK, "Welcome to the index page :)")
}

func helloworld(c *gin.Context) {
	c.String(http.StatusOK, "Hello World!")
}

func samplejson(c *gin.Context) {
	body, _ := ioutil.ReadAll(c.Request.Body)

	reqData := struct {
		Data string `json:"data"`
	}{}
	json.Unmarshal(body, &reqData)

	// some generic process

	resData := struct {
		Body string `json:"body"`
	}{
		Body: reqData.Data,
	}

	res, _ := json.Marshal(resData)
	c.String(http.StatusOK, string(res))
}

func samplepost(c *gin.Context) {
	body, _ := ioutil.ReadAll(c.Request.Body)
	reqBody, _ := url.ParseQuery(string(body))

	reqData := struct {
		Data string `form:"data"`
	}{}
	decoder := form.NewDecoder()
	decoder.Decode(&reqData, reqBody)

	// some generic process

	resData := struct {
		Body string `json:"body"`
	}{
		Body: reqData.Data,
	}

	res, _ := json.Marshal(resData)
	c.String(http.StatusOK, string(res))
}

// GinMainEngine creates the main gin engine used to route requests
func GinMainEngine() *gin.Engine {
	router := gin.Default()
	router.RedirectTrailingSlash = true

	// routing test
	router.GET("/", index)
	router.GET("/hello-world/", helloworld)
	router.POST("/sample-json/", samplejson)
	router.POST("/sample-form/", samplepost)

	adminGroup := router.Group("/a")
	{
		// product CRUD
		adminGroup.POST("/product/crud", admin.HandleProductCreate)
		adminGroup.GET("/product/crud", admin.HandleProductRead)
		adminGroup.GET("/product/crud/:_key", admin.HandleProductDetail)
		adminGroup.PUT("/product/crud/", admin.HandleProductUpdate)
		adminGroup.DELETE("/product/crud/", admin.HandleProductDelete)

		// merchant CRUD
		adminGroup.POST("/merchant/crud", admin.HandleMerchantCreate)
		adminGroup.GET("/merchant/crud", admin.HandleMerchantRead)
		adminGroup.PUT("/merchant/crud/", admin.HandleMerchantUpdate)
		adminGroup.DELETE("/merchant/crud/", admin.HandleMerchantDelete)
	}

	mobileGroup := router.Group("/m")
	{
		mobileGroup.GET("/onboard/check", abeona.HandleOnboardingCheck)
		mobileGroup.GET("/onboard/tag", abeona.HandleOnboardingTag)
		mobileGroup.POST("/onboard/product", abeona.HandleOnboardingProduct)
		mobileGroup.POST("/onboard/final", abeona.HandleOnboardingCreateUser)

		mobileGroup.GET("/", abeona.HandleIndex)
		mobileGroup.GET("/product", abeona.HandleProductView)
		mobileGroup.GET("/merchant", abeona.HandleMerchantView)
	}

	return router
}

func main() {
	env, err := core.GetEnv()
	if err != nil {
		log.Fatal("Get Env", err)
	}

	port := env.Server.Port

	if port == "" {
		log.Fatal("PORT must be set in .env")
	}

	gin.SetMode(gin.ReleaseMode)
	GinMainEngine().Run(":" + port)
}
