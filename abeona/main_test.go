package main

import (
	"bytes"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"

	"github.com/bouk/monkey"
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"gitlab.com/PPL2018csui/PPL2018-D2/abeona/src/core"
)

func TestRoutingIndex(t *testing.T) {
	r, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()

	gin.SetMode(gin.ReleaseMode)
	GinMainEngine().ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "Welcome to the index page :)", w.Body.String())
}

func TestRoutingHelloWorld(t *testing.T) {
	r, _ := http.NewRequest("GET", "/hello-world/", nil)
	w := httptest.NewRecorder()

	gin.SetMode(gin.TestMode)
	GinMainEngine().ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "Hello World!", w.Body.String())
}

func TestRoutingSampleJson(t *testing.T) {
	jsonBody := []byte(`{"data":"foobar"}`)
	body := bytes.NewBuffer(jsonBody)

	r, _ := http.NewRequest("POST", "/sample-json/", body)
	w := httptest.NewRecorder()

	gin.SetMode(gin.TestMode)
	GinMainEngine().ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "{\"body\":\"foobar\"}", w.Body.String())
}

func TestRoutingSamplePost(t *testing.T) {
	data := url.Values{}
	data.Add("data", "barfoo")

	r, _ := http.NewRequest("POST", "/sample-form/", strings.NewReader(data.Encode()))
	w := httptest.NewRecorder()

	gin.SetMode(gin.TestMode)
	GinMainEngine().ServeHTTP(w, r)

	assert.Equal(t, http.StatusOK, w.Code)
	assert.Equal(t, "{\"body\":\"barfoo\"}", w.Body.String())
}

func TestMain(t *testing.T) {
	go main()

	os.Setenv("PORT", "")
	go main()

	// monkeypatch
	monkey.Patch(core.GetEnv, func() (*core.Config, error) {
		return nil, errors.New("this is a mock error")
	})

	// revert monkeypatch
	defer monkey.UnpatchAll()

	go main()
}
