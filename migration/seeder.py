import random

from faker import Faker

NUM_DATA = 100

fake = Faker('id_ID')
fake.seed(666)
random.seed(666)

def choices(collection, k):
	choice = set()

	sample = []
	for i in range(k):
		a = None
		while True:
			a = random.randrange(len(collection))
			if a not in choice:
				choice.add(a)
				break
		sample.append(collection[a])
	return sample

def seed_admin(graph):
	admins = []
	for i in range(NUM_DATA):
		admin = graph.createVertex("Admin", {
				'username' : fake.first_name().lower(),
				'password' : fake.password(10),
				'email' : fake.email(),
				'photo' : fake.image_url(),
				'nickname' : fake.first_name().lower()
			})
		admins.append(admin)
	return admins

def seed_auth(graph):
	auths = []
	for i in range(NUM_DATA):
		auth = graph.createVertex("Auth", {
				'facebook': fake.uuid4(),
				'password' : fake.password(10),
				'google' : fake.uuid4()
			})
		auths.append(auth)
	return auths

def seed_merchant(graph):
	merchants = []
	categories = ['food', 'accomodation']
	opening_time = ['05:00', '09:00', '11:00']
	closing_time = ['15:00', '18:00', '21:00']
	for i in range(10):
		merchant = graph.createVertex("Merchant", {
				'name' : fake.company(),
				'phone_number' : fake.phone_number(),
				'address' : fake.address(),
				'description' : fake.bs(),
				'location' : [
					float(fake.longitude()),
					float(fake.latitude())
				],
				'is_open' : fake.boolean(80),
				'category' : random.choice(categories),
				'opening_time': random.choice(opening_time),
				'closing_time': random.choice(closing_time),
				'image' : fake.image_url(),
				'gallery' : [fake.image_url(), fake.image_url()],
				'rating' : 3.0
			})
		merchants.append(merchant)
	return merchants

def seed_order(graph):
	orders = []
	for i in range(NUM_DATA//4):
		order = graph.createVertex("Order", {
				'total_item' : abs(fake.pyint())%10,
				'price' : abs(fake.pyint())
			})
		orders.append(order)
	return orders

def seed_product(graph):
	products = []
	for i in range(NUM_DATA):
		product = graph.createVertex("Product", {
				'name' : fake.name(),
				'category' : fake.company_suffix(),
				'price' : abs(fake.pyint()),
				'is_available' : fake.boolean(70),
				'image': fake.image_url(),
				'description' : fake.paragraph(1),
				'gallery' : [fake.image_url(), fake.image_url()],
				'rating' : 3.0

			})
		products.append(product)
	return products

def seed_tag(graph):
	tags = []
	for i in range(10):
		tag = graph.createVertex("Tag", {
			'name' : fake.word()
			})
		tags.append(tag)
	return tags

def seed_user(graph):
	users = []
	for i in range(NUM_DATA):
		user = graph.createVertex("User", {
				'name' : fake.name(),
				'email' : fake.free_email(),
				'phone' : fake.phone_number(),
				'location' : [
							float(fake.longitude()),
							float(fake.latitude())
						],
				'is_verified' : fake.boolean(80),
				'password' : fake.password(20)
			})
		users.append(user)
	return users

def seed_contains(graph, collections):
	orders = collections['Order']
	products = collections['Product']

	for order in orders:
		num_of_items = random.randrange(1,5)
		products_choice = choices(products, k=num_of_items)
		for product in products_choice:
			graph.link("Contains", order, product, {})

def seed_ordered(graph, collections):
	orders = collections['Order']
	users = collections['User']

	for order in orders:
		graph.link("Ordered", random.choice(users), order, {})

def seed_owns(graph, collections):
	products = collections['Product']
	merchants = collections['Merchant']

	for product in products:
		graph.link("Owns", random.choice(merchants), product, {})

def seed_reviews(graph, collections):
	users = collections['User']
	products = collections['Product']

	for user in users:
		num_of_items = random.randrange(1,5)
		products_choice = choices(products, k=num_of_items)
		for product in products_choice:
			graph.link("Reviews", user, product, {})

def seed_handles(graph, collections):
	admins = collections['Admin']
	orders = collections['Order']

	for order in orders:
		graph.link('Handles', random.choice(admins), order, {})

def seed_has(graph, collections):
	users = random.sample(collections['User'], k=len(collections['User']))
	auths = random.sample(collections['Auth'], k=len(collections['Auth']))

	for i in range(len(users)):
		graph.link("Has", users[i], auths[i], {})

def seed_hasTag(graph, collections):
	for items in [collections["Product"], collections['Merchant']]:
		for item in items:
			num = random.randrange(3, 6)
			tags = random.sample(collections['Tag'], k=num)

			for tag in tags :
				graph.link("HasTag", item, tag, {})


def seeding_graph(graph):
	collections = {}
	vertex_seed_func = [
		(seed_admin, "Admin"), (seed_auth,"Auth"),
		(seed_merchant, "Merchant"),
		(seed_order, "Order"), (seed_product, "Product") ,
		(seed_user, "User"), (seed_tag, "Tag")
	]

	for func, vertex in vertex_seed_func:
		collections[vertex] = func(graph)

	edges_func = [
		seed_contains, seed_ordered, seed_owns,
		seed_reviews, seed_handles, seed_has, seed_hasTag
	]

	for func in edges_func:
		func(graph, collections)
