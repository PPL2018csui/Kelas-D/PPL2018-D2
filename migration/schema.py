import os

from pyArango.connection import Connection
from pyArango.collection import Edges, Collection, Field
from pyArango.graph import Graph, EdgeDefinition
from pyArango.validation import *

class Admin(Collection):
	_fields = {
		'username' : Field([NotNull()]),
		'password' : Field([NotNull()]),
		'email' : Field([NotNull(), Email()]),
		'photo' : Field(),
		'nickname' : Field()
	}
	
class Auth(Collection):
	_fields = {
		'facebook' : Field(),
		'google' : Field(),
		'password' : Field()
	}

class Merchant(Collection):
	_fields = {
		'name' :  Field(),
		'phone_number' : Field(),
		'address' : Field(),
		'description' : Field(),
		'is_open' : Field(),
		'category' : Field(), 
		'opening_time' : Field(), 
		'closing_time' : Field(), 
		'location' : Field(), 
		'image' : Field(), 
		'gallery' : Field(), 
		'rating' : Field() 
	}

class Order(Collection):
	_fields = {
		'total_item' : Field([NotNull()]),
		'price' : Field([NotNull()])
	}

class Product(Collection):
	_fields = {
		'name' : Field(),
		'price' : Field(),
		'is_available' : Field(),
		'category' : Field(),
		'description' : Field(),
		'image' : Field(),
		'gallery' : Field(),
		'rating' : Field()
	}

class User(Collection):
	_fields = {
		'name' : Field(),
		'email' : Field([Email(), NotNull()]),
		'phone' : Field(),
		'location' : Field(),
			'is_verified' : Field(),
		'password': Field()
	}

class Tag(Collection):
	_fields = {
		'name' : Field()
	}


class Contains(Edges):
	pass

class Ordered(Edges):
	pass

class Owns(Edges):
	pass

class Reviews(Edges):
	pass

class Handles(Edges):
	pass

class Has(Edges):
	pass

class HasTag(Edges):
	pass

class AbeonaGraph(Graph):
	_edgeDefinitions = [
		EdgeDefinition("Contains", ["Order"], ["Product"]),
		EdgeDefinition("Ordered", ["User"], ["Order"]),
		EdgeDefinition("Owns", ["Merchant"], ["Product"]),
		EdgeDefinition("Reviews", ["User"], ["Product"]),
		EdgeDefinition("Handles", ["Admin"], ["Order"]),
		EdgeDefinition("Has", ['User'], ['Auth']),
		EdgeDefinition("HasTag", ['Product', 'Merchant'], ['Tag'])
	]

	_orphanedCollections = []

def login():
	host = os.getenv("ARANGO_HOST", 'http://127.0.0.1:8529')
	username = os.getenv("ARANGO_USERNAME", None)
	password = os.getenv("ARANGO_PASSWORD", None)

	return Connection(host, username, password)

def create_database(conn):
	db_name = os.getenv("ARANGO_DATABASE_NAME", "abeona")
	if not (conn.hasDatabase(db_name)):
		return conn.createDatabase(name=db_name)
	else:
		raise NameError(db_name + " already exist")

def migration_collection(db):
	collections = ['Admin', 'Auth', 'Merchant', 'Order', 'Product', 'User', 'Tag']
	edges = ['Contains', 'Ordered', 'Owns', 'Handles', 'Reviews', 'Has', 'HasTag']

	collections_dict = {}
	edges_dict = {}

	for collection in collections:
		collections_dict[collection] = db.createCollection(collection)
	for edge in edges:
		edges_dict[edge] = db.createCollection(edge)

	print(collections_dict)
	print(edges_dict)

	return collections_dict, edges_dict

def migration_graph(db):
	graph_name = os.getenv("ARANGO_GRAPH_NAME", "AbeonaGraph")
	return db.createGraph(graph_name)