import os
import time

from dotenv import load_dotenv

from schema import *
from seeder import *

load_dotenv()

def wait_delay(delay_time):
	time.sleep(delay_time)

def main():
	print("Program is up...")
	delay_time = int(os.getenv("WAIT_TIME", "60"))
	print("Waiting for " + str(delay_time) + " seconds.")
	wait_delay(delay_time)

	try:
		conn = login()
		db = create_database(conn)
	except Exception as e:
		print(e)
		exit()
	collections, edges = migration_collection(db)
	graph = migration_graph(db)
	seeding_graph(graph)

if __name__ == '__main__':
	main()
