cription

Please include a summary of the change and which issue is fixed. Please also include relevant motivation and context. List any dependencies that are required for this change.

Fixes # (issue-number)

## Type of change

Please delete options that are not relevant.

- [ ] Bug fix (non-breaking change which fixes an issue)
- [ ] New feature (non-breaking change which adds functionality)
- [ ] Breaking change (fix or feature that would cause existing functionality to not work as expected)
- [ ] Environment, CI/CD or Docker related change
- [ ] This change requires a documentation update

# How Has This Been Tested?

Please describe the tests that you ran to verify your changes. Provide instructions so we can reproduce. Please also list any relevant details for your test configuration

- [ ] Test A
- [ ] Test B

# Who worked on this
Mention the people worked on this change
/cc @boing @cokor
/assign @boing @cokor

# Definition of Done:
Delete this part if not merging a finished task. 
[ ] Sudah lolos seluruh unit testing, green status di GitLab   
[ ] Code coverage 100%.  
[ ] Ada initial data untuk demo, uji coba dan evaluasi pada staging deployment.  
[ ] Sudah memisahkan database berikut script migrasi dan deployment-nya  
untuk testing/staging dan production.  
[ ] Sudah di-deploy di staging server  
[ ] Setiap commit terkait user story ke cabang sit_uat, sudah di-review oleh  
dua orang anggota tim.  
[ ] Setiap isu di GitLab terkait user story, sudah selesai dan di-closed.  
[ ] Semua kode dan pesan commit dipastikan bebas dari profane word

