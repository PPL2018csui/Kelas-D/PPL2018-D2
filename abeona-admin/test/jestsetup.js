/**
 * setup so dont have to import
 */

 const Adapter = require('enzyme-adapter-react-16');
 const { shallow, render, mount } = require('enzyme');
 const Enzyme = require('enzyme');
 //import Adapter from 'enzyme-adapter-react-16';
 //import 'raf/polyfill';
 //import Enzyme, { shallow, render, mount } from 'enzyme';

// React 16 Enzyme adapter
Enzyme.configure({ adapter: new Adapter() });

// Make Enzyme functions available in all test files without importing
global.shallow = shallow;
global.render = render;
global.mount = mount;
