![Abeona](https://gdurl.com/8b2Y)
> Discover Wonder, Explore More

A Conversational Interface based Smart Assistant intended to save your time while travelling and let you discover great things from local businesses by giving you personalized recommendations based on your preferences.

Abeona consists of three applications
* REST API backend written in [Go](https://golang.org/)
* Android native frontend written in [Kotlin](https://kotlinlang.org/)
* Administrator web frontend written with [React](https://reactjs.org/)

----------------------------------
![forthebadge](https://forthebadge.com/images/badges/60-percent-of-the-time-works-every-time.svg)![forthebadge](https://forthebadge.com/images/badges/contains-technical-debt.svg)![forthebadge](https://forthebadge.com/images/badges/built-with-grammas-recipe.svg)

|branch| pipeline | coverage |
|------|--------|---------|
| master | [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/badges/master/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/commits/master) | [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/badges/master/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/commits/master) |
|sit_uat| [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/badges/sit_uat/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/commits/sit_uat) | [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/badges/sit_uat/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/commits/sit_uat) |
| coba_coba | [![pipeline status](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/badges/coba_coba/pipeline.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/commits/coba_coba) | [![coverage report](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/badges/coba_coba/coverage.svg)](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/commits/coba_coba) |

## Table of Contents

- [Introduction](#introduction)
- [Important Links](#important-links)
- [Getting Started](#getting-started)
- [Maintainers](#maintainers)
- [Acknowledgements](#acknowledgements)
- [License](#license)

## Introduction

Abeona is an application service made for PPL subject in collaboration with [Halonesia](https://halonesia.co.id/). There are three parts of this service. The backend written in Go, with two frontend; one is for end user on Android written in Kotlin and another is web based administrator dashboard written with React.

**Presentation**

View Abeona project vision, lean canvas and low fidelity mockup [here](https://gdurl.com/DGfP)

**Concept Map**

![Concept Map](https://gdurl.com/sVw3l)

**Screenshots**

![onboarding](https://gdurl.com/K8lt)![plan](https://gdurl.com/VsCy)![recommendation](https://gdurl.com/eTZI)![Start app](https://gdurl.com/et9D)

View the clickable mockup on [Figma](https://www.figma.com/proto/XSCF4u9ou9hVbYf4FJLhqwAQ/Abeona?scaling=contain)

**Architecture**

![Architecture](https://gdurl.com/pzJd)

## Important Links

**Artifacts**

* [Abeona Blog](http://birthofabeona.surge.sh/) made to document Abeona's journey
* [Gitlab Board](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/boards) as task management and tracking

**Deployment**

* [Admin Webpage](http://abeona-admin.ml)
* [Database Server](https://abeona.tk)
* [REST API Server](https://abeona.ml)

## Getting Started

To get started, clone this project

```bash
git clone https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2.git
cd PPL2018-D2/
```

To get started with the REST API - Go backend

```bash
cd abeona/
```

To get started with the Android - Kotlin frontend

```bash
cd android/
```

To get started with the Web - React frontend

```bash
cd abeona-admin/
```


Prerequisites, installation, tests and usages can be viewed in individual app directory

|Go| Android | React |
|--|--|--|
| [README](abeona) | [README](android) | [README](abeona-admin) |

## Maintainers
This project is made by PPL-D2
* 🍅 [Fersandi Pratama](https://gitlab.com/fersandi) - Coding Ninja | DevOps | SysOps | Hacker
* 🐱 [Wibisana Bramawidya](https://gitlab.com/carash) - Hacker
* 🧟‍♂️ [Vincent](https://gitlab.com/vinctcent) - Hacker
* 💨 [T.G.A. Octavio Putra](https://gitlab.com/Northavon) - Hipster
* 🐼 [Felicia](https://gitlab.com/feliciakrismanta) - Hipstler
* 💆 [Stefanus James Aditya](https://gitlab.com/stefanus.james) - Huscker

With Scrum Master [Ferdinand](https://gitlab.com/ferzos)

## Acknowledgements

- [Firebase Auth](https://firebase.google.com/docs/auth/)
- [Retrofit](https://github.com/square/retrofit)
- [RxJava](https://github.com/ReactiveX/RxJava)
- [RxAndroid](https://github.com/ReactiveX/RxAndroid)
- [RxKotlin](https://github.com/ReactiveX/RxKotlin)
- [GSON](https://github.com/google/gson)
- [Picasso](https://github.com/square/picasso)
- [Google Play Service Maps](https://developers.google.com/android/reference/com/google/android/gms/maps/package-summary)
- [Go Fight](https://github.com/appleboy/gofight)
- [ArangoDB Go Driver](https://github.com/arangodb/go-driver)
- [ArangoDB Velocy Pack](https://github.com/arangodb/go-velocypack)
- [Admin on Rest](https://github.com/marmelab/admin-on-rest)
- [Monkey](https://github.com/bouk/monkey)
- [Go Spew](https://github.com/davecgh/go-spew)
- [Gin](https://github.com/gin-gonic/gin)
- [Form](https://github.com/go-playground/form)
- [Golang Mock](https://github.com/golang/mock)
- [ENV Decode](https://github.com/joeshaw/envdecode)
- [Testify](https://github.com/stretchr/testify)
- [JSDom](https://github.com/jsdom/jsdom)
- [React](https://reactjs.org/)
- [Jest](https://github.com/facebook/jest)
- [Enzyme](https://github.com/airbnb/enzyme)
- [Babel](https://babeljs.io/)
- [ESLint](https://eslint.org/)
- [Docker](https://www.docker.com/)
- [ArangoDB](https://www.arangodb.com/)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/PPL2018csui/Kelas-D/PPL2018-D2/tags).


## License

Abeona is licensed under the [GNU Affero General Public License v3.0](LICENSE.md).
