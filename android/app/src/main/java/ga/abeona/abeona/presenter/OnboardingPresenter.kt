package ga.abeona.abeona.presenter

import ga.abeona.abeona.api.OnboardingApiService
import ga.abeona.abeona.contract.OnboardingContract
import ga.abeona.abeona.model.Tag
import ga.abeona.abeona.model.request.OnboardingProductRequest
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class OnboardingPresenter(
        val view : OnboardingContract.View,
        val backgroundScheduler: Scheduler,
        val mainScheduler: Scheduler
    ) : OnboardingContract.Presenter {
    override var compositeDisposable = CompositeDisposable()
    var apiService = OnboardingApiService.create()
    override var tagList = arrayOf<Tag>()
    override lateinit var tagSelected: BooleanArray

    override fun fetchTagList() {
        val disposable = apiService.getTagList()
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(
                        {
                            response ->
                            tagList = response.data
                            tagSelected = BooleanArray(tagList.size, {_ -> false})
                            view.showTagList(tagList)
                        },
                        {
                            error -> view.showError(parseErrorMessage(error.message))
                        }
                )
        compositeDisposable.add(disposable)
    }

    override fun sendTagList() {
        val productRequest = OnboardingProductRequest(tagList.filterIndexed {
            idx, _ -> tagSelected[idx]
        }.toTypedArray())

        val disposable = apiService.sendTagList(productRequest)
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(
                        {
                            products -> view.nextActivity(products.data)
                        },
                        {
                            error -> view.showError(parseErrorMessage(error.message))
                        }
                )
        compositeDisposable.add(disposable)
    }
}