package ga.abeona.abeona.model.request

import com.google.gson.annotations.SerializedName
import ga.abeona.abeona.model.ProductPreference

data class OnboardingFinalRequest(
    @SerializedName("firebase_id") val firebaseId : String,
    val preference : Array<ProductPreference>
)