package ga.abeona.abeona.contract

import ga.abeona.abeona.model.Product

/**
 * Created by Fersandi Pratama on 22/03/18.
 */
interface ProductPageListContract {
    interface View : BaseView{
        fun showProductList(products: Map<String, List<Product>>)
    }

    interface Presenter : BasePresenter<View>{
        fun showProductList(page : Int = 1, query : String = "")
    }
}