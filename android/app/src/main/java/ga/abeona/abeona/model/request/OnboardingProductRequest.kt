package ga.abeona.abeona.model.request

import ga.abeona.abeona.model.Tag

data class OnboardingProductRequest(
        val data : Array<Tag>
)