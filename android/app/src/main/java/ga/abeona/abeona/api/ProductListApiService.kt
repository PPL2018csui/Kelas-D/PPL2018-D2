package ga.abeona.abeona.api

import ga.abeona.abeona.model.response.ProductListResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by tomato on 22/03/18.
 */
interface ProductListApiService {
    @GET("m/product")
    fun getProducts(
            @Query("page") page : Int = 1,
            @Query ("q") q : String = ""
    ) : Observable<ProductListResponse>

    companion object {
        fun create(): ProductListApiService {
            return BaseService.createRetrofitObject()
                    .create(ProductListApiService::class.java)
        }
    }
}