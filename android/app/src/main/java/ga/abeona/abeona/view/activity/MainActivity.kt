package ga.abeona.abeona.view.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
//import kotlinx.android.synthetic.main.activity_login.view.*
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.firebase.auth.FirebaseAuth
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
//import com.google.firebase.auth.FirebaseUser
import com.google.android.gms.common.api.ApiException
//import android.support.v4.app.FragmentActivity
import android.util.Log
import com.facebook.login.Login
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.AuthResult
//import android.support.annotation.NonNull
import com.google.firebase.auth.GoogleAuthProvider
//import com.google.firebase.auth.AuthCredential
//import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient

import ga.abeona.abeona.R
import ga.abeona.abeona.presenter.LoginPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class MainActivity : AppCompatActivity(), View.OnClickListener, GoogleApiClient.OnConnectionFailedListener {

    private var mAuth: FirebaseAuth? = null
    private var mGoogleSignInClient: GoogleSignInClient? = null
    private var mGoogleApiClient: GoogleApiClient? = null
    private val TAG = "GoogleActivity"
    private val RC_SIGN_IN = 8766

    private lateinit var presenter : LoginPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build()

        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        mAuth = FirebaseAuth.getInstance()

        presenter = LoginPresenter(this,
                Schedulers.io(), AndroidSchedulers.mainThread())

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .enableAutoManage(this@MainActivity /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build()


        findViewById<View>(R.id.emailB).setOnClickListener(this)
        findViewById<View>(R.id.fbB).setOnClickListener(this)
        findViewById<View>(R.id.googleB).setOnClickListener(this)
    }

    private fun signIn() {
        val signInIntent = mGoogleSignInClient!!.signInIntent
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                // Google Sign In was successful, authenticate with Firebase
                val account = task.getResult(ApiException::class.java)
                firebaseAuthWithGoogle(account)
            } catch (e: ApiException) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e)
            }

        }
    }

    private fun firebaseAuthWithGoogle(acct: GoogleSignInAccount) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.id!!)

        val credential = GoogleAuthProvider.getCredential(acct.idToken, null)
        mAuth!!.signInWithCredential(credential).addOnCompleteListener(this, object : OnCompleteListener<AuthResult> {
            override fun onComplete(task: Task<AuthResult>) {
                if (task.isSuccessful()) {
                    Log.d(TAG, "signInWithCredential:success")
//                    startActivity(Intent(this@MainActivity, EmailAndPassActivity::class.java))
                    presenter.checkLogin(mAuth?.currentUser?.uid.orEmpty())
                } else {
                    Log.w(TAG, "signInWithCredential:failure", task.getException());
                }

            }
        })
    }

    private fun signOut() {
        mAuth!!.signOut()

        Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback {}
    }

    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        // An unresolvable error has occurred and Google APIs (including Sign-In) will not
        // be available.
        Log.d(TAG, "onConnectionFailed:" + connectionResult)
    }

    override fun onClick(view: View){
        when(view.id){
            R.id.googleB -> signIn()
            R.id.emailB -> startActivity(Intent(this@MainActivity, EmailAndPassActivity::class.java))
            R.id.signOutB -> signOut()
        }

    }

    fun nextActivity(exist : Boolean){
        val intent : Intent
        if (exist){
            intent = Intent(this, MainActivity::class.java)
        } else {
            intent = Intent(this, OnboardingWelcome::class.java)
        }

        startActivity(intent)
    }
}
