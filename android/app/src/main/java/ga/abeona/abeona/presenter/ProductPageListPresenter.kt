package ga.abeona.abeona.presenter

import ga.abeona.abeona.api.ProductListApiService
import ga.abeona.abeona.contract.ProductPageListContract
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Fersandi Pratama on 22/03/18.
 */
class ProductPageListPresenter(
        val view: ProductPageListContract.View,
        val backgroundScheduler: Scheduler,
        val mainScheduler: Scheduler
) : ProductPageListContract.Presenter {

    var productListApiService = ProductListApiService.create()
    override var compositeDisposable = CompositeDisposable()


    override fun showProductList(page: Int, query: String) {
        val disposable = productListApiService.getProducts(page, query)
                .flatMap { products -> Observable.fromArray(products) }
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(
                        {response -> view.showProductList(response.data.groupBy { product -> product.type })},
                        {error -> view.showError(parseErrorMessage(error.message))}
                )
        compositeDisposable.add(disposable)
    }
}