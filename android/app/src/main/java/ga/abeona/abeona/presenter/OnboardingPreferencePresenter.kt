package ga.abeona.abeona.presenter

import ga.abeona.abeona.api.OnboardingPreferenceApiService
import ga.abeona.abeona.contract.OnboardingPreferenceContract
import ga.abeona.abeona.model.Product
import ga.abeona.abeona.model.ProductPreference
import ga.abeona.abeona.model.request.OnboardingFinalRequest
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable

class OnboardingPreferencePresenter(
        val view : OnboardingPreferenceContract.View,
        val backgroundScheduler: Scheduler,
        val mainScheduler: Scheduler,
        val products : Array<Product>
) : OnboardingPreferenceContract.Presenter{
    var apiService = OnboardingPreferenceApiService.create()
    override var compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val productPreferences = BooleanArray(products.size, {_ -> false})
    var productIndex = products.size -1

    override fun onChooseProduct(like: Boolean) {
        productPreferences[productIndex--] = like

        if (productIndex == 0) {
            sendPreference()
        }

    }

    override fun sendPreference() {
        val request = OnboardingFinalRequest(
                "test", Array(
                products.size, {i ->
            ProductPreference(products[i], productPreferences[i])}
        )
        )
        val disposable = apiService.sendProductPreferenceList(request)
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(
                        {
                            _ -> view.nextActivity()
                        },
                        {
                            err -> view.showError(parseErrorMessage(err.message))
                        }
                )

        compositeDisposable.add(disposable)
    }
}