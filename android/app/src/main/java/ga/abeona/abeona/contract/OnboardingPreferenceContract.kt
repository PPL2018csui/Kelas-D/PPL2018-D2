package ga.abeona.abeona.contract

interface OnboardingPreferenceContract {
    interface View : BaseView{
        fun nextActivity()
    }

    interface Presenter : BasePresenter<View> {
        fun onChooseProduct(like : Boolean)
        fun sendPreference()
    }
}