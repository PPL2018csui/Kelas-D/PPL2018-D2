package ga.abeona.abeona.model

import com.google.gson.annotations.SerializedName
import kotlinx.serialization.*


/**
 * Created by Fersandi Pratama on 12/03/18.
 */
@Serializable
data class Product(val _key : String,
                   val name : String,
                   @SerializedName("is_available") val isAvailable : Boolean = false,
                   val price : Double = 0.0,
                   @SerializedName("category") val type: String = "unknown",
                   val description: String = "unknown",
                   val merchant: Merchant? = null,
                   val rating: Float = 0.0f,
                   val isWishlist: Boolean = false,
                   val phoneNumber: String = "",
                   val website: String = "",
                   @SerializedName("image") val coverPhoto: String = "",
                   @SerializedName("gallery") val galleryPhotos: Array<String>? = null,
                   val nearByProducts: Array<Product>? = null)