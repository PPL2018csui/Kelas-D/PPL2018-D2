package ga.abeona.abeona.view.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ga.abeona.abeona.R
import kotlinx.android.synthetic.main.activity_onboarding_welcome.*


class OnboardingWelcome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding_welcome)
        onboardingFinal_button_prev.setOnClickListener({
            nextActivity()
        })
    }

    fun nextActivity(){
        val intent = Intent(this, OnboardingBubbleActivity::class.java)
        startActivity(intent)
    }
}
