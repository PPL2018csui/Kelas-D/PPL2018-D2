package ga.abeona.abeona.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import ga.abeona.abeona.R;

public class EmailAndPassActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "EmailPassword";

    private TextView statusTextView;
    private TextView detailTextView;
    private EditText field_email;
    private EditText field_pass;

    private FirebaseAuth mAuth;

    @Override
    public void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        statusTextView = findViewById(R.id.status);
        detailTextView = findViewById(R.id.detail);
        field_email = findViewById(R.id.email);
        field_pass = findViewById(R.id.password);

        findViewById(R.id.loginB).setOnClickListener(this);
        findViewById(R.id.registerB).setOnClickListener(this);
        findViewById(R.id.outB).setOnClickListener(this);
        findViewById(R.id.prodB).setOnClickListener(this);
        findViewById(R.id.backB).setOnClickListener(this);
        findViewById(R.id.prodO).setOnClickListener(this);

        mAuth = FirebaseAuth.getInstance();
    }

    private void createAccount(String email, String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(EmailAndPassActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void signIn(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "signInWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            updateUI(user);
                        } else {
                            Log.w(TAG, "signInWithEmail:failure", task.getException());
                            Toast.makeText(EmailAndPassActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
    }

    private void updateUI(FirebaseUser user) {
        if (user != null) {
            Log.d(TAG,"logged in");
            statusTextView.setText(user.getEmail());
            detailTextView.setText(user.getUid());

            findViewById(R.id.emailtextF).setVisibility(View.GONE);
            findViewById(R.id.passwordtextF).setVisibility(View.GONE);
            findViewById(R.id.email).setVisibility(View.GONE);
            findViewById(R.id.password).setVisibility(View.GONE);
            findViewById(R.id.loginB).setVisibility(View.GONE);
            findViewById(R.id.registerB).setVisibility(View.GONE);
            findViewById(R.id.outB).setVisibility(View.VISIBLE);
            findViewById(R.id.prodB).setVisibility(View.VISIBLE);
            findViewById(R.id.prodO).setVisibility(View.VISIBLE);
        } else {
            Log.d(TAG,"logged out or not logged in");
            statusTextView.setText(null);
            detailTextView.setText(null);

            findViewById(R.id.emailtextF).setVisibility(View.VISIBLE);
            findViewById(R.id.passwordtextF).setVisibility(View.VISIBLE);
            findViewById(R.id.email).setVisibility(View.VISIBLE);
            findViewById(R.id.password).setVisibility(View.VISIBLE);
            findViewById(R.id.loginB).setVisibility(View.VISIBLE);
            findViewById(R.id.registerB).setVisibility(View.VISIBLE);
            findViewById(R.id.outB).setVisibility(View.GONE);
            findViewById(R.id.prodB).setVisibility(View.GONE);
            findViewById(R.id.prodO).setVisibility(View.GONE);
        }
    }

    private void signOut(){
        mAuth.signOut();
        updateUI(null);
    }

    @Override
    public void onClick(View v) {
        Log.d(TAG, "" + v.getId());
        switch (v.getId()){
            case (R.id.registerB):
                createAccount(field_email.getText().toString(), field_pass.getText().toString());
                break;
            case (R.id.loginB):
                signIn(field_email.getText().toString(), field_pass.getText().toString());
                break;
            case (R.id.outB):
                signOut();
                startActivity(new Intent(this, MainActivity.class));
                break;
            case (R.id.prodB):
                startActivity(new Intent(this, ProductPageListActivity.class));
                break;
            case (R.id.prodO):
                startActivity(new Intent(this, OnboardingWelcome.class));
                break;
            case (R.id.backB):
                startActivity(new Intent(this, MainActivity.class));
                break;
        }

    }

}