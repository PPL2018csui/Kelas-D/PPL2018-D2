package ga.abeona.abeona.model.response

import ga.abeona.abeona.model.Meta
import ga.abeona.abeona.model.Product

data class ProductListResponse(
        val statusCode : Int,
        val meta : Meta,
        val data : Array<Product>
)