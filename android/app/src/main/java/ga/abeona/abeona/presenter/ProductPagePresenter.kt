package ga.abeona.abeona.presenter

import android.util.Log
import ga.abeona.abeona.api.ProductApiService
import ga.abeona.abeona.contract.ProductPageContract
import ga.abeona.abeona.model.Product
import io.reactivex.Scheduler
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Fersandi Pratama on 15/03/18.
 */
class ProductPagePresenter(val view : ProductPageContract.View,
                           val backgroundScheduler : Scheduler,
                           val mainScheduler : Scheduler)
    : ProductPageContract.Presenter {
    var productApiService = ProductApiService.create()

    override var compositeDisposable = CompositeDisposable()

    override fun loadData(productKey : String) {
        val disposable = productApiService.singleProductData(productKey)
                   .subscribeOn(backgroundScheduler)
                   .observeOn(mainScheduler)
                   .subscribe(
                    {response -> view.showProductData(response.data)},
                    {error -> view.showError(parseErrorMessage(error.message))}
                    )
        compositeDisposable.add(disposable)
    }
}