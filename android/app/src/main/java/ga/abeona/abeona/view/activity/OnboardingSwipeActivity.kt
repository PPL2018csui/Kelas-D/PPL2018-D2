package ga.abeona.abeona.view.activity

import android.content.Intent
import kotlinx.android.synthetic.main.activity_onboarding_swipe.*

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View

import com.afollestad.materialdialogs.MaterialDialog
import com.google.gson.Gson
import com.kidach1.tinderswipe.model.CardModel
import com.kidach1.tinderswipe.view.CardContainer
import com.kidach1.tinderswipe.view.SimpleCardStackAdapter
import ga.abeona.abeona.R
import ga.abeona.abeona.contract.OnboardingPreferenceContract
import ga.abeona.abeona.model.Product
import ga.abeona.abeona.presenter.OnboardingPreferencePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class OnboardingSwipeActivity : AppCompatActivity(), OnboardingPreferenceContract.View {
    private val PRODUCTS = "ga.abeona.abeona.productOnboarding"
    private val cardAdapter by lazy {
        SimpleCardStackAdapter(this)
    }
    private lateinit var presenter : OnboardingPreferencePresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding_swipe)

        val products = Gson().fromJson<Array<Product>>(
                intent.getStringExtra(PRODUCTS), Array<Product>::class.java)

       presenter =  OnboardingPreferencePresenter(this,
                Schedulers.io(), AndroidSchedulers.mainThread(),products)

        setCard(products)
    }

    override fun onResume() {
        super.onResume()

        if (swipeText.visibility == View.VISIBLE){
            nextActivity()
        }
    }

    private fun setCard(products : Array<Product>?){

        if (products == null || products.isEmpty()){
            swipeText.visibility = View.VISIBLE
            swipeText.setText(R.string.no_product_swipe)
            cardContainer.visibility = View.GONE
            return
        }

        for (product in products) {
            val cardModel = CardModel(
                    product.name,
                    product.description,
                    product.coverPhoto)
            cardModel.apply {
                onCardDismissedListener = object : CardModel.OnCardDismissedListener {
                    override fun onLike(callback: CardContainer.OnLikeListener) {
                        Log.i(TAG, "I like the card")
                        presenter.onChooseProduct(true)
                    }

                    override fun onDislike() {
                        Log.i(TAG, "I dislike the card")
                        presenter.onChooseProduct(false)
                    }
                }
                cardAdapter.add(this)
            }
        }

        cardContainer.adapter = cardAdapter
        addSwipeListener(cardContainer)
    }


    private fun addSwipeListener(cardContainer: CardContainer) {
        cardContainer.setOnSwipeListener(object : CardContainer.onSwipeListener {
            override fun onSwipe(scrollProgressPercent: Float) {
                val view : View = cardContainer.selectedView
                view.findViewById<View>(R.id.item_swipe_right_indicator)
                        .alpha = if (scrollProgressPercent < 0) -scrollProgressPercent else 0F
                view.findViewById<View>(R.id.item_swipe_left_indicator)
                        .alpha = if (scrollProgressPercent > 0) scrollProgressPercent else 0F

            }
        })
    }

    override fun nextActivity() {
        startActivity(
                Intent(this, OnboardingFinal::class.java)
        )
    }

    override fun showError(errorMessage: String) {
        Snackbar.make(swipeLayout, errorMessage, Snackbar.LENGTH_LONG).show()
    }

    companion object {

        private val TAG = OnboardingSwipeActivity::class.java.simpleName
    }
}
