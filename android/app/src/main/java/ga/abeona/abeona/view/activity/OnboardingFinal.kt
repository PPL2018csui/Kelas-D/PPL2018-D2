package ga.abeona.abeona.view.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import ga.abeona.abeona.R
import kotlinx.android.synthetic.main.activity_onboarding_final.*
import kotlinx.android.synthetic.main.activity_onboarding_welcome.*

class OnboardingFinal : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding_final)
        OnboardingFinal_button_next.setOnClickListener({
            nextActivity()
        })
        OnboardingFinal_button_prev.setOnClickListener({
            finish()
        })
    }

    fun nextActivity(){
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}
