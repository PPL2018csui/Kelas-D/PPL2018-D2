package ga.abeona.abeona.api

import ga.abeona.abeona.model.response.ProductResponse
import retrofit2.http.GET
import io.reactivex.Observable
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Fersandi Pratama on 15/03/18.
 */
interface ProductApiService {
    @GET("/m/product")
    fun singleProductData(
            @Query("_key") key : String
    ) : Observable<ProductResponse>

    @POST("/wishlist")
    fun toogleWishlist(
            @Query("user") user : String,
            @Query("product") product : String
    )

    companion object {
        fun create(): ProductApiService {
            return BaseService.createRetrofitObject()
                    .create(ProductApiService::class.java)
        }
    }
}