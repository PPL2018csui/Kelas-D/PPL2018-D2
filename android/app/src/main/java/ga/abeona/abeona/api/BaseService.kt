package ga.abeona.abeona.api

import ga.abeona.abeona.BuildConfig
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Fersandi Pratama on 22/03/18.
 */
object BaseService {
        fun createRetrofitObject() : Retrofit{
            val baseUrl = BuildConfig.BASE_URL
             return Retrofit.Builder()
                    .addCallAdapterFactory(
                            RxJava2CallAdapterFactory.create()
                    )
                    .addConverterFactory(
                            GsonConverterFactory.create()
                    )
                    .baseUrl(baseUrl)
                    .build()
        }
}