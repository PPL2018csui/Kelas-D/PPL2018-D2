package ga.abeona.abeona.model

import com.google.gson.annotations.SerializedName

data class Meta(
        @SerializedName("err_code") val errorCode : Int,
        val count : Int = 0,
        @SerializedName("err_msg") val errorMessage : String = ""
)