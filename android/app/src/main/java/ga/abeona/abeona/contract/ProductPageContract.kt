package ga.abeona.abeona.contract

import ga.abeona.abeona.model.Product

/**
 * Created by Fersandi Pratama on 15/03/18.
 */
interface ProductPageContract {
    interface View : BaseView {
        fun showProductData(product : Product)
    }

    interface Presenter : BasePresenter<View> {
        fun loadData(productKey : String)
    }
}