package ga.abeona.abeona.api

import ga.abeona.abeona.model.request.OnboardingProductRequest
import ga.abeona.abeona.model.response.OnboardingProductResponse
import ga.abeona.abeona.model.response.OnboardingTagResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface OnboardingApiService {
    @GET("/m/onboard/tag")
    fun getTagList() : Observable<OnboardingTagResponse>

    @POST("/m/onboard/product")
    fun sendTagList(@Body data : OnboardingProductRequest) : Observable<OnboardingProductResponse>

    companion object {
        fun create() : OnboardingApiService{
            return BaseService.createRetrofitObject()
                    .create(OnboardingApiService::class.java)
        }
    }
}