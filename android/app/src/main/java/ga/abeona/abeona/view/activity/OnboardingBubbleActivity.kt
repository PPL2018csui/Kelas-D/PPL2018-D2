package ga.abeona.abeona.view.activity

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.widget.LinearLayout
import android.content.Intent
import android.util.Log
import com.google.gson.Gson

import com.igalata.bubblepicker.BubblePickerListener
import com.igalata.bubblepicker.adapter.BubblePickerAdapter
import com.igalata.bubblepicker.model.PickerItem
import com.igalata.bubblepicker.rendering.BubblePicker
import ga.abeona.abeona.R
import ga.abeona.abeona.contract.OnboardingContract
import ga.abeona.abeona.model.Product
import ga.abeona.abeona.model.Tag
import ga.abeona.abeona.presenter.OnboardingPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_onboarding_bubble.*
import kotlinx.serialization.json.JSON

class OnboardingBubbleActivity : AppCompatActivity(), OnboardingContract.View {
    private lateinit var presenter : OnboardingContract.Presenter
    private var picker : BubblePicker? = null

    private val PRODUCTS = "ga.abeona.abeona.productOnboarding"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_onboarding_bubble)
        presenter = OnboardingPresenter(
                this, Schedulers.io(), AndroidSchedulers.mainThread())
        next_button.setOnClickListener({
            presenter.sendTagList()
        })

        back_button.setOnClickListener({
            finish()
        })
    }

    override fun onStart() {
        super.onStart()
        presenter.fetchTagList()
    }


    override fun onResume() {
        super.onResume()
        picker?.onResume()
    }

    override fun onPause() {
        super.onPause()
        picker?.onPause()
    }


    override fun showTagList(tags: Array<Tag>) {
        picker = BubblePicker(this)
        picker?.layoutParams = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT)

        bubble_layout.addView(picker)
        setBubblePicker(tags)
    }

    override fun nextActivity(products: Array<Product>) {
        val intent = Intent(this, OnboardingSwipeActivity::class.java)
        intent.putExtra(PRODUCTS, Gson().toJson(products))
        startActivity(intent)
    }

    override fun showError(errorMessage: String) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    private fun setBubblePicker(tags: Array<Tag>){
        picker?.adapter = object : BubblePickerAdapter {
            override val totalCount: Int = tags.size

            override fun getItem(position: Int): PickerItem {
                return PickerItem().apply {
                    title = tags[position].name
                    textColor = ContextCompat.getColor(this@OnboardingBubbleActivity,
                            R.color.white)
                    color = ContextCompat.getColor(this@OnboardingBubbleActivity,
                            R.color.onboarding_bubble_background)
                    typeface = ResourcesCompat.getFont(this@OnboardingBubbleActivity, R.font.noto_sans)!!
                }
            }
        }

        picker?.bubbleSize = 70

        picker?.listener = object : BubblePickerListener {
            override fun onBubbleDeselected(item: PickerItem) {
                item.color = ContextCompat.getColor(this@OnboardingBubbleActivity, R.color.colorPrimary)
                presenter.toggleTag(item.title!!)
            }

            override fun onBubbleSelected(item: PickerItem) {
                item.color = ContextCompat.getColor(this@OnboardingBubbleActivity,
                        R.color.onboarding_bubble_background)
                presenter.toggleTag(item.title!!)
            }
        }
    }

}
