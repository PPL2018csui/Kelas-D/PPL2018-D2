package ga.abeona.abeona.contract

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Created by Fersandi Pratama on 15/03/18.
 */
interface BasePresenter<T : BaseView> {
    var compositeDisposable : CompositeDisposable

    fun onDestroy(){
        compositeDisposable.dispose()
    }

    fun parseErrorMessage(message : String?) : String{
    	if (message != null){
    		return message.toString()
    	}
    	return "Unknown Error"
    }
}