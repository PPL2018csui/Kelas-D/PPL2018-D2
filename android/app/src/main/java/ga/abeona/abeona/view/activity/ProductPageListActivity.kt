package ga.abeona.abeona.view.activity

import android.content.Intent
import android.graphics.Color
import android.graphics.Typeface
import android.opengl.Visibility
import android.os.Bundle
import android.support.v4.content.res.ResourcesCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.ViewGroup.LayoutParams.MATCH_PARENT
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.widget.*
import ga.abeona.abeona.R

import ga.abeona.abeona.contract.ProductPageListContract
import ga.abeona.abeona.model.Product
import ga.abeona.abeona.presenter.ProductPageListPresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.activity_product_page_list.*

class ProductPageListActivity : AppCompatActivity(), View.OnClickListener,
        ProductPageListContract.View {
    private val PRODUCT_ID = "ga.abeona.abeona.product_id"

    val presenter : ProductPageListPresenter by lazy {
        ProductPageListPresenter(this, Schedulers.io(), AndroidSchedulers.mainThread())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_page_list)

    }

    override fun onStart() {
        super.onStart()
        presenter.showProductList()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    fun goToProduct(productId : String) {
        val intent = Intent(this@ProductPageListActivity, ProductPageActivity::class.java)
        intent.putExtra(PRODUCT_ID, productId)
        startActivity(intent)
    }

    override fun onClick(view: View) {

    }

    override fun showProductList(products: Map<String, List<Product>>) {
        val main = findViewById<LinearLayout>(R.id.ProductPageList_MainScrollContainer)

        for (type in products.keys){
            val tagContainer = LinearLayout(this)
            val matchParent = LinearLayout.LayoutParams.MATCH_PARENT;
            val wrapContent = LinearLayout.LayoutParams.WRAP_CONTENT;
            val parentParams = LinearLayout.LayoutParams(matchParent, wrapContent)
            tagContainer.orientation = LinearLayout.VERTICAL
            parentParams.setMargins(0, 0, 0, 10)
            tagContainer.layoutParams = parentParams

            val categoryTextView = TextView(this)
            categoryTextView.textSize = 20f
            categoryTextView.text = type
            categoryTextView.setPadding(20, 30, 0, 0)
            categoryTextView.setTextColor(Color.parseColor("#444444"))
            val typeface = ResourcesCompat.getFont(this, R.font.noto_serif)
            categoryTextView.setTypeface(typeface)
            tagContainer.addView(categoryTextView)

            val productScroll = HorizontalScrollView(this)
            val scrollParams = ViewGroup.LayoutParams(matchParent, wrapContent)
            productScroll.layoutParams = scrollParams
            productScroll.isHorizontalScrollBarEnabled = false
            productScroll.setPadding(0 , 50, 0, 0)

            val productScrollContainer = LinearLayout(this)
            productScrollContainer.layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
            productScrollContainer.orientation = LinearLayout.HORIZONTAL
            productScroll.addView(productScrollContainer)

            for (product in products.get(type)!!.iterator()){
                val linearImageContainer = LinearLayout(this)
                linearImageContainer.layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                linearImageContainer.orientation = LinearLayout.VERTICAL
                linearImageContainer.setBackgroundResource(R.drawable.abc_item_background_holo_dark)
                linearImageContainer.setOnClickListener { goToProduct(product._key) }

                val productImage = ImageView(this)
                productImage.setImageResource(R.drawable.geprek)
                val imgLayoutParams = LinearLayout.LayoutParams(250, 150)
                imgLayoutParams.setMargins(20, 0, 20, 0)
                productImage.layoutParams = imgLayoutParams
                productImage.adjustViewBounds = true
                productImage.scaleType = ImageView.ScaleType.CENTER_CROP
                linearImageContainer.addView(productImage)

                val productTitle = TextView(this)
                val titleParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                titleParams.setMargins(20, 20, 0, 0)
                productTitle.layoutParams = titleParams
                productTitle.gravity = Gravity.LEFT
                productTitle.text = product.name
                val contentTypeface = ResourcesCompat.getFont(this, R.font.noto_sans)
                productTitle.setTypeface(contentTypeface)
                productTitle.textSize = 18F
                productTitle.setPadding(20, 0, 0, 5)
                linearImageContainer.addView(productTitle)

                val productAddress = TextView(this)
                productAddress.layoutParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                val addressParams = LinearLayout.LayoutParams(matchParent, wrapContent)
                addressParams.setMargins(20, 0, 0, 0)
                productAddress.layoutParams = addressParams
                productAddress.gravity = Gravity.LEFT
                productAddress.text = product.merchant?.address.orEmpty()
                productAddress.textSize = 12F
                productAddress.setTypeface(contentTypeface)
                productTitle.setPadding(0, 0, 0, 5)
                linearImageContainer.addView(productAddress)

                productScrollContainer.addView(linearImageContainer)
            }

            tagContainer.addView(productScroll)
            main.addView(tagContainer)

        }

        progressBarProductPage.visibility =  View.GONE
        ProductPageList_MainScroll.visibility = View.VISIBLE
    }

    override fun showError(errorMessage: String) {
        errorText.text = errorMessage
        errorText.visibility = View.VISIBLE
        progressBarProductPage.visibility = View.GONE
    }
}
