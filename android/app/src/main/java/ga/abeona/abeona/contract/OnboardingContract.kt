package ga.abeona.abeona.contract

import ga.abeona.abeona.model.Product
import ga.abeona.abeona.model.Tag

interface OnboardingContract {
    interface View : BaseView{
        fun showTagList(tags : Array<Tag>)
        fun nextActivity(products : Array<Product>)
    }

    interface Presenter : BasePresenter<View>{
        var tagSelected : BooleanArray
        var tagList : Array<Tag>

        fun fetchTagList()
        fun sendTagList()
        fun toggleTag(title : String){
            val index = tagList.indexOfFirst({tag -> tag.name == title})

            if (index != -1)
                tagSelected[index] = !tagSelected[index]
        }
    }
}