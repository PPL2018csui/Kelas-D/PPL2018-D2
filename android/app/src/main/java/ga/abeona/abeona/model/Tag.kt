package ga.abeona.abeona.model

data class Tag(
        val _key : Int,
        val name : String
)