package ga.abeona.abeona.model.response

import com.google.gson.annotations.SerializedName
import ga.abeona.abeona.model.Meta
import ga.abeona.abeona.model.Product

data class OnboardingProductResponse(
        @SerializedName("status_code") val statusCode : Int,
        val meta : Meta,
        val data : Array<Product>
)