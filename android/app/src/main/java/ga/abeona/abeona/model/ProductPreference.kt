package ga.abeona.abeona.model

data class ProductPreference (
        val product: Product,
        val preferred : Boolean
)