package ga.abeona.abeona.presenter

import ga.abeona.abeona.api.LoginApiService
import ga.abeona.abeona.view.activity.MainActivity
import io.reactivex.Scheduler

class LoginPresenter(
        val view : MainActivity,
        val backgroundScheduler: Scheduler,
        val mainScheduler: Scheduler) {
    var apiService = LoginApiService.create()

    fun checkLogin(firebaseId : String){
        val disposable = apiService.checkUser(firebaseId)
                .subscribeOn(backgroundScheduler)
                .observeOn(mainScheduler)
                .subscribe(
                        { response ->
                            view.nextActivity(response.data.exists)
                        },
                        {
                            view.nextActivity(false)
                        }
                )

    }
}