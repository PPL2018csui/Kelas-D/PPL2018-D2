package ga.abeona.abeona.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Fersandi Pratama on 12/03/18.
 */
data class Merchant(
        val _key : String,
        val name : String,
        val location: Array<Double>,
        val rating : Float  = 0.0f,
        @SerializedName("is_open") val isOpen : Boolean = true,
        val category : String = "",
        val summary : String = "No summary available.",
        @SerializedName("opening_time") val openingTime : String = "09.00",
        @SerializedName("closing_time") val closingTime : String = "19.00",
        val address : String = "Jl. Random",
        @SerializedName("phone_number") val phoneNumber : String = "(+62) 888-8888-8888",
        val image : String = "",
        val gallery : Array<String> = arrayOf()
        )