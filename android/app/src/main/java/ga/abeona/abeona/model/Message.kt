package ga.abeona.abeona.model

/**
 * Created by Fersandi Pratama on 22/03/18.
 */
data class Message(
        val code: Int,
        val message: String
)