package ga.abeona.abeona.view.activity

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent

import ga.abeona.abeona.R


class StartActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_start)
    }

    fun getStarted() {
        val intent = Intent(this@StartActivity, MainActivity::class.java)
        startActivity(intent)
    }
}
