package ga.abeona.abeona.view.fragment

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ga.abeona.abeona.R

class StepFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.activity_step_fragment, container, false)
        //TODO this made error
        //lblPage = view.findViewById(R.id.lbl_page)
        return view

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val headingTxt = view!!.findViewById<TextView>(R.id.heading_text)
        val infoTxt = view!!.findViewById<TextView>(R.id.info_text)
        val page = arguments!!.getInt("page", 0)
        if (page == 1) {
            infoTxt.setText(R.string.onboarding_guide_1)
            headingTxt.setText(R.string.onboarding_welcome)
        } else {
            infoTxt.setText(R.string.onboarding_guide_2)
            headingTxt.setText(R.string.onboarding_thanks)
        }
    }

    companion object {

        fun newInstance(page: Int, isLast: Boolean): StepFragment {
            val args = Bundle()
            args.putInt("page", page)
            if (isLast)
                args.putBoolean("isLast", true)
            val fragment = StepFragment()
            fragment.arguments = args
            return fragment
        }
    }
}