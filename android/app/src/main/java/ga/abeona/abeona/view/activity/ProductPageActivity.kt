package ga.abeona.abeona.view.activity

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.*
import ga.abeona.abeona.model.Product
import ga.abeona.abeona.contract.ProductPageContract
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.picasso.Picasso
import ga.abeona.abeona.R
import ga.abeona.abeona.presenter.ProductPagePresenter
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

import kotlinx.android.synthetic.main.activity_product_page.*

class ProductPageActivity : AppCompatActivity(),  ProductPageContract.View, OnMapReadyCallback {

    private val PRODUCT_ID = "ga.abeona.abeona.PRODUCT_ID"
    private lateinit var presenter : ProductPagePresenter
    private lateinit var coordinatorLayout : View
    private lateinit var mapFragment: SupportMapFragment
    private lateinit var intentMaps : Intent
    private lateinit var productObj : Product

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_page)

        coordinatorLayout = findViewById<View>(R.id.productPage)
        presenter = ProductPagePresenter(this, Schedulers.io(), AndroidSchedulers.mainThread())
        presenter.loadData(intent.getStringExtra(PRODUCT_ID).orEmpty())

        fab.setOnClickListener { _ ->
            startActivity(intentMaps)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showError(errMessage: String) {
        Snackbar.make(coordinatorLayout, errMessage, Snackbar.LENGTH_LONG).show()
    }

    override fun showProductData(product: Product) {
        val locationName = findViewById<TextView>(R.id.location_name)
        val ratingBar = findViewById<RatingBar>(R.id.location_rating)
        val descriptionText = findViewById<TextView>(R.id.description_text)
        val websiteText = findViewById<TextView>(R.id.website)
        val phoneText = findViewById<TextView>(R.id.phone_num)
        val heroImage = findViewById<ImageView>(R.id.hero_image)
        val galleryPhotos = findViewById<LinearLayout>(R.id.gallery_linear)
        val noGalleryText = findViewById<TextView>(R.id.no_gallery_text)

        locationName.text = product.name
        ratingBar.rating = product.rating
        descriptionText.text = product.description
        websiteText.text = product.website
        phoneText.text = product.phoneNumber

        if (product.galleryPhotos == null){
            galleryPhotos.visibility = View.GONE
            noGalleryText.visibility = View.VISIBLE

        }

        val uri = "https://www.google.com/maps/search/?api=1&query=${product.merchant!!.location[0]}," +
                "${product.merchant.location[1]}"
        intentMaps = Intent(Intent.ACTION_VIEW, Uri.parse(uri))

        Picasso.with(this).load(product.coverPhoto).into(heroImage)
        productObj = product
        mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

    }

    override fun onMapReady(gMaps: GoogleMap?) {
        val latLng = LatLng(productObj.merchant!!.location[0], productObj.merchant!!.location[1])
        gMaps!!.addMarker(MarkerOptions().position(latLng).title(""))
        gMaps.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15f))
    }
}