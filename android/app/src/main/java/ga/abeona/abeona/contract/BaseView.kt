package ga.abeona.abeona.contract

/**
 * Created by Fersandi Pratama on 15/03/18.
 */
interface BaseView {
    fun showError(errorMessage : String)
}