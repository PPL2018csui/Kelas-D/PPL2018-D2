package ga.abeona.abeona.api

import ga.abeona.abeona.model.response.CheckUserResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface LoginApiService {

    @GET("/m/onboard/check")
    fun checkUser(@Query("firebase_id") firebaseId : String) : Observable<CheckUserResponse>

    companion object {
        fun create() : LoginApiService{
            return BaseService.createRetrofitObject()
                    .create(LoginApiService::class.java)
        }
    }
}