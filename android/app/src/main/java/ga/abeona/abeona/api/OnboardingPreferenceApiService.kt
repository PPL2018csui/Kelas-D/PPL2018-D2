package ga.abeona.abeona.api

import ga.abeona.abeona.model.Message
import ga.abeona.abeona.model.User
import ga.abeona.abeona.model.request.OnboardingFinalRequest
import ga.abeona.abeona.model.response.OnboardingFinalResponse
import ga.abeona.abeona.model.response.ProductListResponse
import io.reactivex.Observable
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface OnboardingPreferenceApiService {
    @POST("/m/onboard/final")
    fun sendProductPreferenceList(@Body data : OnboardingFinalRequest)
            : Observable<OnboardingFinalResponse>
    companion object {
        fun create() : OnboardingPreferenceApiService{
            return BaseService.createRetrofitObject()
                    .create(OnboardingPreferenceApiService::class.java)
        }
    }
}