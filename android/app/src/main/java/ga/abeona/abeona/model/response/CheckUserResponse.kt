package ga.abeona.abeona.model.response

import com.google.gson.annotations.SerializedName
import ga.abeona.abeona.model.Meta

data class CheckUserResponse(
        @SerializedName("status_code") val statusCode : Int,
        val meta : Meta,
        val data : CheckUser
)

data class CheckUser(
        val exists : Boolean
)