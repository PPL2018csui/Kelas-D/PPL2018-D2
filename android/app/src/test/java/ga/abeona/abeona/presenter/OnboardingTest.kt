package ga.abeona.abeona.presenter

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import ga.abeona.abeona.api.OnboardingApiService
import ga.abeona.abeona.contract.OnboardingContract
import ga.abeona.abeona.model.*
import ga.abeona.abeona.model.request.OnboardingProductRequest
import ga.abeona.abeona.model.response.OnboardingProductResponse
import ga.abeona.abeona.model.response.OnboardingTagResponse
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import junit.framework.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Test

class OnboardingTest {
    val onboardingApiService : OnboardingApiService = mock()
    val view : OnboardingContract.View = mock()
    val scheduler = TestScheduler()
    val tagSelected = BooleanArray(5, {_ -> true})
    lateinit var presenter : OnboardingPresenter

    @Before
    fun init(){
        presenter = OnboardingPresenter(view, scheduler, scheduler)
        presenter.apiService = onboardingApiService
        presenter.tagSelected = tagSelected
        presenter.tagList = arrayOf(
                Tag(1, "Food"),
                Tag(2, "Beverage"),
                Tag(3, "Sushi"),
                Tag(4, "Wine"),
                Tag(5, "Souvenir")
        )
    }

    @Test
    fun fetchTagSuccess(){
        val mockTags = arrayOf<Tag>(
                Tag(1, "Food"),
                Tag(2, "Beverage"),
                Tag(3, "Sushi")
        )
        val mockResponse = OnboardingTagResponse(
                200,
                Meta(200, 3),
                mockTags
        )

        whenever(onboardingApiService.getTagList())
                .thenReturn(Observable.just(mockResponse))

        presenter.fetchTagList()
        scheduler.triggerActions()

        verify(view).showTagList(mockTags)
    }

    @Test
    fun fetchTagError(){
        val error = Exception("Error Occured")

        whenever(onboardingApiService.getTagList())
                .thenReturn(Observable.error(error))

        presenter.fetchTagList()
        scheduler.triggerActions()

        verify(view).showError("Error Occured")
    }

//    @Test
//    fun sendTagSuccess(){
//        val mockRequest = OnboardingProductRequest(
//                presenter.tagList
//        )
//
//        val mockResponse = OnboardingProductResponse(
//                200,
//                Meta(200),
//                arrayOf(
//                        Product("1", "Nendoroid Maki Nishikino")
//                )
//        )
//
//        whenever(onboardingApiService.sendTagList(mockRequest))
//                .thenReturn(Observable.just(mockResponse))
//
//        presenter.sendTagList()
//        scheduler.triggerActions()
//
//        verify(view).nextActivity(mockResponse.data)
//    }
//
//    @Test
//    fun sendTagFailed(){
//        val mockPostRequest = OnboardingProductRequest(
//                presenter.tagList
//        )
//
//        val error = Exception("Network Timed Out")
//
//        whenever(onboardingApiService.sendTagList(mockPostRequest))
//                .thenReturn(Observable.error(error))
//
//        presenter.sendTagList()
//        scheduler.triggerActions()
//
//        verify(view).showError("Network Timed Out")
//    }


    @Test
    fun testSetterGetter(){
        val tags = BooleanArray(1, {_ -> false})
        presenter.tagSelected = tags

        assertTrue(presenter.tagSelected.size == 1)

        val bScheduler = presenter.backgroundScheduler
        val mScheduler = presenter.mainScheduler
        val cDisposable = presenter.compositeDisposable
        val aService = presenter.apiService

        assertTrue(bScheduler is TestScheduler)
        assertTrue(mScheduler is TestScheduler)
        assertTrue(cDisposable is CompositeDisposable)
        assertTrue(aService is OnboardingApiService)
        assertTrue(tags is BooleanArray)
    }


    @Test
    fun testOnDestroy(){
        val compositeDisposable : CompositeDisposable = mock()

        presenter.compositeDisposable = compositeDisposable

        presenter.onDestroy()
        verify(compositeDisposable).dispose()
    }
}