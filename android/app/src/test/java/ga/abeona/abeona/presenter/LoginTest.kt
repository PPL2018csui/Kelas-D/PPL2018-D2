package ga.abeona.abeona.presenter

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import ga.abeona.abeona.api.LoginApiService
import ga.abeona.abeona.model.Meta
import ga.abeona.abeona.model.response.CheckUser
import ga.abeona.abeona.model.response.CheckUserResponse
import ga.abeona.abeona.view.activity.MainActivity
import io.reactivex.Observable
import org.junit.Before
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Test

class LoginTest{
    private val apiService : LoginApiService = mock()

    private val view : MainActivity = mock()

    private lateinit var presenter : LoginPresenter
    private val scheduler = TestScheduler()

    @Before
    fun init(){
        presenter = LoginPresenter(view, scheduler, scheduler)
        presenter.apiService = apiService
    }

    @Test
    fun checkLoginSuccess(){
        val firebaseId = "usodayo"

        val mockResponse = CheckUserResponse(
                200,
                Meta(200),
                CheckUser(true)
        )

        whenever(apiService.checkUser(firebaseId)).thenReturn(Observable.just(mockResponse))

        presenter.checkLogin(firebaseId)
        scheduler.triggerActions()

        verify(view).nextActivity(true)
    }

    @Test
    fun checkLoginFail(){
        val firebaseId = "usodayo"

        whenever(apiService.checkUser(firebaseId)).thenReturn(Observable.error(Exception()))

        presenter.checkLogin(firebaseId)
        scheduler.triggerActions()

        verify(view).nextActivity(false)
    }

    @Test
    fun testGetterSetter(){
        val backgroundScheduler = presenter.backgroundScheduler
        val mainScheduler = presenter.mainScheduler
        val productApiServiceTest = presenter.apiService

        Assert.assertTrue(backgroundScheduler is TestScheduler)
        Assert.assertTrue(mainScheduler is TestScheduler)
        Assert.assertTrue(productApiServiceTest is LoginApiService)
    }

}