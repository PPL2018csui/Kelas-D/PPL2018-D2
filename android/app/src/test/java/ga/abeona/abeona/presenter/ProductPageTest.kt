package ga.abeona.abeona.presenter

import ga.abeona.abeona.api.ProductApiService
import ga.abeona.abeona.contract.ProductPageContract
import ga.abeona.abeona.model.Merchant
import ga.abeona.abeona.model.Product
import io.reactivex.Observable
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.InjectMocks
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import ga.abeona.abeona.model.Meta
import ga.abeona.abeona.model.response.ProductResponse
import io.reactivex.disposables.CompositeDisposable
import org.junit.Assert.assertTrue

/**
 * Created by Fersandi Pratama on 19/03/18.
 */
class ProductPageTest {
    private val productApiService : ProductApiService = mock()
    private val view : ProductPageContract.View = mock()

    @InjectMocks
    private lateinit var presenter : ProductPagePresenter

    lateinit var scheduler : TestScheduler

    @Before
    fun init(){
//        MockitoAnnotations.initMocks(this)
        scheduler = TestScheduler()

        presenter = ProductPagePresenter(this.view,
                scheduler,
                scheduler)

        presenter.productApiService = productApiService
    }

    @Test
    fun fetchProductSuccess(){
        val mockProduct = Product("", "", true, 1.0, "",
                "", Merchant("", "", arrayOf(1.0, 1.0)), 4.0f, true,
                "123", "321", "",
                null)
        val responseMock = ProductResponse(
                200,
                Meta(200, 1),
                mockProduct
        )

        whenever(productApiService.singleProductData("maki"))
                .thenReturn(Observable.just(responseMock))

        presenter.loadData("maki")
        scheduler.triggerActions()

        verify(view).showProductData(mockProduct)
    }

    @Test
    fun fetchProductFail(){
        val ex = Exception("Error occured")
        whenever(productApiService.singleProductData("nico"))
                .thenReturn(Observable.error(ex))

        presenter.loadData("nico")
        scheduler.triggerActions()

        verify(view).showError("Error occured")
    }

    @Test
    fun fetchProductFailUnknownError(){
        val ex = Exception()
        whenever(productApiService.singleProductData("honk"))
                .thenReturn(Observable.error(ex))

        presenter.loadData("honk")
        scheduler.triggerActions()

        verify(view).showError("Unknown Error")
    }

    @Test
    fun testGetterSetter(){
        val backgroundScheduler = presenter.backgroundScheduler
        val mainScheduler = presenter.mainScheduler
        val productApiServiceTest = presenter.productApiService

        assertTrue(backgroundScheduler is TestScheduler)
        assertTrue(mainScheduler is TestScheduler)
        assertTrue(productApiServiceTest is ProductApiService)
    }

    @Test
    fun testOnDestroy(){
        var compositeDisposable : CompositeDisposable = mock()

        presenter.compositeDisposable = compositeDisposable

        presenter.onDestroy()
        verify(compositeDisposable).dispose()
    }
}