package ga.abeona.abeona.presenter

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import com.nhaarman.mockito_kotlin.whenever
import ga.abeona.abeona.api.ProductListApiService
import ga.abeona.abeona.contract.ProductPageListContract
import ga.abeona.abeona.model.Meta
import ga.abeona.abeona.model.Product
import ga.abeona.abeona.model.response.ProductListResponse
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.TestScheduler
import org.junit.Assert
import org.junit.Before
import org.junit.Test

/**
 * Created by Fersandi Pratama on 22/03/18.
 */
class ProducrPageListTest {
    val productListService : ProductListApiService  = mock()
    val view : ProductPageListContract.View = mock()
    lateinit var productPageListPresenter : ProductPageListPresenter
    val scheduler = TestScheduler()

    @Before
    fun initObject(){
        productPageListPresenter = ProductPageListPresenter(view, scheduler, scheduler)
        productPageListPresenter.productListApiService = productListService
    }

    @Test
    fun fetchDataSuccess(){
        val mockProducts : Array<Product> = arrayOf(
                Product("Nendo_Maki", "Nendoroid Maki Nishikino Casual ver."),
                Product("Neso_Maru", "SUper nesoberi Hanamaru Kunikida"),
                Product("Figmma_Kato", "Figma Kato Megumi")
        )
        val responseMock = ProductListResponse(
                200,
                Meta(200, 3),
                mockProducts
        )

        whenever(productListService.getProducts(1, ""))
                .thenReturn(Observable.just(responseMock))

        productPageListPresenter.showProductList(1, "")
        scheduler.triggerActions()

        verify(view).showProductList(mockProducts.groupBy { it.type })
    }

    @Test
    fun fetchDataFail(){
        val exception = Exception("Network time out")
        whenever(productListService.getProducts(10, "Error"))
                .thenReturn(Observable.error(exception))

        productPageListPresenter.showProductList(10, "Error")
        scheduler.triggerActions()


        verify(view).showError(exception.message.orEmpty())
    }

    @Test
    fun fetchProductFailUnknownError(){
        val ex = Exception()
        whenever(productListService.getProducts(10, "Error"))
                .thenReturn(Observable.error(ex))

        productPageListPresenter.showProductList(10, "Error")
        scheduler.triggerActions()

        verify(view).showError("Unknown Error")
    }

    @Test
    fun testGetterSetter(){
        val backgroundScheduler = productPageListPresenter.backgroundScheduler
        val mainScheduler = productPageListPresenter.mainScheduler
        val productListApiServiceTest = productPageListPresenter.productListApiService

        Assert.assertTrue(backgroundScheduler is TestScheduler)
        Assert.assertTrue(mainScheduler is TestScheduler)
        Assert.assertTrue(productListApiServiceTest is ProductListApiService)
    }

    @Test
    fun testOnDestroy(){
        val compositeDisposable : CompositeDisposable = mock()

        productPageListPresenter.compositeDisposable = compositeDisposable

        productPageListPresenter.onDestroy()
        verify(compositeDisposable).dispose()
    }
}