package ga.abeona.abeona.presenter

import com.nhaarman.mockito_kotlin.mock
import ga.abeona.abeona.api.OnboardingPreferenceApiService
import ga.abeona.abeona.contract.OnboardingPreferenceContract
import ga.abeona.abeona.model.Product
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test

class OnboardingPreferenceTest{
    private val apiService : OnboardingPreferenceApiService = mock()
    private val view : OnboardingPreferenceContract.View = mock()
    private val testScheduler  = TestScheduler()

    lateinit var presenter : OnboardingPreferencePresenter

    @Before
    fun init(){
        presenter  = OnboardingPreferencePresenter(view, testScheduler, testScheduler, arrayOf(
                Product("1", "test")
        ))
        presenter.apiService = apiService
    }

    @Test
    fun fetchDataSuccess(){
        presenter.apiService
    }

    @Test
    fun fetchDataFail(){}
}